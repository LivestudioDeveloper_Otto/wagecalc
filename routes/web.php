<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
//Route::get('/', 'Auth\LoginController@login')->name('auth.login');

Auth::routes();

Route::get('/', 'DashboardController@index');
Route::get('home', 'DashboardController@index');
Route::get('logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::prefix('admin')->group(function () {
	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::post('/set-session-date', 'DashboardController@setSessionDate')->name('setsessiondate');
	
	Route::prefix('restricteddate')->group(function () {
		Route::get('', 'RestrictedDateController@view')->name('restricted_date.view');
		Route::post('create', 'RestrictedDateController@create');
		Route::get('delete/{restricted_date}', 'RestrictedDateController@delete');
	});
	
	Route::prefix('closedmonths')->group(function () {
		Route::get('', 'ClosedMonthController@index')->name('closed_month.index');
		Route::get('view/{id}', 'ClosedMonthController@view')->name('closed_month.view');
		Route::get('close', 'ClosedMonthController@close');
		Route::get('open', 'ClosedMonthController@open');
		Route::get('download/{month_id}/{worker_id}', 'ClosedMonthController@download')->name('closed_month.download');
		Route::get('download/{month_id}', 'ClosedMonthController@download')->name('closed_month.download_sum');
		Route::get('download-zip/{month_id}', 'ClosedMonthController@downloadZip')->name('closed_month.download_zip');
	});
	
	/*
	 *	@not implemented yet, only superadmin can edit
	 */
	Route::prefix('workergroup')->group(function () {
		Route::get('', 'WorkerGroupController@view')->name('worker_group.view');
		Route::post('create', 'WorkerGroupController@create');
		Route::get('edit/{id?}', 'WorkerGroupController@edit');
		Route::get('delete/{worker_group}', 'WorkerGroupController@delete');
		
		Route::prefix('dates/{group_id}/')->group(function () {
			Route::get('', 'WorkerGroupDateController@view');
			Route::post('create', 'WorkerGroupDateController@create');
			Route::get('edit/{id?}', 'WorkerGroupDateController@edit');
			Route::get('delete/{worker_group_date}', 'WorkerGroupDateController@delete');
		});
	});
	
	Route::prefix('grouphistory')->group(function () {
		Route::get('', 'GroupHistoryController@view');
		Route::post('create', 'GroupHistoryController@create');
		Route::get('delete/{id}', 'GroupHistoryController@delete');
	});
	
	Route::prefix('job')->group(function () {
		Route::get('', 'JobController@view')->name('job.view');
		Route::post('create', 'JobController@create');
		Route::get('edit/{id?}', 'JobController@edit');
		Route::get('delete/{id}', 'JobController@delete');
	});	
	
	Route::prefix('company')->group(function () {
		Route::get('', 'CompanyController@view')->name('company.view');
		Route::post('create', 'CompanyController@create');
		Route::get('edit/{id?}', 'CompanyController@edit');
		Route::get('delete/{id}', 'CompanyController@delete');
	});	
    
    Route::prefix('worker')->group(function () {
        Route::get('/board', 'WorkerBoardController@view')->name('workerboard');
        Route::get('/company/{company_id}', 'WorkerBoardController@board')->where(['company_id' => '[0-9]+']);
        Route::get('/{worker_id}', 'WorkerController@board')->where(['worker_id' => '[0-9]+'])->name('worker.board');
        Route::put('/{worker_id}/save', 'WorkerController@saveBoard')->where(['worker_id' => '[0-9]+'])->name('worker.saveboard');
		Route::get('edit/{id?}', 'WorkerController@edit')->where(['id' => '[0-9]+'])->name('worker.edit');
		Route::post('save', 'WorkerController@save')->name('worker.save');
		Route::get('delete/{id}', 'WorkerController@delete')->where(['id' => '[0-9]+'])->name('worker.delete');
		Route::get('export/{id}', 'WorkerController@exportMonth')->where(['id' => '[0-9]+'])->name('worker.export');
    });
	
	Route::prefix('check')->group(function () {
		Route::get('', 'CheckController@index')->name('check.index');
		Route::post('/import', 'CheckController@import')->name('check.import');
	});
});