<?php

use Illuminate\Database\Seeder;

class WorkerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//Factory(App\Worker::class, 20)->create();
		/*$workers = [
			['nr' => '085', 'name' => 'Ancsin Györgyné'],
			['nr' => '011', 'name' => 'Annaházi Erzsébet'],
			['nr' => '004', 'name' => 'Augusztin Mária'],
			['nr' => '028', 'name' => 'Bánszki Pálné'],
			['nr' => '062', 'name' => 'Bárdos Katalin'],
			['nr' => '039', 'name' => 'Bihari Gábor'],
			['nr' => '091', 'name' => 'Bohusné'],
			['nr' => '60', 'name' => 'Boldizsár'],
			['nr' => '110', 'name' => 'Dankó Lászlóné'],
			['nr' => '56', 'name' => 'Diószegi Ramóna'],
			['nr' => '002', 'name' => 'Dobróczki János'],
			['nr' => '012', 'name' => 'Farkas Zoltán'],
			['nr' => '065', 'name' => 'Felföldi András'],
			['nr' => '069', 'name' => 'Furac András'],
			['nr' => '057', 'name' => 'Gance Gábor'],
			['nr' => '035', 'name' => 'Gömöri János'],
			['nr' => '112', 'name' => 'Hegyesi Michel'],
			['nr' => '046', 'name' => 'Horváth László'],
			['nr' => '93', 'name' => 'Izsó Imre'],
			['nr' => '031', 'name' => 'Kiss László'],
			['nr' => '095', 'name' => 'Kova Nikolett'],
			['nr' => '015', 'name' => 'Kovács Éva'],
			['nr' => '126', 'name' => 'Lipcsei Anikó'],
			['nr' => '006', 'name' => 'Mecinné'],
			['nr' => '083', 'name' => 'Mezei Katalin'],
			['nr' => '34', 'name' => 'Mihalec György'],
			['nr' => '027', 'name' => 'Péter Tiborné'],
			['nr' => '103', 'name' => 'Popol Péter'],
			['nr' => '007', 'name' => 'Rakusz János'],
			['nr' => '053', 'name' => 'Sütő Norbert'],
			['nr' => '094', 'name' => 'Sziegel Dávid Zoltán'],
			['nr' => '030', 'name' => 'Szűcs Katalin'],
			['nr' => '059', 'name' => 'Tóth Éva'],
			['nr' => '092', 'name' => 'Tóth Gergő'],
			['nr' => '045', 'name' => 'Varga Lajos'],
			['nr' => '014', 'name' => 'Varró Imre'],
			['nr' => '77', 'name' => 'Viczián Gyuláné'],
			['nr' => '026', 'name' => 'Vidovenyecz László'],
			['nr' => '100', 'name' => 'Vincze Zoltán']
		];*/
		$workers = [
			['nr' => 21, 'name' => 'Ancsin Györgyné', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 30, 'annual_sick_leave' => 15, 'start_free_days' => 30, 'start_sick_leaves' => 0, 'start_overtimes' => 59],
			['nr' => 35, 'name' => 'Annaházi Erzsébet', 'group' => 'Szellemi1', 'job' => 'GYEK', 'annual_free_day' => 34, 'annual_sick_leave' => 15, 'start_free_days' => 24, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 57, 'name' => 'Aradszki Tamás', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 12, 'annual_sick_leave' => 7, 'start_free_days' => 7, 'start_sick_leaves' => 0, 'start_overtimes' => 55],
			['nr' => 41, 'name' => 'Augusztin Mária', 'group' => 'Szellemi1', 'job' => 'Igazgatás', 'annual_free_day' => 32, 'annual_sick_leave' => 15, 'start_free_days' => 29, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 22, 'name' => 'Bánfi Róbert', 'group' => 'Szellemi1', 'job' => 'GYEK', 'annual_free_day' => 36, 'annual_sick_leave' => 15, 'start_free_days' => 36, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 36, 'name' => 'Bánszki Pálné', 'group' => 'Szellemi1', 'job' => 'GYEK', 'annual_free_day' => 32, 'annual_sick_leave' => 15, 'start_free_days' => 23, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 2, 'name' => 'Bárdos Katalin', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 25, 'annual_sick_leave' => 15, 'start_free_days' => 19, 'start_sick_leaves' => 0, 'start_overtimes' => 44],
			['nr' => 13, 'name' => 'Bohus Györgyné', 'group' => 'Takarítás', 'job' => 'Igazgatás', 'annual_free_day' => 31, 'annual_sick_leave' => 15, 'start_free_days' => 25, 'start_sick_leaves' => 4, 'start_overtimes' => 20],
			['nr' => 23, 'name' => 'Boldizsár Zoltán', 'group' => 'Műszak1', 'job' => 'Nyomtatás', 'annual_free_day' => 28, 'annual_sick_leave' => 15, 'start_free_days' => 27, 'start_sick_leaves' => 0, 'start_overtimes' => 37],
			['nr' => 3, 'name' => 'Dankó Lászlóné', 'group' => 'Takarítás', 'job' => 'Igazgatás', 'annual_free_day' => 31, 'annual_sick_leave' => 15, 'start_free_days' => 24, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 45, 'name' => 'Dér Imre', 'group' => 'Műszak1', 'job' => 'Nyomtatás', 'annual_free_day' => 20, 'annual_sick_leave' => 15, 'start_free_days' => 12, 'start_sick_leaves' => 0, 'start_overtimes' => 23],
			['nr' => 4, 'name' => 'Dobróczki János', 'group' => 'Műszak2', 'job' => 'Nyomtatás', 'annual_free_day' => 33, 'annual_sick_leave' => 15, 'start_free_days' => 22, 'start_sick_leaves' => 11, 'start_overtimes' => 32],
			['nr' => 24, 'name' => 'Farkas Zoltán', 'group' => 'TMK 2', 'job' => 'TMK', 'annual_free_day' => 33, 'annual_sick_leave' => 15, 'start_free_days' => 29, 'start_sick_leaves' => 3, 'start_overtimes' => 0],
			['nr' => 25, 'name' => 'Felföldi András', 'group' => 'Műszak3', 'job' => 'Nyomtatás', 'annual_free_day' => 40, 'annual_sick_leave' => 15, 'start_free_days' => 31, 'start_sick_leaves' => 0, 'start_overtimes' => 23],
			['nr' => 48, 'name' => 'Furac András', 'group' => 'Műszak3', 'job' => 'Nyomtatás', 'annual_free_day' => 21, 'annual_sick_leave' => 15, 'start_free_days' => 16, 'start_sick_leaves' => 0, 'start_overtimes' => 26],
			['nr' => 104, 'name' => 'Gance Gábor', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 13, 'annual_sick_leave' => 8, 'start_free_days' => 5, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 42, 'name' => 'Gömöri János', 'group' => 'Szellemi2', 'job' => 'Anyaggazdálkodás', 'annual_free_day' => 31, 'annual_sick_leave' => 15, 'start_free_days' => 21, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 101, 'name' => 'Hegyesi Michel', 'group' => 'Műszak2', 'job' => 'Nyomtatás', 'annual_free_day' => 17, 'annual_sick_leave' => 13, 'start_free_days' => 13, 'start_sick_leaves' => 3, 'start_overtimes' => 34],
			['nr' => 6, 'name' => 'Horváth László', 'group' => 'Szellemi1', 'job' => 'Termelésszervezés', 'annual_free_day' => 35, 'annual_sick_leave' => 15, 'start_free_days' => 25, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 51, 'name' => 'Izsó Imre', 'group' => 'Stúdió2', 'job' => 'Formakészítés', 'annual_free_day' => 21, 'annual_sick_leave' => 15, 'start_free_days' => 17, 'start_sick_leaves' => 0, 'start_overtimes' => 15],
			['nr' => 14, 'name' => 'Kiss László', 'group' => 'TMK 1', 'job' => 'TMK', 'annual_free_day' => 30, 'annual_sick_leave' => 15, 'start_free_days' => 23, 'start_sick_leaves' => 0, 'start_overtimes' => 15],
			['nr' => 117, 'name' => 'Kovan Nikolett', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 12, 'annual_sick_leave' => 9, 'start_free_days' => 9, 'start_sick_leaves' => 3, 'start_overtimes' => 45],
			['nr' => 27, 'name' => 'Kovács Éva', 'group' => 'Szellemi1', 'job' => 'Igazgatás', 'annual_free_day' => 30, 'annual_sick_leave' => 15, 'start_free_days' => 23, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 50, 'name' => 'Lipcsei Anikó', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 30, 'annual_sick_leave' => 15, 'start_free_days' => 27, 'start_sick_leaves' => 6, 'start_overtimes' => 36],
			['nr' => 47, 'name' => 'Mecinné', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 31, 'annual_sick_leave' => 15, 'start_free_days' => 22, 'start_sick_leaves' => 4, 'start_overtimes' => 55],
			['nr' => 40, 'name' => 'Mezei Katalin', 'group' => 'Szellemi1', 'job' => 'Igazgatás', 'annual_free_day' => 31, 'annual_sick_leave' => 15, 'start_free_days' => 25, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 44, 'name' => 'Mihalec György', 'group' => 'Műszak3', 'job' => 'Nyomtatás', 'annual_free_day' => 23, 'annual_sick_leave' => 15, 'start_free_days' => 14, 'start_sick_leaves' => 0, 'start_overtimes' => 27],
			['nr' => 16, 'name' => 'Péter Tiborné', 'group' => 'Szellemi1', 'job' => 'Igazgatás', 'annual_free_day' => 33, 'annual_sick_leave' => 15, 'start_free_days' => 28, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 9, 'name' => 'Popol Péter', 'group' => 'Műszak1', 'job' => 'Nyomtatás', 'annual_free_day' => 34, 'annual_sick_leave' => 15, 'start_free_days' => 24, 'start_sick_leaves' => 0, 'start_overtimes' => 43],
			['nr' => 17, 'name' => 'Rakusz János', 'group' => 'Stúdió1', 'job' => 'Formakészítés', 'annual_free_day' => 26, 'annual_sick_leave' => 15, 'start_free_days' => 21, 'start_sick_leaves' => 0, 'start_overtimes' => 10],
			['nr' => 33, 'name' => 'Sütő Norbert', 'group' => 'Műszak2', 'job' => 'Nyomtatás', 'annual_free_day' => 25, 'annual_sick_leave' => 15, 'start_free_days' => 19, 'start_sick_leaves' => 0, 'start_overtimes' => 44],
			['nr' => 113, 'name' => 'Sziegl Dávid Zoltán', 'group' => 'Műszak3', 'job' => 'Kötészet', 'annual_free_day' => 12, 'annual_sick_leave' => 9, 'start_free_days' => 10, 'start_sick_leaves' => 3, 'start_overtimes' => 49],
			['nr' => 31, 'name' => 'Sic Katalin', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 30, 'annual_sick_leave' => 15, 'start_free_days' => 25, 'start_sick_leaves' => 0, 'start_overtimes' => 45],
			['nr' => 49, 'name' => 'Tóth Éva', 'group' => 'Szellemi1', 'job' => 'Termelésszervezés', 'annual_free_day' => 22, 'annual_sick_leave' => 15, 'start_free_days' => 14, 'start_sick_leaves' => 2, 'start_overtimes' => 0],
			['nr' => 102, 'name' => 'Tóth Gergő', 'group' => 'Stúdió2', 'job' => 'Formakészítés', 'annual_free_day' => 16, 'annual_sick_leave' => 12, 'start_free_days' => 11, 'start_sick_leaves' => 0, 'start_overtimes' => 4],
			['nr' => 10, 'name' => 'Varga Lajos', 'group' => 'TMK 1', 'job' => 'TMK', 'annual_free_day' => 28, 'annual_sick_leave' => 15, 'start_free_days' => 23, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 37, 'name' => 'Varró Imre', 'group' => 'Szellemi1', 'job' => 'Igazgatás', 'annual_free_day' => 24, 'annual_sick_leave' => 15, 'start_free_days' => 16, 'start_sick_leaves' => 0, 'start_overtimes' => 0],
			['nr' => 11, 'name' => 'Viczián Gyuláné', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 30, 'annual_sick_leave' => 15, 'start_free_days' => 22, 'start_sick_leaves' => 8, 'start_overtimes' => 55],
			['nr' => 12, 'name' => 'Vidovenyecz László', 'group' => 'Műszak2', 'job' => 'Nyomtatás', 'annual_free_day' => 29, 'annual_sick_leave' => 15, 'start_free_days' => 20, 'start_sick_leaves' => 0, 'start_overtimes' => 28],
			['nr' => 34, 'name' => 'Zsombori Zsolt', 'group' => 'Rugalmas', 'job' => 'Kötészet', 'annual_free_day' => 22, 'annual_sick_leave' => 15, 'start_free_days' => 14, 'start_sick_leaves' => 0, 'start_overtimes' => 56],
		];

		$worker_cols = ['nr', 'name', 'group_id', 'job_id', 'annual_free_day', 'annual_sick_leave'];
		
		$_startdate = new \Carbon\Carbon('2017-10-01');
		
		foreach($workers as $worker)
		{
			$worker['group_id'] = \App\WorkerGroup::where('name', '=', $worker['group'])->pluck('id')->first();
			if (!$worker['group_id']) {
				dd('group', $worker);
			}
			
			$worker['job_id'] = \App\Job::where('name', '=', $worker['job'])->pluck('id')->first();
			if (!$worker['job_id']) {
				dd('job', $worker);
			}
			
			$__worker = array_intersect_key($worker, array_flip($worker_cols));
			$_worker = Factory(App\Worker::class)->create($__worker);
			$now = Carbon\Carbon::now();
			$now->subMonths(2);
			$now->endOfMonth();
			$wr = \App\Worker::find($_worker->id);
			
			$groupdate = $wr->group->getGroupDate($now);
			
			if ($worker['start_overtimes']) {
				for($i=4; $i<$worker['start_overtimes']; $i=$i+4) {
					do {
						$day = $now->format('Y-m-d');
						$status = $wr->getWorkerDay($day);
						if ($status->alias !== 'workday') {
							$now->subDay();
							$day = $now->format('Y-m-d');
						}
					} while($status->alias !== 'workday');
					$groupdate = $wr->group->getGroupDate($now);
					$groupstart = \Carbon\Carbon::parse($day . ' ' . $groupdate->start);
					$groupend = \Carbon\Carbon::parse($day . ' ' . $groupdate->end);
					if ($groupend < $groupstart) {
						$groupend->addDay();
					}
					
					$startdate = \Carbon\Carbon::parse($day . ' ' . $groupdate->start);
					$enddate = \Carbon\Carbon::parse($day . ' ' . $groupdate->end);
					if ($enddate < $startdate) {
						$enddate->addDay();
					}
					if ($enddate > \Carbon\Carbon::parse($day . ' 20:00:00')) {
						$check_in = $startdate->format('Y-m-d H:i:s');
						$check_out = $enddate->copy()->addHours(4)->format('Y-m-d H:i:s');
					} else {
						$check_in = $startdate->copy()->subHours(4)->format('Y-m-d H:i:s');
						$check_out = $enddate->format('Y-m-d H:i:s');
					}
					
					\App\Check::insert(['worker_id' => $_worker->id, 'check_in' => $check_in, 'check_out' => $check_out, 'configured_check_in' => $check_in, 'configured_check_out' => $check_out, 'configured_formal_start' => $groupstart, 'configured_formal_end' => $groupend]);
					$now->subDay();
					$groupdate = $wr->group->getGroupDate($now);
				}
				if (($i-4) < $worker['start_overtimes']) {
					$diff = $worker['start_overtimes'] - ($i-4);
					do {
						$day = $now->format('Y-m-d');
						$status = $wr->getWorkerDay($day);
						if ($status->alias != 'workday') {
							$now->subDay();
							$day = $now->format('Y-m-d');
						}
					} while($status->alias != 'workday');
					
					$groupdate = $wr->group->getGroupDate($now);
					$groupstart = \Carbon\Carbon::parse($day . ' ' . $groupdate->start);
					$groupend = \Carbon\Carbon::parse($day . ' ' . $groupdate->end);
					if ($groupend < $groupstart) {
						$groupend->addDay();
					}
					
					$startdate = \Carbon\Carbon::parse($day . ' ' . $groupdate->start);
					$enddate = \Carbon\Carbon::parse($day . ' ' . $groupdate->end);
					if ($enddate < $startdate) {
						$enddate->addDay();
					}
					if ($enddate > \Carbon\Carbon::parse($day . ' 20:00:00')) {
						$check_in = $startdate->format('Y-m-d H:i:s');
						$check_out = $enddate->copy()->addHours($diff)->format('Y-m-d H:i:s');
					} else {
						$check_in = $startdate->copy()->subHours($diff)->format('Y-m-d H:i:s');
						$check_out = $enddate->format('Y-m-d H:i:s');
					}
					
					\App\Check::insert(['worker_id' => $_worker->id, 'check_in' => $check_in, 'check_out' => $check_out, 'configured_check_in' => $check_in, 'configured_check_out' => $check_out, 'configured_formal_start' => $groupstart, 'configured_formal_end' => $groupend]);
					
					$now->subDay();
					$groupdate = $wr->group->getGroupDate($now);
				}
			}
			
			while ($now >= $_startdate) {
				$day = $now->format('Y-m-d');
				$status = $wr->getWorkerDay($day);
				$groupdate = $wr->group->getGroupDate($now);
				$groupstart = \Carbon\Carbon::parse($day . ' ' . $groupdate->start);
				$groupend = \Carbon\Carbon::parse($day . ' ' . $groupdate->end);
				if ($groupend < $groupstart) {
					$groupend->addDay();
				}
				
				if ($status->alias == 'workday') {
					\App\Check::insert(['worker_id' => $_worker->id, 'check_in' => $groupstart, 'check_out' => $groupend, 'configured_check_in' => $groupstart, 'configured_check_out' => $groupend, 'configured_formal_start' => $groupstart, 'configured_formal_end' => $groupend]);
				}
				$now->subDay();
			}
			
			$now2 = $now->copy()->startOfYear();
			
			if ($worker['start_free_days']) {
				for($i=0; $i<$worker['start_free_days']; $i++) {
					$day = $now2->format('Y-m-d');
					\App\RestrictedWorkerDate::insert(['worker_id' => $_worker->id, 'date' => $day, 'restricted_worker_date_status_id' => 2]);
					$now2->addDay();
				}
			}
			if ($worker['start_sick_leaves']) {
				for($i=0; $i<$worker['start_sick_leaves']; $i++) {
					$day = $now2->format('Y-m-d');
					\App\RestrictedWorkerDate::insert(['worker_id' => $_worker->id, 'date' => $day, 'restricted_worker_date_status_id' => 3]);
					$now2->addDay();
				}
			}
		}
		$closedmonth = App\ClosedMonth::first();
		$closedmonth->date = '2017-10-01';
		$closedmonth->save();
    }
}
