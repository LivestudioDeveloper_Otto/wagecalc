<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
			['name' => 'Délkelet-Press', 'overtime' => 9000, 'active' => 1],
		]);
    }
}
