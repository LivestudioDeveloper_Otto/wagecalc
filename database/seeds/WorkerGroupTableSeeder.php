<?php

use Illuminate\Database\Seeder;

class WorkerGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $worker_group = App\WorkerGroup::create([
			'name' => 'Műszak1',
			'protected' => 1
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '06:00',
				'end' => '14:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 1
			],
			[
				'group_id' => $worker_group->id,
				'start' => '14:00',
				'end' => '22:00',
				'ordering' => 2,
				'bonus' => 1,
				'protected' => 1
			],
			[
				'group_id' => $worker_group->id,
				'start' => '22:00',
				'end' => '06:00',
				'ordering' => 3,
				'bonus' => 1.15,
				'protected' => 1
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Műszak2',
			'protected' => 1
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '14:00',
				'end' => '22:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 1
			],
			[
				'group_id' => $worker_group->id,
				'start' => '22:00',
				'end' => '06:00',
				'ordering' => 3,
				'bonus' => 1.15,
				'protected' => 1
			],
			[
				'group_id' => $worker_group->id,
				'start' => '06:00',
				'end' => '14:00',
				'ordering' => 2,
				'bonus' => 1,
				'protected' => 1
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Műszak3',
			'protected' => 1
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '22:00',
				'end' => '06:00',
				'ordering' => 1,
				'bonus' => 1.15,
				'protected' => 1
			],
			[
				'group_id' => $worker_group->id,
				'start' => '06:00',
				'end' => '14:00',
				'ordering' => 2,
				'bonus' => 1,
				'protected' => 1
			],
			[
				'group_id' => $worker_group->id,
				'start' => '14:00',
				'end' => '22:00',
				'ordering' => 3,
				'bonus' => 1,
				'protected' => 1
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Szellemi1',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '08:00',
				'end' => '16:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Szellemi2',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '07:00',
				'end' => '15:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Takarítás',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '13:00',
				'end' => '19:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'TMK 1',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '06:00',
				'end' => '14:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'TMK 2',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '08:00',
				'end' => '16:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Stúdió1',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '06:00',
				'end' => '14:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Stúdió2',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '14:00',
				'end' => '22:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '22:00',
				'end' => '06:00',
				'ordering' => 2,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
        $worker_group = App\WorkerGroup::create([
			'name' => 'Rugalmas',
			'protected' => 0
		]);
		
		App\WorkerGroupDate::insert([
			[
				'group_id' => $worker_group->id,
				'start' => '06:00',
				'end' => '14:00',
				'ordering' => 1,
				'bonus' => 1,
				'protected' => 0
			]
		]);
		
    }
}
