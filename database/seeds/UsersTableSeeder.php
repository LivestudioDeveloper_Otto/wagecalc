<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'dev@livestudio.eu',
                'password' => bcrypt('123456')
            ],
            [
                'name' => 'Péter Erika',
                'email' => 'peter.erika@delkeletpress.hu',
                'password' => bcrypt('ekonyv')
            ],
            [
                'name' => 'Augusztin Mária',
                'email' => 'augusztin.maria@delkeletpress.hu',
                'password' => bcrypt('Ducika66')
            ],
            [
                'name' => 'Rakusz János',
                'email' => 'rakusz.janos@delkeletpress.hu',
                'password' => bcrypt('Janos123')
            ],
            [
                'name' => 'Művezetők',
                'email' => 'muvezetok@delkeletpress.hu',
                'password' => bcrypt('laci')
            ]
        ]);
    }
}
