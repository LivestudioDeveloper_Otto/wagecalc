<?php

use Illuminate\Database\Seeder;

class ChecksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$workers = App\Worker::all();
		foreach($workers as $worker)
		{
			$GLOBALS['worker'] = $worker;
			Factory(App\Check::class, 50)->create(['worker_id' => $worker->id]);
		}
		unset($GLOBALS['worker']);
    }
}
