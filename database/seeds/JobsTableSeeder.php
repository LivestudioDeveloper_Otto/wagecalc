<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
			['name' => 'Dolgozó', 'white_collar' => false],
			['name' => 'Kötészet', 'white_collar' => false],
			['name' => 'GYEK', 'white_collar' => false],
			['name' => 'Igazgatás', 'white_collar' => false],
			['name' => 'Nyomtatás', 'white_collar' => false],
			['name' => 'Anyaggazdálkodás', 'white_collar' => false],
			['name' => 'Termelésszervezés', 'white_collar' => false],
			['name' => 'Formakészítés', 'white_collar' => false],
			['name' => 'TMK', 'white_collar' => false],
		]);
    }
}
