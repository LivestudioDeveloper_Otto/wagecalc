<?php

use Illuminate\Database\Seeder;

class WorkerDateStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restricted_worker_date_statuses')->insert([
			['name' => 'Munkanap', 'alias' => 'workday'],
			['name' => 'Szabadság', 'alias' => 'free_day'],
			['name' => 'Betegszabadság', 'alias' => 'sick_leave']
		]);
    }
}
