<?php

use Illuminate\Database\Seeder;

class DateStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restricted_date_statuses')->insert([
			['name' => 'Munkanap', 'alias' => 'workday', 'default' => 31, 'bonus' => 1],
			['name' => 'Munkaszüneti nap', 'alias' => 'day_off', 'default' => 32, 'bonus' => 0],
			['name' => 'Munkaszüneti nap (Vasárnap)', 'alias' => 'day_off_sunday', 'default' => 64, 'bonus' => 1.5],
			['name' => 'Ünnepnap', 'alias' => 'holiday', 'default' => 0, 'bonus' => 2]
		]);
    }
}
