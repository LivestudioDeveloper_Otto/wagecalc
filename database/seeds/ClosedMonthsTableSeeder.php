<?php

use Illuminate\Database\Seeder;
use App\ClosedMonth;

class ClosedMonthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();
		$now->subMonths(3);
		$now->startOfMonth();
        ClosedMonth::insert([
			'date' => $now->toDateString(),
			'user_id' => 1,
			'shift_start' => '18:00',
			'shift_end' => '06:00',
			'night_start' => '22:00',
			'night_end' => '06:00'
		]);
    }
}
