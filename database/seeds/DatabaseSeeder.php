<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompanyTableSeeder::class);
        $this->call(DateStatusTableSeeder::class);
        $this->call(WorkerDateStatusTableSeeder::class);
        $this->call(WorkerGroupTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(GroupHistoryTableSeeder::class);
        $this->call(ClosedMonthsTableSeeder::class);
        $this->call(WorkerTableSeeder::class);
        //$this->call(ChecksTableSeeder::class);
    }
}
