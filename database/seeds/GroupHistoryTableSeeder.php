<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GroupHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group_history')->insert([
			'date' => '2017-01-01',
			'user_id' => 0,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
		]);
        DB::table('group_history')->insert([
			'date' => '2017-08-28',
			'user_id' => 0,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
		]);
    }
}
