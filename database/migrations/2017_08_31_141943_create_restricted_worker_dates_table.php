<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestrictedWorkerDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restricted_worker_dates', function (Blueprint $table) {
            $table->increments('id');
			$table->date('date');
			$table->unsignedInteger('worker_id');
			$table->unsignedTinyInteger('restricted_worker_date_status_id')->index();
			$table->unique(['date', 'worker_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restricted_worker_dates');
    }
}
