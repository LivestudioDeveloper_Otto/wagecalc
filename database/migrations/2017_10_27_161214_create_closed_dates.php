<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosedDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closed_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('closed_month_id')->unsigned()->index();
            $table->integer('check_id')->unsigned()->index();
            $table->unsignedTinyInteger('annual_free_day');
            $table->unsignedTinyInteger('annual_sick_leave');
            $table->unsignedTinyInteger('annual_sick_pay');
            $table->unsignedTinyInteger('overtime');
            $table->unsignedTinyInteger('bonus');
            $table->string('group', 150);
			$table->time('start');
			$table->time('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closed_dates');
    }
}
