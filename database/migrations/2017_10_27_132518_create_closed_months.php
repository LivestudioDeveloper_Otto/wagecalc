<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosedMonths extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closed_months', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->unique();
            $table->integer('user_id')->unsigned()->index();
			$table->time('shift_start');
			$table->time('shift_end');
			$table->time('night_start');
			$table->time('night_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closed_months');
    }
}
