<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('nr');
            $table->unsignedInteger('group_id');
            $table->string('name', 100);
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('job_id');
            $table->unsignedInteger('annual_free_day');
            $table->unsignedInteger('annual_sick_leave');
            $table->unsignedInteger('annual_sick_pay');
            $table->boolean('flexible');
            $table->boolean('performance_pay');
            $table->time('formal_start');
            $table->time('formal_end');
            $table->time('core_start');
            $table->time('core_end');
            $table->boolean('active');
            $table->timestamps();
            
            $table->unique('nr', 'nr_unique');
            $table->index('company_id', 'company_index');
            $table->index('job_id', 'job_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
