<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checks', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			
            $table->increments('id');
			$table->unsignedTinyInteger('worker_id')->index();
			//$table->unsignedTinyInteger('group_date_id');
			$table->dateTime('check_in')->nullable();
			$table->dateTime('check_out')->nullable();
			$table->dateTime('configured_check_in')->nullable();
			$table->dateTime('configured_check_out')->nullable();
			$table->dateTime('configured_formal_start')->nullable();
			$table->dateTime('configured_formal_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checks');
    }
}
