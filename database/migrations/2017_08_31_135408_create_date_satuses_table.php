<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateSatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restricted_date_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias', 15)->unique();
            $table->string('name', 50);
			$table->float('bonus', 4, 2);
			$table->unsignedTinyInteger('default')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restricted_date_statuses');
    }
}
