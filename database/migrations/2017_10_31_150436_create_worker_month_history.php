<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerMonthHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('worker_month_history', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('worker_id')->unsigned()->index();
            $table->integer('closed_month_id')->unsigned()->index();
            $table->unsignedTinyInteger('annual_free_day');
            $table->unsignedTinyInteger('annual_sick_leave');
            $table->unsignedTinyInteger('annual_sick_pay');
            $table->unsignedTinyInteger('overtime');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_month_history');
    }
}
