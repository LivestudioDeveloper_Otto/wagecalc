<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerGroupDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_group_dates', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedTinyInteger('group_id');
			$table->boolean('active')->default(1);
			$table->boolean('protected')->default(0);
			$table->time('start');
			$table->time('end');
			$table->float('bonus', 4, 2);
			$table->unsignedTinyInteger('ordering')->default(1);
			$table->unique(['group_id', 'ordering']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_group_dates');
    }
}
