<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Check::class, function (Faker $faker) {
	
	$check_in = $faker->dateTimeBetween('-20 days', 'now');
	$check_in_clone = clone $check_in;
	$check_in_clone->modify('+1 day');
	$check_in_clone->setTime(0, 0, 0);
	$check_out = $faker->dateTimeBetween($check_in, $check_in_clone);
	
    return [
        'worker_id' => 0,
        'check_in' => $check_in->format('Y-m-d H:i:s'),
        'check_out' => $check_out->format('Y-m-d H:i:s'),
        'configured_check_in' => $check_in->format('Y-m-d H:i:s'),
        'configured_check_out' => $check_out->format('Y-m-d H:i:s'),
        'configured_formal_start' => $check_in->format('Y-m-d') . ' ' . $GLOBALS['worker']->formal_start,
        'configured_formal_end' => $check_in->format('Y-m-d') . ' ' . $GLOBALS['worker']->formal_end
    ];
});
