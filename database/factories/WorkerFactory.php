<?php

use Faker\Generator as Faker;
use App\Worker as Worker;

$factory->define(Worker::class, function (Faker $faker) {
	
    $hours = $faker->numberBetween(6, 8);
    $formal_start_h = $faker->numberBetween(8, 10);
    $formal_end_h = $formal_start_h + $hours - 1;
	
    return [
        'nr' => $faker->unique()->numberBetween(),
        'group_id' => $faker->numberBetween(1, 11),
        'name' => $faker->name,
        'company_id' => 1,
        'job_id' => $faker->numberBetween(1, 9),
        'annual_free_day' => $faker->numberBetween(4, 7),
        'annual_sick_leave' => $faker->numberBetween(4, 7),
        'annual_sick_pay' => 0,
        'flexible' => $faker->numberBetween(0, 1),
        'performance_pay' => $faker->numberBetween(0, 1),
        'formal_start' => '00:00',
        'formal_end' => '00:00',
        'core_start' => '00:00',
        'core_end' => '00:00',
        'active' => 1
    ];
});
