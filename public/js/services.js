angular.module('app.services', [])
  
.factory('AppHelper', function($rootScope, appConfig) {
  return {

    setConfig: function(data){
      angular.extend(appConfig, data);
    },

    getConfig: function(key){
      if (!appConfig[key]){
        return false;
      }
      return appConfig[key];
    },

    setBaseUrl: function(url){
      if (url.slice(-1) != "/"){
        url += "/";
      }
      return appConfig.url = url;
    },

    getBaseUrl: function(){
      return appConfig.url;
    },
    
    getLength: function(data) {
      if (data){
        if( data instanceof Array ) {
          return data.length;
        }else{
          return Object.keys(data).length;
        }
      }
      return 0;
    },
    openPage: function(page, data) {
      //$state.go(page, data);
    },

    goBack: function() {
      
    },
    
    inArray: function(needle, haystack) {
      var length = haystack.length;
      for(var i = 0; i < length; i++) {
          if(haystack[i] == needle) return true;
      }
      return false;
    },
    
    empty: function(arr) {
      return this.getLength(arr)==0;
    },
    
    array_keys: function (myObject) {
      output = [];
      for(var key in myObject) {
          output.push(key);
      }
      return output;
    },
	
    newDate: function(date){
      if (date){
        return new Date(date);
      }
      return new Date();
    },
    
    get_time_difference: function (earlierDate, laterDate) {
        var oDiff = new Object();

        earlierDate = new Date(earlierDate);
        laterDate = new Date(laterDate);
      
        if (earlierDate.getTime()>laterDate.getTime()){
          laterDate.setDate(laterDate.getDate()+1);
        }

        //  Calculate Differences
        //  -------------------------------------------------------------------  //
        var nTotalDiff = laterDate.getTime() - earlierDate.getTime();

        oDiff.days = Math.floor(nTotalDiff / 1000 / 60 / 60 / 24);
        nTotalDiff -= oDiff.days * 1000 * 60 * 60 * 24;

        oDiff.hours = Math.floor(nTotalDiff / 1000 / 60 / 60);
        nTotalDiff -= oDiff.hours * 1000 * 60 * 60;

        oDiff.minutes = Math.floor(nTotalDiff / 1000 / 60);
        nTotalDiff -= oDiff.minutes * 1000 * 60;

        oDiff.seconds = Math.floor(nTotalDiff / 1000);
        //  -------------------------------------------------------------------  //

        //  Format Duration
        //  -------------------------------------------------------------------  //
        //  Format Hours
        var hourtext = '00';
        if (oDiff.days > 0){ hourtext = String(oDiff.days);}
        if (hourtext.length == 1){hourtext = '0' + hourtext};

        //  Format Minutes
        var mintext = '00';
        if (oDiff.minutes > 0){ mintext = String(oDiff.minutes);}
        if (mintext.length == 1) { mintext = '0' + mintext };

        //  Format Seconds
        var sectext = '00';
        if (oDiff.seconds > 0) { sectext = String(oDiff.seconds); }
        if (sectext.length == 1) { sectext = '0' + sectext };

        //  Set Duration
        var sDuration = hourtext + ':' + mintext + ':' + sectext;
        oDiff.duration = sDuration;
        //  -------------------------------------------------------------------  //

        return oDiff;
    }
  }
})

.factory('FormHelper', function($rootScope, appConfig) {
  return {

    validate: function(form){
      if (form.$invalid) {
        angular.forEach(form.$error, function (field) {
          angular.forEach(field, function(errorField){
            errorField.$setTouched();
          });
        });
        return false;
      }
      return true;
    },
    
    isInvalid: function(field){
      return (field.$invalid && field.$touched) ? true : false;
    }
		
  }
})

.factory('Ajax', function($rootScope, $http, $httpParamSerializerJQLike, $httpParamSerializer, appConfig, myDialog, myLoader, AppHelper, auth, errorHandler) {
  return {
    run: function(data) {

      if (typeof data.silent === "undefined"){
        data.silent = false;
      }
      if (typeof data.file === "undefined"){
        data.file = false;
      }
      
      if ((typeof data.loader === "undefined" || (data.loader && data.loader > 0)) && !data.silent){
        myLoader.show();
      }

      if (!data.params) {
        data.params = {};    
      }
    
      var formData;
      var headers = {};
      
      if (!data.externalUrl){
        angular.extend(data.params, {
          //csrf_name: appConfig.csrf_name,
          //csrf_value: appConfig.csrf_value,
          appVersion: appConfig.appVersion,
          appVersionCode: appConfig.appVersionCode,
          platform: appConfig.platform
        });
      }
    
      var url = data.url ? appConfig.url+data.url : data.externalUrl;
      
      /*if (data.params.video_id){
        url = "http://79.172.249.137/asd.php";
      }*/
      
      // Ha nem küldök fájlt
      if (!data.file){
        headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        formData = $httpParamSerializerJQLike(data.params);
      }
      
      // Ha fájlt is küldök
      else {
        headers['Content-Type'] = undefined;
        formData = data.params;
      }
    
      var http_params = {
        withCredentials: data.externalUrl ? false : true,
        headers: headers,
        method: (typeof data.method === undefined || data.method == "") ? 'get' : data.method,
        url: url,
        //data : (data.method != "get") ? formData : "",
        //params : (data.method == "get") ? data.params : "",
        timeout: 600000
      };
      
      if (data.method != "get"){
        http_params.data = formData;
      }else{
        http_params.params = data.params;
      }
      
      if (data.file){
        http_params.transformRequest = function (ret_data, headersGetter) {
          var form_data = new FormData();
          angular.forEach(ret_data, function (value, key) {
              form_data.append(key, value);
          });
          return form_data;
        };
      }
      
      return $http(http_params).then(function(result) {
        myLoader.hide();

        /*var csrf = JSON.parse(result.headers("X-CSRF-Token"));
        if (csrf) {
          appConfig.csrf_name = csrf.csrf_name;
          appConfig.csrf_value = csrf.csrf_value;
        }*/
        
        if (!result || !result.data){ return false; }

        if ((result.data.errorCode > 0 || (result.data.errorText && result.data.errorText != "")) && !data.silent){
          myDialog.error(result.data.errorText, "Hiba!");
          return false;
        }
        return result;
      }, 
      function(result) {
        myLoader.hide();
        
        if (result.data && result.data.errors) {
          errorHandler.set(result.data.errors);
        }
        
        var error = (result && result.data && result.data.message) ? result.data.message : "Az adatokat nem sikerült lekérdezni a központtól. Kérjük próbáld meg újra!";

        if (!data.silent) {
           myDialog.error(error, "Hiba!");
        }
        
        // Ha nincs jogosultsága, kiléptetjük
        if (result.status == 401 || result.status == 403){
          auth.logout().then(function(){
            AppHelper.openPage("app.login");
          });
        }

        return false;
      });
    }
  }
})

.factory('errorHandler', function() {
  
  var errors = [];
  
  return {

    set: function(errors) {
      this.errors = errors;
    },
    
    get: function(key){
      if (this.errors && typeof this.errors[key] !== "undefined"){
        return this.errors[key];
      }
      
      return false;
    },
    
    getAll: function(key){
      return this.errors;
    }
		
  }
})

.service('myLoader', function($rootScope) {
  
  var service = {};

  service.show = function() {
    jQuery("body").addClass("loading");
  };

  service.hide = function() {
    jQuery("body").removeClass("loading");
  };
	
  return service;
  
})

.factory('myDialog', function(toaster, $q) {
  return {

    error: function(body, title, options) {
      
      var toaster_options = {type: 'error', title: title, body: body};
      
      if (options){
        angular.extend(toaster_options, options);
      }
      
      toaster.pop(toaster_options);
    },
    
    success: function(body, title, options){
      
      var toaster_options = {type: 'success', title: title, body: body};
      
      if (options){
        angular.extend(toaster_options, options);
      }
      
      toaster.pop(toaster_options);
    },
    
    confirm: function(title, template){
      /*var deferred = $q.defer();

      var confirmPopup = $ionicPopup.confirm({
        title: title,
        template: template ? template : "",
        buttons: [
          { text: 'Nem' },
          {
            text: '<b>Igen</b>',
            type: 'button-balanced',
            onTap: function(e) {
              return e;
            }
          }
        ]
      });

      confirmPopup.then(function(res) {
        if(res) {
          deferred.resolve(true);
        } else {
          deferred.resolve(false);
        }
      });


      return deferred.promise;*/
    }
		
  }
})

.factory('auth', function($rootScope, $q) {
  return {
    
    init: function(){
      if (!$rootScope.$storage.user){
        $rootScope.$storage.user = {
          loggedin: 0
        }
      }
    },
    
    setUserData: function(data){
      angular.extend($rootScope.$storage.user, data);
    },
    
    getUserParam: function(key){
      if ($rootScope.$storage.user && typeof $rootScope.$storage.user !== "undefined"){
        return $rootScope.$storage.user[key];
      }
      return false;
    },

    isLoggedin: function() {
      loggedin = false;
      
      if ($rootScope.$storage.user && $rootScope.$storage.user.loggedin) {
        loggedin = true;
      }
      
      return loggedin;
    },
    
    login: function(userData) {
      var deferred = $q.defer();
      
      $rootScope.$storage.user.loggedin = 1;
      this.setUserData(userData);
      
      deferred.resolve(true);
      
      return deferred.promise;
    },
    
    logout: function() {
      var deferred = $q.defer();
      
      $rootScope.$storage.user = {
        loggedin: 0
      }
      
      deferred.resolve(true);
      
      return deferred.promise;
    }
    
    
  }
})

.factory('VideoSrv', function($rootScope, $q) {
  return {
    
    record: function() {
      var deferred = $q.defer();

      navigator.device.capture.captureVideo(
        // success
        function(mediaFiles) {
          deferred.resolve(mediaFiles);
        },
        // error
        function(error) {
            deferred.reject('Error code: ' + error.code, null, 'Capture Error');
        },
        { duration: 20 }
      );

      return deferred.promise;
    },
    
    delete: function(fullPath) {
      var deferred = $q.defer();
      
      window.resolveLocalFileSystemURL(fullPath, function(file) {
        file.remove(function(){
          console.log("file deleted");
          deferred.resolve(true);
        }, function(result){
          console.log("delete file error:", result);
          deferred.reject("Hiba történt: "+result);
        });
      }, 
      function(result){
        console.log("resolveLocalFileSystemURL error:", result);
        deferred.reject("Hiba történt: "+result);
      });
      
      return deferred.promise;
    }
    
    
  }
})

.factory('FileTransferSrv', function($rootScope, $q, appConfig, myLoader, myDialog) {
  return {
    
    // Szükséges:
    // (string) data.url -> ide megy a kérés
    // (string) data.file.url -> fájl elérhetősége (fullPath)
    // (string) data.file.key -> ebben a változóban kerül postolásra a fájl
    // (string) data.file.name -> fájl neve
    // (string) data.file.type -> fájl mimeType-ja
    
    // Opcionális:
    // (obj) data.params -> postolandó paraméterek
    // (string) data.externalUrl -> külső url cím (data.url helyett)
    
    upload: function(data) {
      myLoader.show();
      
      var deferred = $q.defer();

      var options = new FileUploadOptions();
      options.fileKey = data.file.key;
      options.fileName = data.file.name;
      options.mimeType = data.file.type;

      var params = {
        appVersion: appConfig.appVersion,
        appVersionCode: appConfig.appVersionCode,
        platform: appConfig.platform
      };
      
      if (data.params){
        angular.extend(params, data.params);
      }
      options.params = params;
      
      var url = data.url ? appConfig.url+data.url : data.externalUrl;

      var ft = new FileTransfer();
      
      ft.onprogress = function(progressEvent) {
        if (progressEvent.lengthComputable) {
          var percent = Math.ceil((progressEvent.loaded / progressEvent.total)*100);
          myLoader.setLoadingPercentage(percent); 
        } else {
          
        }
      };

      ft.upload(data.file.url, encodeURI(url), 
        // Success
        function (r) {
          myLoader.hide();
          deferred.resolve(JSON.parse(r.response));
        },
        // Error
        function (error) {
          myLoader.hide();
          myDialog.error("Hiba történt a feltöltés közben! Kérjük próbálja újra! ("+error.exception+")");
          console.log(error);
          deferred.reject(error);
        }, 
        options
      );

      return deferred.promise;
    }
    
    
  }
});