angular.module('app.filters', [])

.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }

    return input;
  };
})

.filter('roundTime', function() {
  return function(input) {

    if (!input || input == ""){
      return input;  
    }
    
    var arr = input.split(":");
    var hours = parseInt(arr[0]);
    var minutes = parseInt(arr[1]);
    
    if (minutes >= 30) {
      hours = hours+1;
    }
    minutes = "00";
    
    if (hours<10) {
      hours = "0"+hours;
    }
    
    /*if (hours==24){
      hours = "00";
    }*/
    
    return hours+":"+minutes;
    
  };
})
  
.filter('formatTime', function() {
  return function(input) {
    
    if (!input || input == ""){
      return input;  
    }
    
    var hours = 0;
    var minutes = 0;
    
    var arr = input.split(":");
    if (arr.length == 1){
      if (arr[0].length<=2){
        hours = parseInt(arr[0]);
        minutes = 0;
      }else {
        hours = parseInt(arr[0].charAt(0)+arr[0].charAt(1));
        minutes = parseInt(arr[0].charAt(2)+(arr[0].length>3?arr[0].charAt(3):""));
      }
    }
    else{
      hours = parseInt(arr[0]);
      minutes = parseInt(arr[1]);
    }

    if (hours<10) {
      hours = "0"+hours;
    }
    if (minutes<10) {
      minutes = "0"+minutes;
    }

    return hours+":"+minutes;
    
  };
})

.filter('msToTime', function() {
  return function(milliseconds) {

    var hours = milliseconds / (1000*60*60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    return h + ':' + m + ':' + s;
    
  };
});