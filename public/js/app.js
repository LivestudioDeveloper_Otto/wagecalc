//moment().tz("Europe/Budapest");

var app = angular.module("app", ['app.controllers', 'app.services', 'app.filters', 'ui.bootstrap'])

.config(function($interpolateProvider, $rootScopeProvider){
    $interpolateProvider.startSymbol('{[').endSymbol(']}');
})

.constant("appConfig", {
    "platform": "pc", 
    "url": "",
    "appVersion": "0.0.1",
    "appVersionCode": 1
})

.run(function($window, $rootScope, appConfig, AppHelper) {
  
  appConfig.platform = $window.PLATFORM; // Átadjuk a configot külső változóból
  appConfig.url = $window.PRODUCTION ? "http://79.172.249.137/api/" : "http://192.168.1.99:8000/api/";
  
  //$rootScope.$storage = $localStorage.$default({});

});
