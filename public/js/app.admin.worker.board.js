angular.module("app.services").service("admin_worker_board_srv", function($rootScope, $filter) {

  var service = {};
  service.data = {};
  service.worker = {};
  service.info = {};
  service.statuses = [];
  service.checkedItems = [];
  
  service.setData = function(data){
    console.log("months", data);
    service.data = data;
  }
    
  service.setWorker = function(data){
    console.log("worker", data);
    service.worker = data;
  }    
  
  service.setInfo = function(data){
    console.log("info", data);
    service.info = data;
  }
	
	service.getInfo = function(param){
		return service.info[param];
	}
  
  service.setStatuses = function(data) {
    angular.forEach(data, function(item, key){
      angular.extend(item, {id:key});
      service.statuses.push(item);
    });
  }
  
  service.getStatus = function(status_id) {
    return $filter("filter")(service.statuses, {id:status_id}, true)[0];
  }
  
  service.uncheckItems = function() {
    
    angular.forEach(service.data, function(week, wkey){
      angular.forEach(week, function(day, dkey){
        day.configure=false;
      });
    });
      
    service.data.checkedItems = [];
    
  }

  return service;
  
});



angular.module("app.controllers").controller("admin_worker_board_ctrl", function($scope, $filter, admin_worker_board_srv, AppHelper, $uibModal, $document, $timeout) {
    
  $scope.awb_srv = admin_worker_board_srv;
  $scope.AppHelper = AppHelper;
  
  $scope.modalInstance = null;

  // Visszaadja a kijelölt elemek indexeit, hoyg később tudjak rájuk hivatkozni
  $scope.getCheckedIndexes = function(){
    var ret = [];
    
    angular.forEach($scope.awb_srv.data, function(week, wkey){
      angular.forEach(week, function(day, dkey){
        if (day.configure){
          if (!ret[wkey]){
            ret[wkey] = [];
          }
          ret[wkey].push(dkey);
        }
      });
    });
    
    return ret;
  }
  
  // Kezdőpozíióba állítja a dokkolási időket (popup felnyitásakor)
  $scope.resetConfiguredCheckData = function() {
    
    angular.forEach($scope.awb_srv.checkedItems, function(week, wkey){
      angular.forEach(week, function(dayIndex){
        if (!$scope.awb_srv.data[wkey][dayIndex].temp.configured) {
          $scope.awb_srv.data[wkey][dayIndex].temp.configured = {};
        }
        
        $scope.awb_srv.data[wkey][dayIndex].temp.configured.check = angular.copy($scope.awb_srv.data[wkey][dayIndex].configured.check ? $scope.awb_srv.data[wkey][dayIndex].configured.check : $scope.awb_srv.data[wkey][dayIndex].check);
      });
    });
    
  }
  
  $scope.timeDiff = function(data){
    if (data.check_in && data.check_out){ 
      var start_time = $filter("date")(AppHelper.newDate(), "yyyy-MM-dd")+" "+data.check_in;
      var end_time = $filter("date")(AppHelper.newDate(), "yyyy-MM-dd")+" "+data.check_out;
      var diff = AppHelper.get_time_difference(start_time, end_time);
      return diff.hours+":"+diff.minutes;
    }
    return false;
  }
   
  $scope.singleEdit = function(item){
    $scope.awb_srv.uncheckItems();
    $timeout(function(){
      item.configure = true;
      $scope.openModal('configureDateChecks');
    }, 100);
  }

  $scope.openModal = function (id) {
    
    $timeout(function(){
      if (id=="configureDateChecks"){
        $scope.resetConfiguredCheckData();
      }
    
      var parentElem = angular.element($document[0].querySelector('body .modal-parent'));

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: id+'.html',
        controller: 'admin_worker_board_modal_ctrl',
        controllerAs: '$scope',
        appendTo: parentElem
      });
      
      modalInstance.result.then(function () {

      }, function () {
        $scope.awb_srv.uncheckItems();
      });
    }, 100);
    
  };
  
  // Visszaadja az adott státuszú napok mennyiségét
  $scope.countDays = function(status){
    var count = 0;
    
    angular.forEach($scope.awb_srv.data, function(week, wkey){
      angular.forEach(week, function(day, dkey) {
        if (day.configured && day.configured.status && day.configured.status.alias){
          if (day.configured.status.alias == status){
            ++count;
          }
        }else{
          if (day.restricted && day.restricted.alias){    
            if (day.restricted.alias == status){
              ++count;
            }
          }
        }
      });
    });
    
    return count;
  }
  
  $scope.getOverTime = function(day){
    
    overTime = $scope.calculateOverTime(day);                          
    if (overTime) {
      return overTime.format("HH:mm");
    }
    
    return null;
    
  }
  
	$scope.getWorkedHours = function(){
    console.log("getWorkedHours");
		var sum = 0;
		
		angular.forEach($scope.awb_srv.data, function(week, wkey){
      angular.forEach(week, function(day, dkey) {
				if (typeof day.configured === "undefined" || typeof day.configured.check === "undefined" || (typeof day.configured.check.check_in === "undefined" && typeof day.configured.check.check_out === "undefined")){
					if (day.check && day.check.rounded_diff){
						sum = sum+parseInt(day.check.rounded_diff);
          }
          
          if (day.restricted && (day.restricted.alias == "free_day" || day.restricted.alias == "unpaid_leave" || day.restricted.alias == "sick_pay" || day.restricted.alias == "free_leave" || day.restricted.alias == "proven_day")) {
            diff = $filter("roundTime")($scope.timeDiff({check_in:day.groupdate.start, check_out:day.groupdate.end}));
            sum = sum+parseInt(diff);
          }
        } 
        else {
					if (typeof day.configured !== "undefined" && typeof day.configured.check !== "undefined"){
						if (day.configured.check.check_in && day.configured.check.check_out){
							diff = $filter("roundTime")($scope.timeDiff(day.configured.check));
							sum = sum+parseInt(diff);
            }
          }
        }
        
        // ha módosítja a státuszt
        if (typeof day.configured !== "undefined" && typeof day.configured.status !== "undefined" && typeof day.configured.status.alias !== "undefined" ){
          if (day.configured.status.alias == "free_day" || day.configured.status.alias == "unpaid_leave" || day.configured.status.alias == "sick_pay" || day.configured.status.alias == "free_leave" || day.configured.status.alias == "proven_day") {
            diff = $filter("roundTime")($scope.timeDiff({check_in:day.groupdate.start, check_out:day.groupdate.end}));
            sum = sum+parseInt(diff);
          }
        }
			});
		});
		console.log(sum);
		return sum;
	}
	
  $scope.calculateOverTime = function(day){
    if (day.dt && day.dt.date) {
      var date = moment(day.dt.date).format("YYYY-MM-DD");
      var $end_date = moment();
      var $fs_dt, $fe_dt, $ci_dt, $co_dt = moment();
      var check = false;

      if (day.configured && day.configured.check && day.configured.check.formal_start && day.configured.check.check_in){
        $fs_dt = moment(date+" "+day.configured.check.formal_start+":00");
        $fe_dt = moment(date+" "+day.configured.check.formal_end+":00");
        $ci_dt = moment(date+" "+$filter("roundTime")(day.configured.check.check_in)+":00");
        $co_dt = moment(date+" "+$filter("roundTime")(day.configured.check.check_out)+":00");
        check = true;
      }else{
        if (day.check && day.check.check_in && day.check.formal_start){
          $fs_dt = moment(date+" "+day.check.formal_start+":00");
          $fe_dt = moment(date+" "+day.check.formal_end+":00");
          $ci_dt = moment(date+" "+$filter("roundTime")(day.check.check_in)+":00");
          $co_dt = moment(date+" "+$filter("roundTime")(day.check.check_out)+":00");
          check = true;
        }
      }

      if (check){
        if ($fs_dt > $fe_dt) {
          $fe_dt.add(1, "days");
        }

        if ($ci_dt > $co_dt) {	
          $co_dt.add(1, "days");
        }

        if ($fs_dt > $ci_dt && $fs_dt > $co_dt || $fe_dt < $ci_dt && $fe_dt < $co_dt) {
           $end_date.add($ci_dt.diff($co_dt), 'milliseconds');
        } else {
          if ($ci_dt < $fs_dt) {
            $end_date.add($ci_dt.diff($fs_dt), 'milliseconds');
          }

          if ($fe_dt < $co_dt) {
            $end_date.add($fe_dt.diff($co_dt), 'milliseconds');
          }
        }

        return moment(moment().diff($end_date));
        //return moment(moment().diff($end_date)).subtract(1, 'hours').format("HH:mm");
      }
      
      return null;
    }
    
    return null;
  }
  
	// Elavult, nem használjuk
  // Visszaadja a havi túlórák mennyiségét órákban
  $scope.getMonthlyOvertimes = function() {
    var duration = moment.duration();
    
    angular.forEach($scope.awb_srv.data, function(week, wkey){
      angular.forEach(week, function(day, dkey) {
        
        overTime = angular.copy($scope.calculateOverTime(day));
        if (overTime) {
          duration.add(overTime.valueOf());
        }

      });
    });
    return duration.asHours();
  }
  
  $scope.$watch('awb_srv.data', function(newValue) {
    
    $scope.awb_srv.checkedItems = $scope.getCheckedIndexes();
    
  }, true);
  
})

angular.module("app.controllers").controller("admin_worker_board_modal_ctrl", function($scope, admin_worker_board_srv, AppHelper, $uibModalInstance, $filter) {
  
  $scope.awb_srv = admin_worker_board_srv;
  $scope.AppHelper = AppHelper;
  $scope.tempData = {};
  
  $scope.changeStatus = function(status_id){
    var status = $scope.awb_srv.getStatus(status_id);
    
     angular.forEach($scope.awb_srv.checkedItems, function(week, wkey){
      angular.forEach(week, function(dayIndex){
        $scope.awb_srv.data[wkey][dayIndex].configured['status']=status;
      });
    });
    
    $scope.tempData.newStatus = ''; // Alaphelyzetbe állítjuk a selectet
    $scope.awb_srv.uncheckItems(); // Kicsekkoljuk az elemeket
    $scope.closeModal();
  }
  
  $scope.formatTime = function(wkey, dayIndex, key){
    $scope.awb_srv.data[wkey][dayIndex].temp.configured.check[key] = $filter("formatTime")($scope.awb_srv.data[wkey][dayIndex].temp.configured.check[key]);
  }
  
  // Menti a beállított dokkolási időket
  $scope.saveConfiguredCheckData = function(){
    angular.forEach($scope.awb_srv.checkedItems, function(week, wkey){
      angular.forEach(week, function(dayIndex){
        if ($scope.awb_srv.data[wkey][dayIndex].temp.configured.check){
          var tempData = angular.copy($scope.awb_srv.data[wkey][dayIndex].temp.configured.check);
          tempData.check_in = $filter("formatTime")(tempData.check_in);
          tempData.check_out = $filter("formatTime")(tempData.check_out);
          tempData.formal_start = $filter("formatTime")(tempData.formal_start);
          tempData.formal_end = $filter("formatTime")(tempData.formal_end);
          console.log("tempData", tempData);
          $scope.awb_srv.data[wkey][dayIndex].configured.check = tempData;
        }
        delete $scope.awb_srv.data[wkey][dayIndex].temp.configured.check;
      });
    });
    $scope.awb_srv.uncheckItems();
    $scope.closeModal();
  }
  
  $scope.closeModal = function() {
    $scope.awb_srv.uncheckItems();
    $uibModalInstance.close();
  }
  
});