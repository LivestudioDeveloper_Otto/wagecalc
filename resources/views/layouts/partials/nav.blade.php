<nav class="navbar navbar-expand-md p-0 m-0">
	@if (!Auth::check())
		<a class="navbar-brand" href="#">
			<img src="{{asset('logo.svg')}}" class="logo" class="d-inline-block align-top" alt="">
		</a>
	@endif
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto w-100 d-flex justify-content-end">
        @guest
					{{--<li class="nav-item dropdown active">
						<a class="nav-link dropdown-toggle" href="{{ route('dashboard') }}">Főoldal</a>
					</li>--}}
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">Belépés</a>
					</li>
				@else
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menü</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="{{ route('dashboard') }}">Főoldal</a>
							<a class="dropdown-item" href="{{ route('restricted_date.view') }}">Napok felülbírálása</a>
							<a class="dropdown-item" href="{{ route('worker_group.view') }}">Csoportok</a>
							<a class="dropdown-item" href="{{ route('workerboard') }}">Dolgozók</a>
							<a class="dropdown-item" href="{{ route('company.view') }}">Részlegek</a>
							<a class="dropdown-item" href="{{ route('job.view') }}">Beosztások</a>
							<a class="dropdown-item" href="{{ route('check.index') }}">Input fájl betöltés</a>
							<a class="dropdown-item" href="{{ route('closed_month.index') }}">Lezárt hónapok</a>
							<a class="dropdown-item text-danger" href="{{ route('auth.logout') }}">Kijelentkezés</a>
						</div>
					</li>
        @endguest
    </ul>
  </div>
</nav>