<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('favicon.ico')}}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/AngularJS-Toaster-master/toaster.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/ui-bootstrap-angular/ui-bootstrap-custom-2.5.0-csp.css') }}" rel="stylesheet">
    
    <link href="{{ asset('fonts/icomoon/style.css') }}" rel="stylesheet">
	
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    
    @yield('stylesheets') 
  </head>

  <body ng-app="app" ng-controller="mainCtrl" class="d-flex align-items-end flex-column">
    
    <div class="loader-wrap">
      <div class="loader">
          <svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>
      <span class="txt">Töltés...</span>
	</div>


    <header class="bg-light mb-4 w-100">
      <div class="container">
        <div class="row  align-items-center pt-2 pb-2">
					@if (Auth::check())
          <div class="col-md-7">
				
						<div class="row">
							<div class="col-md-4">
								<a class="navbar-brand" href="#">
									<img src="{{asset('logo.svg')}}" class="logo" class="d-inline-block align-top" alt="">
								</a>
							</div>
							<div class="col-md-8">
								<div class="float-left">
										{{ Form::open(['route' => 'setsessiondate', 'method' => 'post', 'class' => 'form-horizontal']) }}
											 
											 <div class="input-group">
												{{ Form::text('year', Session::get('sessionDate')['year'], ['class' => 'form-control', 'placeholder' => 'Év']) }}
												<span class="input-group-addon">-</span>
												{{ Form::text('month', Session::get('sessionDate')['month'], ['class' => 'form-control', 'placeholder' => 'Hónap']) }}
												<span class="input-group-btn">
													{{ Form::submit('Kiválaszt', ['class' => 'btn btn-success']) }}
												</span>
											</div>
											
										{{ Form::close() }}
								</div>
							</div>
						</div>
						

              
          </div>
					@endif
          <div class="col">
            <div class="row">
              <div class="col text-md-right">
                @include('layouts.partials.nav')
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="container mb-4">

      <div class="row row-offcanvas row-offcanvas-right">


        @hasSection('sidebar')
          <div class="col-6 col-md-3 sidebar-offcanvas pr-2" id="sidebar">
            @yield('sidebar') 
          </div>
        @endif

        <div class="col-12 @hasSection('sidebar') pl-2 col-md-9 @endif">
           @yield('content') 
        </div>

      </div>

    </div>

    <footer class="bg-light p-3 mt-4 w-100 mt-auto">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col">Minden jog fenntartva!   ©  2017</div>
          <div class="col text-md-right">A rendszert a <a href="http://livestudio.eu" target="_blank">LIVESTUDIO</a> csapata készítette!</div>
        </div>
      </div>
    </footer>

    <script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('vendor/popper/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/angular-1.6.6/angular.min.js') }}"></script>
    
    <script src="{{ asset('vendor/AngularJS-Toaster-master/toaster.min.js') }}"></script>
    <script src="{{ asset('vendor/ui-bootstrap-angular/ui-bootstrap-custom-2.5.0.min.js') }}"></script>
    <script src="{{ asset('vendor/ui-bootstrap-angular/ui-bootstrap-custom-tpls-2.5.0.min.js') }}"></script>
    <script src="{{ asset('vendor/momentjs/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/momentjs/hu.js') }}"></script>
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/controllers.js') }}"></script>
    <script src="{{ asset('js/services.js') }}"></script>
    <script src="{{ asset('js/filters.js') }}"></script>
    
    @yield('scripts') 
  </body>
</html>