@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Napok felülbírálása
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="m-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>	
						<div class="row form-group">
							<div class="col-md-12">
								{!! Form::open(['action' => 'RestrictedDateController@create', 'class' => 'form-inline']) !!}
									<div class="form-row align-items-center">
										<div class="col-auto form-group">
											{!! Form::Label('date', 'Dátum', ['class' => 'mr-sm-2']) !!}
											{!! Form::date('date', $date->format('Y-m-d'), ['class' => 'form-control mb-2 mb-sm-0']) !!}
										</div>
										<div class="col-auto form-group">
											{!! Form::Label('restricted_date_status_id', 'Dátum típusa', ['class' => 'mr-sm-2']) !!}
											{!! Form::select('restricted_date_status_id', $restricted_date_status_options, null, ['class' => 'custom-select mb-2 mb-sm-0']) !!}
										</div>
										<div class="col-auto form-group">
											{!! Form::submit('Mentés', ['class' => 'btn btn-primary']) !!}
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Dátum</th>
										<th>Státusz</th>
										<th>Művelet</th>
									</tr>
								</thead>
								<tbody>
									@foreach($restricted_dates as $restricted_date)
										<tr>
											<td>
												{{ $restricted_date->date }}
											</td>
											<td>
												{{ $restricted_date->status->name }}
											</td>
											<td>
												{!! link_to_action('RestrictedDateController@delete', 'Törlés', ['id' => $restricted_date->id], ['class' => 'btn btn-sm btn-danger']) !!}
											</td>	
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
