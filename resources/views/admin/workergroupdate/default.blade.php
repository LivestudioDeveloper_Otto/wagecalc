@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							<strong>{{ $worker_group->name }}</strong> műszakok kezelése
							{!! link_to_action('WorkerGroupDateController@edit', 'Műszak létrehozás', ['group_id' => $worker_group->id, 'id' => 0], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('WorkerGroupController@view', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Kezdés idő</th>
										<th>Zárás idő</th>
										<th>Aktív</th>
										<th>Rendezés</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($worker_group_dates as $worker_group_date)
										<tr>
											<td>
												{{ $worker_group_date->start }}
											</td>
											<td>
												{{ $worker_group_date->end }}
											</td>
											<td>
												{{ $worker_group_date->active ? "Igen" : "Nem" }}
											</td>
											<td>
												{{ $worker_group_date->ordering }}
											</td>
											<td>
												@if(!$worker_group_date->protected)
													{!! link_to_action('WorkerGroupDateController@delete', 'Törlés', ['group_id' => $worker_group->id, 'id' => $worker_group_date->id], ['class' => 'btn btn-sm btn-danger']) !!}
													{!! link_to_action('WorkerGroupDateController@edit', 'Szerkesztés', ['group_id' => $worker_group->id, 'id' => $worker_group_date->id], ['class' => 'btn btn-sm btn-primary']) !!}
												@else
													Ez egy alapértelmezett műszak
												@endif
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
