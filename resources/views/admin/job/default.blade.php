@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Beosztások kezelése
							{{ link_to_action('JobController@edit', 'Beosztás létrehozás', [], ['class' => 'btn btn-sm btn-secondary']) }}
						</div>
						<div class="col-md-6 text-right">
							{{ link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) }}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Név</th>
										<th>Szellemi</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($jobs as $job)
										<tr>
											<td>
												{{ $job->name }}
											</td>
											<td>
												{{ $job->white_collar ? "Igen" : "Nem" }}
											</td>
											<td>
												{{ link_to_action('JobController@delete', 'Törlés', ['id' => $job->id], ['class' => 'btn btn-sm btn-danger']) }}
												{{ link_to_action('JobController@edit', 'Szerkesztés', ['id' => $job->id], ['class' => 'btn btn-sm btn-primary']) }}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
