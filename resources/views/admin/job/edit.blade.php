@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Beosztás kezelése
						</div>
						<div class="col-md-6 text-right">
							{{ link_to_action('JobController@view', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) }}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								{{ Form::model($job, ['action' => 'JobController@create', 'method' => 'post']) }}
									{{ Form::hidden('id') }}
									<div class="form-group row">
											{{ Form::label('name', 'Név', ["class"=>"col-sm-2 col-form-label"]) }}
                                        <div class="col-sm-10">
											{{ Form::text('name', null, ["class"=>"form-control"]) }}
										</div>
									</div>
									<div class="form-group row">
                                        {{ Form::label('white_collar', 'Szellemi', ["class"=>"col-sm-2 col-form-label"]) }}
										<div class="col-sm-10">
                                          <div class="form-check form-check-inline">
                                            <label class="form-check-label pl-0">
                                              {{ Form::radio('white_collar', 1, ["class"=>"form-check-input"]) }} Igen
                                            </label>
                                          </div>
                                          <div class="form-check form-check-inline">
                                            <label class="form-check-label pl-0">
                                              {{ Form::radio('white_collar', 0, ["class"=>"form-check-input"]) }} Nem
                                            </label>
                                          </div>
										</div>
									</div>
									{{ Form::submit('Mentés', ['class' => 'btn btn-success']) }}
								{{ Form::close() }}
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
