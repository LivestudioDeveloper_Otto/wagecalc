@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Csoportok kezelése
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>Név</th>
										<th>Aktív</th>
										<th>Dolgozók</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($companies as $company)
										<tr>
											<td>
												{!! link_to_action('WorkerBoardController@board', $company->name, ['id' => $company->id], ['class' => 'btn btn-link']) !!}
											</td>
											<td>
												{{ $company->active ? "Igen" : "Nem" }}
											</td>
											<td>
												{{ $company->workers->count() }}
											</td>
											<td>
											{{-- {!! link_to_action('CompanyController@delete', 'x', ['id' => $company->id], ['class' => 'btn btn-link']) !!}
											{!! link_to_action('CompanyController@edit', 'e', ['id' => $company->id], ['class' => 'btn btn-link']) !!} --}}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
