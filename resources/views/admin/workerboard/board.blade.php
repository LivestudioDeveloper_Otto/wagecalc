@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Munkatárs táblázat
							{{ link_to_action('WorkerController@edit', 'Munkatárs hozzáadása', [], ['class' => 'btn btn-sm btn-secondary']) }}
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Név</th>
										<th>Beosztás</th>
										<th>Egyedi napok</th>
										<th>Munkacsoport</th>
										<th>Művelet</th>
									</tr>
								</thead>
								<tbody>
									@foreach($workers as $worker)
										<tr class="worker-{{ (!$worker->active ? 'in':'') }}active {{ $worker->getInvalidChecks($date)->count() > 0 ? 'table-danger':'' }}">
											<td>{{ $worker->name }}</td>
											<td>{{ $worker->job->name }}</td>
											<td>
												{{ $worker->restrictedDates->count() }} db
												{{-- @foreach($worker->restrictedDates as $restricted_date)
													{{ $restricted_date->status->name }}: {{ $restricted_date->date }}<br>
												@endforeach --}}
											</td>
											<td>{{ $worker->group->name }}</td>
											<td>
												{{ link_to_action('WorkerController@board', 'Belépés', ['id' => $worker->id], ['class' => 'btn btn-sm btn-success']) }}
												@if (!isset($company_id))
													{{ link_to_route('worker.edit', 'Szerkesztés', ['id' => $worker->id], ['class' => 'btn btn-sm btn-primary']) }}
												@endif
												{{ link_to_route('worker.export', 'Exportálás', ['id' => $worker->id], ['class' => 'btn btn-sm btn-secondary']) }}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection