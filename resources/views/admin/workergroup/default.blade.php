@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Csoportok kezelése 
							{!! link_to_action('WorkerGroupController@edit', 'Csoport létrehozás', [], ['class' => 'btn btn-sm btn-secondary']) !!}
							{!! link_to_action('GroupHistoryController@view', 'Csoport műszak újrakezdés', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="m-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Név</th>
										<th>Aktív</th>
										<th>Műszakok</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($worker_groups as $worker_group)
										<tr>
											<td>
												{{ $worker_group->name }}
											</td>
											<td>
												{{ $worker_group->active ? "Igen" : "Nem" }}
											</td>
											<td>
												{!! link_to_action('WorkerGroupDateController@view', $worker_group->dates->count() . ' db', ['id' => $worker_group->id], ['class' => '']) !!}
											</td>
											<td>
												@if(!$worker_group->protected)
													{!! link_to_action('WorkerGroupController@delete', 'Törlés', ['id' => $worker_group->id], ['class' => 'btn btn-sm btn-danger']) !!}
													{!! link_to_action('WorkerGroupController@edit', 'Szerkesztés', ['id' => $worker_group->id], ['class' => 'btn btn-sm btn-primary']) !!}
												@else
													Ez egy alapértelmezett csoport
												@endif
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
