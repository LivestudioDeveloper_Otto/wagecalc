@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-3">
							Lezárt hónapok listája
						</div>
						<div class="col-md-6">
							{!! link_to_action('ClosedMonthController@close', 'Aktuális hónap zárása', [], ['class' => 'btn btn-sm btn-danger', 'onClick' => 'return confirm(\'Biztosan elindítja a zárást?\');']) !!}
							{!! link_to_action('ClosedMonthController@open', 'Utolsó hónap feloldása', [], ['class' => 'btn btn-sm btn-warning', 'onClick' => 'return confirm(\'Biztosan feloldja az utolsó lezárt hónapot?\');']) !!}
						</div>
						<div class="col-md-3 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Dátum</th>
										<th>Záró felhasználó</th>
										<th>Feldolgozott adat</th>
										<th>Zárva</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($closed_months as $closed_month)
										<tr>
											<td>
												{{ $closed_month->date }}
											</td>
											<td>
												{{ $closed_month->user->name }}
											</td>
											<td>
												{{ link_to_action('ClosedMonthController@view', $closed_month->dates->count() . ' db', ['id' => $closed_month->id], ['class' => '']) }}
											</td>
											<td>
												{{ $closed_month->created_at->format('Y-m-d H:i:s') }}
											</td>
											<td>
												{{ link_to_action('ClosedMonthController@view', 'Megtekintés', ['id' => $closed_month->id], ['class' => 'btn btn-sm btn-success']) }}
												{{ link_to_route('closed_month.download_sum', 'Összesítés letöltése', [$closed_month->id], ['class' => 'btn btn-sm btn-primary']) }}
												{{ link_to_route('closed_month.download_zip', 'Összes fájl letöltése', [$closed_month->id], ['class' => 'btn btn-sm btn-warning']) }}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
