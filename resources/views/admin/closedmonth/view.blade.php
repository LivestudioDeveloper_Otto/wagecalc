@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Lezárt hónap: {{ $closed_month->date }}
						</div>
						<div class="col-md-6 text-right">
							{{ link_to_route('closed_month.download_sum', 'Összesítés letöltése', [$closed_month->id], ['class' => 'btn btn-sm btn-primary']) }}
							{{ link_to_route('closed_month.download_zip', 'Összes fájl letöltése', [$closed_month->id], ['class' => 'btn btn-sm btn-warning']) }}
							{{ link_to_action('ClosedMonthController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) }}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Név</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($workers as $worker)
										<tr>
											<td>
												{{ $worker->name }}
											</td>
											<td>
												{{-- {!! link_to_action('ClosedMonthController@view', 'Megtekintés', ['id' => $closed_month->id], ['class' => 'btn btn-sm btn-success']) !!} --}}
												{{ link_to_route('closed_month.download', 'Letöltés', [$closed_month->id, $worker->id], ['class' => 'btn btn-sm btn-success']) }}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
