@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<div class="card card-default">
				<div class="card-header">Vezérlőpult</div>
				<div class="card-body text-center">
					{{ link_to_action('RestrictedDateController@view', 'Napok felülbírálása', [], ['class' => 'btn btn-primary']) }}
					{{ link_to_action('WorkerGroupController@view', 'Csoportok', [], ['class' => 'btn btn-primary']) }}
					{{ link_to_action('WorkerBoardController@view', 'Dolgozók', [], ['class' => 'btn btn-primary']) }}
					{{ link_to_action('CompanyController@view', 'Részlegek', [], ['class' => 'btn btn-primary']) }}
					{{ link_to_action('JobController@view', 'Beosztások', [], ['class' => 'btn btn-primary']) }}
					{{ link_to_route('check.index', 'Input fájl betöltés', [], ['class' => 'btn btn-primary']) }}
					{!! link_to_action('ClosedMonthController@index', 'Lezárt hónapok', [], ['class' => 'btn btn-primary']) !!}
					{{ link_to_route('auth.logout', 'Kijelentkezés', [], ['class' => 'btn btn-danger']) }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
