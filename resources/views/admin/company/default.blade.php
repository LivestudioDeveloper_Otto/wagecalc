@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Részlegek kezelése
							{!! link_to_action('CompanyController@edit', 'Részleg létrehozás', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Név</th>
										<th>Aktív</th>
										<th>Dolgozók</th>
										<th>Műveletek</th>
									</tr>
								</thead>
								<tbody>
									@foreach($companies as $company)
										<tr>
											<td>
												{{ $company->name }}
											</td>
											<td>
												{{ $company->active ? "Igen" : "Nem" }}
											</td>
											<td>
												{!! link_to_action('WorkerBoardController@board', $company->workers->count() . ' fő', ['id' => $company->id], ['class' => 'btn btn-link']) !!}
											</td>
											<td>
												{!! link_to_action('CompanyController@delete', 'Törlés', ['id' => $company->id], ['class' => 'btn btn-sm btn-danger']) !!}
												{!! link_to_action('CompanyController@edit', 'Szerkesztés', ['id' => $company->id], ['class' => 'btn btn-sm btn-primary']) !!}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
