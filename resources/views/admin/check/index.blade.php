@extends('layouts.app')

@section('content')
<div class="container">
  <div class="card card-default">
    <div class="card-header">Input állomány felöltése</div>
    <div class="card-body">
      <div class="row">
          <div class="col">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul class="mb-0">
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (\Session::has('success'))
                  <div class="alert alert-success">
                      <ul>
                          <li>{{ \Session::get('success') }}</li>
                      </ul>
                  </div>
              @endif
          </div>
      </div>
      {{ Form::open(['route' => 'check.import', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
          <div class="row form-group">
              {{ Form::label('input_file', 'Input állomány (.csv, .xls, .xlsx)', ["class"=>"col-sm-4 col-form-label"]) }}
              <div class="col-md-8">
                  {{ Form::file('input_file', ['class' => 'input_file', 'accept' => '.csv,.xls,.xlsx']) }}
              </div>
          </div>
          {{ Form::submit('Feltöltés', ['class' => 'btn btn-success']) }}
      {{ Form::close() }}
    </div>
  </div>
</div>
@endsection
