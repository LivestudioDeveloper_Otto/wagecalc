@extends('layouts.app')

@section('content')
<div class="container">
   <div class="card card-default">
     <div class="card-header">
      @if ($worker->id)
         {{$worker->name}} szerkesztése
      @else
         Munkatárs hozzáadása
      @endif</div>
     <div class="card-body">
        {{ Form::model($worker, ['route' => 'worker.save', 'method' => 'post', 'class' => 'form-horizontal']) }}
          {{ Form::hidden('id') }}
          <div class="row form-group">
              {!! Form::label('', 'Azonosító', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::text('nr', null, ['class' => 'form-control']) }}
              </div>
          </div>
          <div class="row form-group">
              {!! Form::label('', 'Név', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::text('name', null, ['class' => 'form-control']) }}
              </div>
          </div>
          <div class="row form-group">
              {!! Form::label('', 'Részleg', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::select('company_id', $companies, null, ['class' => 'form-control']) }}
              </div>
          </div>
          <div class="row form-group">
              {!! Form::label('', 'Csoport', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::select('group_id', $groups, null, ['class' => 'form-control']) }}
              </div>
          </div>
          <div class="row form-group">
              {!! Form::label('', 'Beosztás', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::select('job_id', $jobs, null, ['class' => 'form-control']) }}
              </div>
          </div>
          <div class="row form-group">
              {!! Form::label('', 'Éves szabadságkeret', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::number('annual_free_day', null, ['class' => 'form-control']) }}
              </div>
          </div>
          <div class="row form-group">
              {!! Form::label('', 'Éves betegszabadság keret', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::number('annual_sick_leave', null, ['class' => 'form-control']) }}
              </div>
          </div>
          {{--<div class="row form-group">
              {!! Form::label('', 'Negyedéves táppénz keret', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">
                  {{ Form::number('annual_sick_pay', null, ['class' => 'form-control']) }}
              </div>
          </div>--}}
          <div class="row form-group">
              {!! Form::label('', 'Aktív', ["class"=>"col-md-3 col-form-label"]) !!}
              <div class="col-md-9">

                  {{-- {{ Form::hidden('active', 0) }}
                  {{ Form::checkbox('active', 1) }} --}}

                  <div class="form-check form-check-inline">
                    <label class="form-check-label pl-0">
                      {!! Form::radio('active', 1, ["class"=>"form-check-input"]) !!} Igen
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label pl-0">
                      {!! Form::radio('active', 0, ["class"=>"form-check-input"]) !!} Nem
                    </label>
                  </div>
              </div>
          </div>
          {{ Form::submit('Mentés', ['class' => 'btn btn-success']) }}
      {{ Form::close () }}
    </div>
  </div>
</div>
@endsection