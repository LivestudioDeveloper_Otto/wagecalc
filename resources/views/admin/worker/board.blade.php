@extends('layouts.app')

@section('content')
  <div ng-cloak class="container" ng-controller="admin_worker_board_ctrl" ng-init="awb_srv.setInfo({{json_encode($info)}}); awb_srv.setWorker({{json_encode($worker)}}); awb_srv.setStatuses({{json_encode($statuses)}}); awb_srv.setData({{json_encode($month)}})">
   
    @include('admin.worker.board_modals')
   
    <div class="row">
      <div class="col-md-12">
        
        <div class="card card-default">
			<div class="card-header">
				<div class="row">
					<div class="col-md-6">
						Munkatárs hónapja: <strong>{{ $worker->name }}</strong> <small>({{ $worker->group->name }})</small>
					</div>
					<div class="col-md-6 text-right">
						{{ Form::submit('Mentés', ['class' => 'btn btn-success', 'form' => '#workerBoard']) }}
						{{ Form::submit('Mentés és vissza', ['class' => 'btn btn-success', 'form' => '#workerBoard', 'name' => 'returnBack', 'value' => '1']) }}
						{!! link_to_action('WorkerBoardController@view', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
					</div>
				</div>
			</div>

          <div class="card-body">
              @if (session('status'))
                <div class="alert alert-success">
                  {{ session('status') }}
                </div>
              @endif

              <div class="card card-default mb-3">
                <div class="card-body text-center">
                  <ul class="list-unstyled list-inline mb-0">
                    <li class="list-inline-item"><strong>Munkanapok a hónapban:</strong> {[countDays('workday')]} nap</li>
                    <li class="list-inline-item"><strong>Éves szabadságkeret:</strong> {{$worker->annual_free_day}} / {[awb_srv.info.counts.free_day+countDays('free_day')]} nap</li>
                    <li class="list-inline-item"><strong>Éves betegszabadság:</strong> {{$worker->annual_sick_leave}} / {[awb_srv.info.counts.sick_leave+countDays('sick_leave')]} nap</li>
                    <li class="list-inline-item"><strong>Túlóra <small>(hó/negyedév)</small>:</strong> {[getWorkedHours()-awb_srv.getInfo('global_hours_in_month')]} / {[awb_srv.getInfo('over_workhours_in_quarter')+getWorkedHours()]}  óra<!--{[awb_srv.info.over_time+getMonthlyOvertimes()|number:0]}/{[getMonthlyOvertimes()|number:0]} óra-->
										</li>
                  </ul>
                </div>
              </div>
             
              {{ Form::open(['route' => ['worker.saveboard', $worker->id], 'method' => 'put', 'id' => '#workerBoard']) }}
							
                <table class="table table-bordered text-center weeks-table">
                  <thead>
                    <tr>
                      <th width="14%">Hétfő</th>
                      <th width="14%">Kedd</th>
                      <th width="14%">Szerda</th>
                      <th width="14%">Csütörtök</th>
                      <th width="14%">Péntek</th>
                      <th width="14%">Szombat</th>
                      <th width="14%">Vasárnap</th>
                    </tr>
                  </thead>
                  <tbody>
                   <tr ng-repeat="week in awb_srv.data">
                     <td ng-repeat="a in [] | range:AppHelper.array_keys(week)[0]-1" class="blank-day week-day-{[$index]} status-alias-null">&nbsp;</td>
                     
                     <td ng-repeat="(key, value) in week" class="week-day-{[$index]} board-day-{[ AppHelper.newDate(value.dt.date) | date : 'yyyy-MM-dd' ]} status-alias-{[value.restricted.alias]}" ng-init="value.configured={};value.temp={}" ng-click="value.configure=!value.configure;" ng-class="{'selected':value.configure}">
                    
                       <input name="configured_status[{[ AppHelper.newDate(value.dt.date) | date : 'yyyy-MM-dd' ]}]" value="{[value.configured.status.id ? value.configured.status.id : value.restricted.id]}" type="hidden">
                       
											 <input name="configured_check_in[{[ AppHelper.newDate(value.dt.date) | date : 'yyyy-MM-dd' ]}]" value="{[ value.configured.check.check_in !== undefined ? value.configured.check.check_in : value.check.check_in]}" type="hidden">
                       
											 <input name="configured_check_out[{[ AppHelper.newDate(value.dt.date) | date : 'yyyy-MM-dd' ]}]" value="{[ value.configured.check.check_out !== undefined ? value.configured.check.check_out : value.check.check_out]}" type="hidden">
                       
											 <input name="configured_formal_start[{[ AppHelper.newDate(value.dt.date) | date : 'yyyy-MM-dd' ]}]" value="{[ value.configured.check.formal_start !== undefined ? value.configured.check.formal_start : value.check.formal_start]}" type="hidden">
                       
											 <input name="configured_formal_end[{[ AppHelper.newDate(value.dt.date) | date : 'yyyy-MM-dd' ]}]" value="{[ value.configured.check.formal_end !== undefined ? value.configured.check.formal_end : value.check.formal_end]}" type="hidden">
                       {[ AppHelper.newDate(value.dt.date) | date : "yyyy-MM-dd" ]}
                       {[value.restricted.name]}<br/>
                       <div ng-if="value.check || value.configured.check">
                         <div ng-if="value.configured.check.check_in !== undefined || value.configured.check.check_out !== undefined">
                           Módosított: 
                           <span ng-if="value.configured.check.check_in">{[value.configured.check.check_in|roundTime]}</span>
                           <span ng-if="!value.configured.check.check_in"><i class="icon-exclamation-circle"></i></span>
                           <span> - </span>
                           <span ng-if="value.configured.check.check_out">{[value.configured.check.check_out|roundTime]}</span>
                           <span ng-if="!value.configured.check.check_out"><i class="icon-exclamation-circle"></i></span>
                           <br/>
                           <span ng-if="timeDiff(value.configured.check) && value.configured.check.check_in && value.configured.check.check_out">({[timeDiff(value.configured.check)|roundTime]})</span>
                         </div>
                         <div ng-if="value.configured.check.check_in === undefined && value.configured.check.check_out === undefined && (value.check.rounded_check_in || value.check.rounded_check_out)">
                           <span ng-if="value.check.rounded_check_in">{[value.check.rounded_check_in]}</span>
                           <span ng-if="!value.check.rounded_check_in"><i class="icon-exclamation-circle"></i></span>
                           <span> - </span>
                           <span ng-if="value.check.rounded_check_out">{[value.check.rounded_check_out]}</span>
                           <span ng-if="!value.check.rounded_check_out"><i class="icon-exclamation-circle"></i></span>
                           <br/>
                           <span ng-if="value.check.rounded_check_in && value.check.rounded_check_out">
                           ({[value.check.rounded_diff]})
                           </span>
                         </div>
                       </div>
                       
                       <div ng-if="value.configured.status && value.configured.status.alias!=value.restricted.alias" class="board-day-modified status-alias-{[value.configured.status.alias]}">{[value.configured.status.name]}</div>
                       
                       <div class="btn-group mt-3" role="group">
                         <button type="button" class="btn btn-sm btn-primary" ng-click="singleEdit(value);">
                           <i class="icon-edit"></i> Szerkesztés
                         </button>
                       </div>
                       
                       <input class="configure_date" name="configure_date" ng-model="value.configure" value="" type="checkbox" hidden>

                       {{-- Túlóra: {[value.check.rounded_over_time]} - {[getOverTime(value)]} --}}
                       
                     </td>
                     
                     <td ng-repeat="b in [] | range:(7-((AppHelper.array_keys(week)[0]-1)+AppHelper.getLength(week)))+1" class="blank-day week-day-{[$index]} status-alias-null">&nbsp;</td>
                   </tr>
                   
                  </tbody>
                </table>

                <div class="row">
                  <div class="col-md-6">
                    <button type="button" class="btn btn-info" ng-disabled="AppHelper.empty(awb_srv.checkedItems)" ng-click="openModal('configureDateStatuses')">
                      Kijelölt státuszok módosítása
                    </button>
                    <button type="button" class="btn btn-warning" ng-disabled="AppHelper.empty(awb_srv.checkedItems)" ng-click="openModal('configureDateChecks');">
                      Kijelölt dokkolások módosítása
                    </button>
                  </div>
                  <div class="col-md-6 text-right">
                    {{ Form::submit('Mentés', ['class' => 'btn btn-success']) }}
										{{ Form::submit('Mentés és vissza', ['class' => 'btn btn-success', 'name' => 'returnBack', 'value' => '1']) }}
                  </div>
                </div>

              {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
    
  </div>
@endsection

@section('scripts')
  <script src="{{asset('js/app.admin.worker.board.js')}}"></script>
@endsection