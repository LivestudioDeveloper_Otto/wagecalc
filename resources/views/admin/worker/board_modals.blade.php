<div class="modal-parent"></div>

<script type="text/ng-template" id="configureDateChecks.html">
  <div class="modal-header">
    <h4 class="modal-title">Kijelölt napok módosítása</h4>
    <button type="button" class="close" ng-click="closeModal()" aria-label="Bezárás">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">

    <div ng-repeat="(wkey, week) in awb_srv.checkedItems track by $index">
     <div ng-repeat="dayIndex in week">
        <div class="row form-group">
          <div class="col">
            <h5 class="day-date">{[ AppHelper.newDate(awb_srv.data[wkey][dayIndex].dt.date) | date : 'yyyy-MM-dd' ]}</h5>
          </div>
        </div>
        <div class="row form-group">
          <label class="col-md-4 col-form-label">Hivatalos idő</label>
          <div class="col-md-8 input-group">
              <input class="form-control" type="text" class="formal_start_config" ng-model="awb_srv.data[wkey][dayIndex].temp.configured.check.formal_start" ng-blur="formatTime(wkey,dayIndex,'formal_start')" tabindex="1">
              <span class="input-group-addon">-</span>
              <input class="form-control" type="text" class="formal_end_config" ng-model="awb_srv.data[wkey][dayIndex].temp.configured.check.formal_end" ng-blur="formatTime(wkey,dayIndex,'formal_end')" tabindex="2">
          </div>
        </div>
        <div class="row form-group">
          <label class="col-md-4 col-form-label">Dokkolási idő</label>
          <div class="col-md-8 input-group">
              <input class="form-control" type="text" class="check_in_config" ng-model="awb_srv.data[wkey][dayIndex].temp.configured.check.check_in" ng-blur="formatTime(wkey,dayIndex,'check_in')" tabindex="3">
              <span class="input-group-addon">-</span>
              <input class="form-control" type="text" class="check_out_config" ng-model="awb_srv.data[wkey][dayIndex].temp.configured.check.check_out" ng-blur="formatTime(wkey,dayIndex,'check_out')" tabindex="4">
          </div>
        </div>
     </div>
    </div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" ng-click="closeModal()">Bezárás</button>
    <button type="button" id="configureChecksSave" class="btn btn-primary" data-dismiss="modal" ng-click="saveConfiguredCheckData()">Mentés</button>
  </div>
</script>

<script type="text/ng-template" id="configureDateStatuses.html">
  <div class="modal-header">
    <h4 class="modal-title">Kijelölt napok módosítása</h4>
    <button type="button" class="close" ng-click="closeModal()" aria-label="Bezárás">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <label class="col-md-4 col-form-label">Napok státusza</label>
      <div class="col-md-8">
        <select class="form-control" name="restricted_date_status" id="restricted_date_status" ng-model="tempData.newStatus" ng-init="tempData.newStatus=''">
          <option value="">Válassz...</option>
          @foreach ($statuses as $id => $status)
              <option value="{{ $id }}" data-status_alias="{{ $status['alias'] }}">{{ $status['name'] }}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="closeModal()">Bezárás</button>
    <button type="button" id="configureStatusSave" class="btn btn-primary" data-dismiss="modal"  ng-click="changeStatus(tempData.newStatus)">Mentés</button>
  </div>
</script>