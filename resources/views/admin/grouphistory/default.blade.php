@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
							Csoport műszakok újrakezdés dátuma
						</div>
						<div class="col-md-6 text-right">
							{!! link_to_action('DashboardController@index', 'Vissza', [], ['class' => 'btn btn-sm btn-secondary']) !!}
						</div>
					</div>
				</div>

                <div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="mb-0">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>	
						<div class="row form-group">
							<div class="col-md-12">
								{!! Form::open(['action' => 'GroupHistoryController@create', 'class' => 'form-inline']) !!}
									<div class="form-row align-items-center">
										<div class="col-auto form-group">
											{!! Form::Label('date', 'Dátum', ['class' => 'mr-sm-2']) !!}
											{!! Form::date('date', '', ['class' => 'form-control mb-2 mb-sm-0']) !!}
										</div>
										<div class="col-auto form-group">
											{!! Form::submit('Mentés', ['class' => 'btn btn-primary']) !!}
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
						<div class="row">
							<table class="table table-striped table-hover mb-0">
								<thead>
									<tr>
										<th>Dátum</th>
										<th>Hozzáadta</th>
										<th>Művelet</th>
									</tr>
								</thead>
								<tbody>
									@foreach($group_histories as $group_history)
										<tr>
											<td>
												{{ $group_history->date }}
											</td>
											<td>
												{{ $group_history->user ? $group_history->user->name : 'Rendszer' }}
											</td>
											<td>
												@if($group_history->user_id)
													{!! link_to_action('GroupHistoryController@delete', 'Törlés', ['id' => $group_history->id], ['class' => 'btn btn-sm btn-danger']) !!}
												@else
													Rendszer által hozzáadott műszak újrakezdés
												@endif
											</td>	
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
