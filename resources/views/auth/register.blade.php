@extends('layouts.app')

@section('content')
            <div class="card">
                <div class="card-header">Register</div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="row form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Név</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail cím</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('profile[phone]') ? ' has-error' : '' }}">
                            <label for="profile_phone" class="col-md-4 control-label">Telefonszám</label>

                            <div class="col-md-8">
                                <input id="profile_phone" type="text" class="form-control" name="profile[phone]" value="{{ old('profile[phone]') }}" required>

                                @if ($errors->has('profile[phone]'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile[phone]') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('profile[zip]') ? ' has-error' : '' }}">
                            <label for="profile_zip" class="col-md-4 control-label">Irányítószám</label>

                            <div class="col-md-8">
                                <input id="profile_zip" type="text" class="form-control" name="profile[zip]" value="{{ old('profile')['zip'] }}" required>

                                @if ($errors->has('profile[zip]'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile[zip]') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Jelszó</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group">
                            <label for="password-confirm" class="col-md-4 control-label">jelszó megerősítése</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-8 ml-auto">
                                <button type="submit" class="btn btn-primary">
                                    Regisztráció
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
@endsection
