<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class closeMonth extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIsClosedMonth()
    {
		/*
		 *	active date
		 *	get active workers
		 *	company
		 *	check
		 *	restricted dates calculate
		 */
		
		$closed_month = \App\ClosedMonth::isClosedMonth(\Carbon\Carbon::parse('2017-10-05'));
		
        $this->assertTrue($closed_month);
    }
}
