<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RestrictedDateStatus extends TestCase
{
	//use RefreshDatabase;
	
	public function testBasicTest()
	{
		$restricted_date = \App\RestrictedDate::firstOrCreate(['date' => \Carbon\Carbon::today(), 'restricted_date_status_id' => 1]);
		$this->assertEquals(range(4,0), $restricted_date->status->getDays());
	}
}
