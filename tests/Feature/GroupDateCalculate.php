<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;

class GroupDateCalculate extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGroupCalculate()
    {
		//$this->withoutExceptionHandling();
		
        $worker_group = \App\WorkerGroup::find(1);
		
		$this->assertEquals(2, $worker_group->getGroupDate(Carbon::today())->ordering);
		
		$this->assertEquals(2, $worker_group->getGroupDate(Carbon::parse('2017-11-03'))->ordering);
		
		$this->assertEquals(3, $worker_group->getGroupDate(Carbon::parse('2017-11-10'))->ordering);
		
		$this->assertEquals(1, $worker_group->getGroupDate(Carbon::parse('2017-11-17'))->ordering);
		
		$this->assertEquals(1, $worker_group->getGroupDate(Carbon::parse('2017-10-25'))->ordering);
    }
}
