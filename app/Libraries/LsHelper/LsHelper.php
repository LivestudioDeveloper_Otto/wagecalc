<?php

namespace App\Libraries;

use Carbon\Carbon;
use Carbon\CarbonInterval;

class LsHelper {
    
    public static function roundTime(Carbon $dt) {
        if ($dt->minute >= 30) {
            $dt->addHour();
        }
        $dt->minute(0)->second(0);
        return $dt;
    }
	
    public static function roundInterval(DateInterval $interval) {
        if ($interval->i >= 30) {
            $interval->h++;
        }
		
		$interval->i = 0;
		$interval->s = 0;
		
        return $interval;
    }
	
	public static function timeExp($time, int $key = 0) {
		return (int)explode(':', $time)[$key];
	}
	
	public static function timeFormat($hours, bool $empty = false, int $minutes = 0, bool $pad_h = false) {
		if (!is_numeric($hours) && strpos($hours, ':') !== false) {
			$minutes = static::timeExp($hours, 1);
			$hours = static::timeExp($hours);
		}
		if ($empty && !$hours && !$minutes) {
			return '';
		}
		return (($pad_h && $hours <= 9) ? str_pad($hours, 2, 0, STR_PAD_LEFT) : $hours) . ':' . str_pad($minutes, 2, 0, STR_PAD_LEFT);
	}
	
	/*
	 *	Calculate time intervals
	 *	@IN
	 *		formal_start @string
	 *		formal_end @string
	 *		check_in @string
	 *		check_out @string
	 *		operation @string values inner|outer
	 *	@OUT
	 *		DateInterval | null
	 */
    public static function timeMod($time1, $time2, $operation = 'add')
	{
        $t1_exp = explode(':', $time1);
        $t2_exp = explode(':', $time2);
        
        switch ($operation) {
            case 'add':
				$h = $t1_exp[0];
				if (!isset($t1_exp[1])) {
					$t1_exp[1] = 0;
				}
				if (!isset($t2_exp[1])) {
					$t2_exp[1] = 0;
				}
				$m = $t1_exp[1] + $t2_exp[1];
				if ($m > 60) {
					$h++;
					$m = $m - 60;
				}
				$h += $t2_exp[0];
				return str_pad($h, 2, 0, STR_PAD_LEFT).':'.str_pad($m, 2, 0, STR_PAD_LEFT);
                break;
        }
		
		throw new Exception('Ismeretlen operandus.');
    }
 
	/*
	 *	Calculate time intervals
	 *	@IN
	 *		formal_start @string
	 *		formal_end @string
	 *		check_in @string
	 *		check_out @string
	 *		operation @string values inner|outer
	 *	@OUT
	 *		DateInterval | null
	 */
    public static function timeOperation($formal_start, $formal_end, $check_in, $check_out, $operation = 'inner')
	{
        if (!$formal_start || !$formal_end || !$check_in || !$check_out) {
            return new \DateInterval('P0D');
        }
		
        $fs_dt = Carbon::parse($formal_start);
        $fe_dt = Carbon::parse($formal_end)->setDate($fs_dt->year, $fs_dt->month, $fs_dt->day);
        $ci_dt = Carbon::parse($check_in)->setDate($fs_dt->year, $fs_dt->month, $fs_dt->day);
        $co_dt = Carbon::parse($check_out)->setDate($fs_dt->year, $fs_dt->month, $fs_dt->day);
        
		if ($fs_dt > $fe_dt)
		{
			$fe_dt->addDay();
		}
		
		if ($ci_dt > $co_dt)
		{
			$co_dt->addDay();
		}
		
        switch ($operation) {
            case 'inner':
			
				$start_date = $fs_dt;
				$end_date = $fe_dt;
				
				if ($fs_dt > $ci_dt && $fs_dt > $co_dt || $fe_dt < $ci_dt && $fe_dt < $co_dt) {
					return new \DateInterval('P0D');
				}
				
                if ($fs_dt < $ci_dt)
				{
					$start_date = $ci_dt;
                }
				
                if ($fe_dt > $co_dt)
				{
					$end_date = $co_dt;
                }
                
                return $start_date->diff($end_date);
                break;
            case 'outer':
				
				$end_date = Carbon::today();
				
				if ($fs_dt > $ci_dt && $fs_dt > $co_dt || $fe_dt < $ci_dt && $fe_dt < $co_dt) {
					$end_date->add($ci_dt->diff($co_dt));
				}
				else
				{
					if ($ci_dt < $fs_dt)
					{
						$end_date->add($ci_dt->diff($fs_dt));
					}
					
					if ($fe_dt < $co_dt)
					{
						$end_date->add($fe_dt->diff($co_dt));
					}
				}
				
                return Carbon::today()->diff($end_date);
                break;
        }
		
		throw new Exception('Ismeretlen operandus.');
    }
}

?>