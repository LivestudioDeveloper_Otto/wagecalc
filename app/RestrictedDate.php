<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RestrictedDate extends Model
{
	public $timestamps = false;
	
    protected $fillable = ['date', 'restricted_date_status_id'];
	
	public function status()
	{
		return $this->hasOne(RestrictedDateStatus::class, 'id', 'restricted_date_status_id');
	}
	
	public static function getMonth(int $year, int $month) {
        $dt = Carbon::create($year, $month, 1, 0, 0, 0, 'Europe/Budapest');
		return static::whereBetween('date', [$dt->startOfMonth()->format('Y-m-d H:i:s'), $dt->endOfMonth()->format('Y-m-d H:i:s')])->get();
	}
	
	public static function getRestrictedDates(Carbon $date)
	{
		return static::whereBetween('date', [$date->copy()->startOfMonth()->subDay()->format('Y-m-d H:i:s'), $date->copy()->endOfMonth()->addDay()->format('Y-m-d H:i:s')])->get();
	}
}
