<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestrictedWorkerDate extends Model
{
	public $timestamps = false;
	
    protected $fillable = ['date', 'worker_id', 'restricted_worker_date_status_id'];
    
    public function status() {
        return $this->hasOne('App\restrictedWorkerDateStatus', 'id', 'restricted_worker_date_status_id');
    }
}
