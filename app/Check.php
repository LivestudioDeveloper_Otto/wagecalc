<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Worker;
use App\WorkerGroupDate;
use App\ClosedMonth;

class Check extends Model
{
    protected $fillable = ['worker_id', 'check_in', 'check_out', 'configured_check_in', 'configured_check_out', 'configured_formal_start', 'configured_formal_end'];
    
	public function worker() 
	{
		return $this->hasOne(Worker::class, 'id', 'worker_id');
	}
	
	public static function getInvalidChecks(Carbon $_date)
	{
		$date = $_date->copy();
		return self::where(function ($query) use ($date) {
			$query->whereBetween('configured_check_in', [$date->startOfMonth()->format('Y-m-d H:i:s'), $date->endOfMonth()->format('Y-m-d H:i:s')])
			->orWhereBetween('configured_check_out', [$date->startOfMonth()->format('Y-m-d H:i:s'), $date->endOfMonth()->format('Y-m-d H:i:s')]);
		})
		->where(function($query) {
			$query->whereNull('configured_check_in')
			->orWhereNull('configured_check_out');
			//->orWhereNull('configured_formal_start')
			//->orWhereNull('configured_formal_end');
		})->get();
	}
	
    public static function getChecksArray() {
        $list = self::select(DB::Raw('checks.id as id, worker_id, nr, DATE_FORMAT((CASE WHEN configured_check_in IS NULL THEN configured_check_out ELSE (CASE WHEN configured_check_in IS NOT NULL THEN configured_check_in ELSE (CASE WHEN configured_formal_start IS NULL THEN configured_formal_end ELSE configured_formal_start END) END) END), "%Y-%m-%d") AS date, check_in, check_out, configured_check_in, configured_check_out, configured_formal_start, configured_formal_end'))->join('workers', function($join) {
            $join->on('workers.id', '=', 'checks.worker_id');
        })->groupBy(['date', 'worker_id'])->having('date', '>', Carbon::parse(ClosedMonth::getLast())->endOfMonth())->get();
        $checks = [];
        foreach($list as $item) {
            if (!array_key_exists($item->nr, $checks)) {
                $checks[$item->nr] = [];
            }
            $checks[$item->nr][$item->date] = ['id' => $item->id, 'check_in' => $item->check_in, 'check_out' => $item->check_out, 'configured_check_in' => $item->configured_check_in, 'configured_check_out' => $item->configured_check_out, 'configured_formal_start' => $item->configured_formal_start, 'configured_formal_end' => $item->configured_formal_end];
        }
        return $checks;
    }
    
    public static function setChecksArray(Array $checks, bool $miss_null = false) {
		$return = ['success' => 0, 'miss' => 0];
		
        foreach($checks as $worker_nr => $days) {
            $worker = Worker::where('nr', '=', $worker_nr)->first();
            if ($worker && count($days)) {
                foreach($days as $date => $check) {
					if ($miss_null && !$check['configured_check_in'] && !$check['configured_check_out']) {
						$return['miss']++;
						continue;
					}
                    $check['worker_id'] = $worker->id;
                    $groupDate = $worker->group->getGroupDate(Carbon::parse($date));
                    $check['group_date_id'] = $groupDate->id;
                    $formal_start_date = Carbon::createFromFormat('Y-m-d H:i:s', $date . ' ' . $groupDate->start);
                    $formal_end_date = Carbon::createFromFormat('Y-m-d H:i:s', $date . ' ' . $groupDate->end);
					
                    if ($formal_end_date < $formal_start_date) {
                        $formal_end_date->addDay();
                    }
                    if (!array_key_exists('configured_formal_start', $check)) {
                        //$check['configured_formal_start'] = $formal_start_date->toDateTimeString();
                    }
                    if (!array_key_exists('configured_formal_end', $check)) {
                        //$check['configured_formal_end'] = $formal_end_date->toDateTimeString();
                    }
                    $_check = ($check['id'] ? Check::find($check['id']) : new Check);
					
                    $_check->fill($check);
                    if ($_check->save()) {
						$return['success']++;
					}/* else {
						$return['miss']++;
					}*/
                }
            } else {
				$return['miss'] += count($days);
			}
        }
		
		return $return;
    }
    
    public function save(array $options = [])
    {
		if (!$this->check_in && !$this->check_out && !$this->configured_check_in && !$this->configured_check_out && !$this->configured_formal_start && !$this->configured_formal_end) {
			return $this->delete();
		}
		
        $date = $this->configured_check_in ? $this->configured_check_in : ($this->configured_check_out ? $this->configured_check_out : ($this->configured_formal_start ? $this->configured_formal_start : ($this->configured_formal_end ? $this->configured_formal_end : ($this->check_in ? $this->check_in : $this->check_out))));
		
		if (!$date) {
			return false;
		}
		
        $this_dt = Carbon::parse($date)->startOfMonth();
		
        
        if (ClosedMonth::isClosedMonth($this_dt)) {
            return false;
        }
        
        return parent::save($options);
    }
}
