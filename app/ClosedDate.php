<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClosedDate extends Model
{
    public function check()
	{
		$this->hasOne(Check::class, 'id', 'check_id');
	}
	
    public function closedMonth()
	{
		$this->belongsTo(ClosedMonth::class, 'id', 'closed_month_id');
	}
}
