<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestrictedDateStatus extends Model
{
    protected $fillable = ['name'];
	
	protected $guarded = ['default'];
	
	public function hasDay($day)
	{
		return in_array($day, $this->getDays());
	}
	
	public function getDays()
	{
		$days = [];
		$day = 7;
		
		while($day-- > 0)
		{
			if(($this->default & (2 ** $day)) != 0)
			{
				if(!in_array($day, $days))
				{
					$days[] = $day;
				}
			}
		}
		
		return $days;
	}
	
	public function setDays($days)
	{
		if(is_array($days))
		{
			for($day = 0; $day < 7; $day++)
			{
				if(in_array($day, $days))
				{
					$this->default += 2 ** (int)$day;
				}
			}
		}
		else if (is_int($days))
		{
			if($days > 0 && $days < 128)
			{
				$this->default = $days;
			}
		}
	}
}
