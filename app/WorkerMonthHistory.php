<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerMonthHistory extends Model
{
	protected $table = 'worker_month_history';
	
    protected $fillable = ['worker_id', 'closed_month_id', 'overtime', 'annual_free_pay', 'annual_sick_pay', 'annual_sick_leave'];
}
