<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Libraries\LsHelper;
use App\Company;
use App\RestrictedWorkerDate;
use App\RestrictedWorkerDateStatus;
use App\RestrictedDate;
use App\RestrictedDateStatus;
use App\Check;
use App\ClosedMonth;
use Excel;
use Storage;

class Worker extends Model
{
	protected $fillable = ['nr', 'name', 'company_id', 'group_id', 'job_id', 'annual_free_day', 'annual_sick_leave', 'annual_sick_pay', 'active'];
	
    public function job() {
        return $this->hasOne('App\Job', 'id', 'job_id');
    }
	protected $restricteds = null;
	protected $allRestrictedDates = null;
	protected $closedChecks = [];
	protected $info = null;
	private $nightBonusMonths = [];
    
    public function restrictedDates(int $year = null, int $month = null) {
		//$date = Session::get('sessionDate');
		$date = ['year' => $year, 'month' => $month];
		if ($year === null && $month === null)
		{
			$date = Session::get('sessionDate');
		}
		elseif ($year === null || $month === null)
		{
			throw new Exception('Hiányzó dátum szegmens.');
		}
		
		$start_date = Carbon::createFromDate($date['year'], $date['month'], 1);
		$end_date = $start_date->copy()->endOfMonth();
		
		return $this->hasMany('App\RestrictedWorkerDate', 'worker_id', 'id')->whereBetween('date', [$start_date->copy()->subDay()->format('Y-m-d'), $end_date->copy()->addDay()->format('Y-m-d')]);
    }
    
	public function getInvalidChecks(Carbon $_date)
	{
		$date = $_date->copy();
		return $this->checks()
		->where(function ($query) use ($date) {
			$query->whereBetween('configured_check_in', [$date->startOfMonth()->format('Y-m-d H:i:s'), $date->endOfMonth()->format('Y-m-d H:i:s')])
			->orWhereBetween('configured_check_out', [$date->startOfMonth()->format('Y-m-d H:i:s'), $date->endOfMonth()->format('Y-m-d H:i:s')]);
		})
		->where(function($query) {
			$query->whereNull('configured_check_in')
			->orWhereNull('configured_check_out');
			//->orWhereNull('configured_formal_start')
			//->orWhereNull('configured_formal_end');
		})->get();
	}
	
    public function getMonthChecks(int $year, int $month) {
        $dt = Carbon::create($year, $month, 1, 0, 0, 0, 'Europe/Budapest');
        $check_dates = [];
        $range = [$dt->copy()->startOfMonth()->format('Y-m-d'), $dt->copy()->endOfMonth()->addDay()->format('Y-m-d')];
		
		$checks = $this->checks()->where(function ($_query) use ($range) {
				$_query->where(function ($query) use ($range) {
					$query->whereNotNull('configured_check_in')
					->whereBetween('configured_check_in', $range);
				})
				->orWhere(function($query) use ($range) {
					$query->whereNotNull('configured_check_out')
					->whereBetween('configured_check_out', $range);
				})
				->orWhere(function($query) use ($range) {
					$query->whereNotNull('configured_formal_start')
					->whereBetween('configured_formal_start', $range);
				})
				->orWhere(function($query) use ($range) {
					$query->whereNotNull('configured_formal_end')
					->whereBetween('configured_formal_end', $range);
				});
		})->get();
		
		foreach($checks as $check) {
			$dt = Carbon::createFromFormat('Y-m-d H:i:s', $check->configured_check_in ? $check->configured_check_in : ($check->configured_check_out ? $check->configured_check_out : ($check->configured_formal_start ? $check->configured_formal_start : $check->configured_formal_end)));
            $day = $dt->format('Y-m-d');
            $_check = new \StdClass;
			
            $groupdate = $this->group->getGroupDate($dt);
			
			$this->checkFormat($_check, $check);
			
			if ($_check->formal_start === null) {
				$_check->formal_start = LsHelper::timeFormat($groupdate->start, false, 0, true);
			}
			if ($_check->formal_end === null) {
				$_check->formal_end = LsHelper::timeFormat($groupdate->end, false, 0, true);
			}
			
            $check_dates[$day] = $_check;
        }
        return $check_dates;
    }
    
    public static function getCoreTimes() {
        $core_times = [];
        $core_times['start'] = [];
        $core_times['end'] = [];
        
        for ($i = 0; $i < Carbon::HOURS_PER_DAY; $i++) {
            $core_times['start'][] = Carbon::createFromTime($i, 0, 0)->format('H:i');
            $core_times['end'][] = Carbon::createFromTime($i, 0, 0)->format('H:i');
        }
        
        return $core_times;
    }
    
    public static function getFormalTimes() {
        $formal_times = [];
        $formal_times['start'] = [];
        $formal_times['end'] = [];
        
        for ($i = 0; $i < Carbon::HOURS_PER_DAY; $i++) {
            $formal_times['start'][] = Carbon::createFromTime($i, 0, 0)->format('H:i');
            $formal_times['end'][] = Carbon::createFromTime($i, 0, 0)->format('H:i');
        }
        
        return $formal_times;
    }
	
    public function checks()
	{
		return $this->hasMany(Check::class, 'worker_id', 'id')->orderBy('check_in', 'asc');
	}
	
    public function group()
	{
		return $this->hasOne(WorkerGroup::class, 'id', 'group_id');
	}
	
    public function company()
	{
		return $this->hasOne(Company::class, 'id', 'company_id');
	}
	
	public function getGroupDateId($date = null) {
		return 1;
	}
	
	public function getWorkerDay($day, $worker_restricteds = true) {
		$dt = Carbon::parse($day);
		$day = $dt->toDateString();
		$restricted_worker = false;
		if ($worker_restricteds) {
			$restricted_worker = RestrictedWorkerDate::select("*")->where('date', '=', $day)->where('worker_id', '=', $this->id)->first();
		}
		if ($restricted_worker) {
			$status = $restricted_worker->status;
			$status->type = "restricted_worker_date";
		} else {
			$restricted = RestrictedDate::select("*")->where('date', '=', $day)->first();
			if (!$restricted) {
				$standards = RestrictedDateStatus::all();
				if (count($standards)) {
					foreach($standards as $rest) {
						if ($rest->hasDay($dt->format('N')-1)) {
							$status = $rest;
							$status->type = "standard";
							break;
						}
					}
				}
			} else {
				$status = $restricted->status;
				$status->type = "restricted_date";
			}
		}
		
		return $status;
	}
	
	public function getCheckDay($day) {
		$dt = Carbon::parse($day);
		$day = $dt->toDateString();
		return Check::select(DB::raw('*, DATE_FORMAT((CASE WHEN configured_check_in IS NULL THEN configured_check_out ELSE (CASE WHEN configured_check_in IS NOT NULL THEN configured_check_in ELSE (CASE WHEN configured_formal_start IS NULL THEN configured_formal_end ELSE configured_formal_start END) END) END), "%Y-%m-%d") AS date'))->where('worker_id', '=', $this->id)->having('date', '=', $day)->first();
	}
	
	public function getNormalTime($day) {
		$dt = Carbon::parse($day);
		$day = $dt->toDateString();
		$status = $this->getWorkerDay($day);
		
		if ($status->alias == 'workday') {
			$check = $this->getCheckDay($day);
			if ($check) {
				$groupdate = $this->group->getGroupDate($dt);
				$formal_start = ($check->configured_formal_start ? Carbon::parse($check->configured_formal_start)->toTimeString() : $groupdate->start);
				$formal_end = ($check->configured_formal_end ? Carbon::parse($check->configured_formal_end)->toTimeString() : $groupdate->end);
				
				return LsHelper::timeOperation($formal_start, $formal_end, $check->check_in, $check->check_out);
			}
		}
		return null;
	}
	
	public function getGlobalHours(Carbon $date) {
		$date = $date->copy();
		$date->startOfMonth();
		$end_date = $date->copy();
		$end_date = $end_date->endOfMonth();
		$hours = 0;
		
		while ($date <= $end_date) {
			$day = $date->toDateString();
			$status = $this->getWorkerDay($day, true);
			if (in_array($status->alias, ['workday', 'free_day', 'sick_leave', 'sick_pay', 'proven_day'])) {
				$groupdate = $this->group->getGroupDate($date);
				
				$hours += LSHelper::timeOperation($groupdate->start, $groupdate->end, $groupdate->start, $groupdate->end)->h;
				
			}
			$date->addDay();
		}
		
		return $hours;
	}
	
	function countLastDays($alias) {
		$sessionDate = Session::get('sessionDate');
		$start_date = Carbon::createFromDate($sessionDate['year'], $sessionDate['month'], 1);
		$end_date = $start_date->copy()->endOfMonth();
		
		if ($this->restricteds === null) {
			$this->restricteds = RestrictedWorkerDate::where('worker_id', '=', $this->id)->where('date', '>', $end_date)->havingRaw('YEAR(`date`) = '.$sessionDate['year']);
		}
		$num = 0;
		foreach($this->restricteds as $restricted) {
			if ($restricted->status->alias == $alias) {
				$num++;
			}
		}
		return $num;
	}
	
    public function getMonthDays($year, $month, $restricted = false, $extended = false) {
        $days = [[]];
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        $start = Carbon::create($year, $month, 1, 0, 0, 0, 'Europe/Budapest');
        $end = $start->copy()->endOfMonth();
		if ($extended) {
			$start->subDay();
			$end->addDay();
		}
        $period = new \DatePeriod($start, new \DateInterval('P1D'), $start->diffInDays($end));
        $check_dates = $this->getMonthChecks($year, $month);
		
        foreach($period as $dt) {
            $day = new Carbon($dt, 'Europe/Budapest');
            if ($day->dayOfWeek == Carbon::MONDAY && count($days[0])) {
                $days[] = [];
            }
            $weekOfMonth = count($days)-1;
            $dayOfWeek = $day->dayOfWeek ? $day->dayOfWeek : 7;
            if (!array_key_exists($weekOfMonth, $days) || !is_array($days[$weekOfMonth])) {
                $days[$weekOfMonth] = [];
            }
            $_day = $day->copy();
			$groupdate = $this->group->getGroupDate($_day);
            $days[$weekOfMonth][$dayOfWeek] = ['dt' => $_day, 'restricted' => null, 'check' => null, 'groupdate' => $groupdate];
            if (array_key_exists($_day->format('Y-m-d'), $check_dates)) {
                $days[$weekOfMonth][$dayOfWeek]['check'] = $check_dates[$_day->format('Y-m-d')];
            } else {
				$days[$weekOfMonth][$dayOfWeek]['check'] = (object)[
															'id' => null,
															'check_in' => null,
															'check_out' => null,
															'diff' => null,
															'normal_time' => null,
															'over_time' => null,
															'rounded_check_in' => null,
															'rounded_check_out' => null,
															'rounded_diff' => null,
															'rounded_normal_time' => null,
															'rounded_over_time' => null,
															'formal_start' => LsHelper::timeFormat($groupdate->start, false, 0, true),
															'formal_end' => LsHelper::timeFormat($groupdate->end, false, 0, true)
															];
			}
        }
		
        if ($restricted) {
			$this->getAllRestrictedDates($days);
			foreach($days as $w => $week) {
				foreach($week as $d => $day) {
					if ($days[$w][$d]['restricted']->alias != 'workday' && $days[$w][$d]['check']->id === null) {
						$days[$w][$d]['check']->formal_start = null;
						$days[$w][$d]['check']->formal_end = null;
					}
				}
			}
        }
        
        return $days;
    }
    
    public function getAllRestrictedDates(&$month) {
		$this->allRestrictedDates = [];
		foreach(RestrictedDateStatus::all() as $date_status) {
			foreach($date_status->getDays() as $day) {
				foreach($month as $w => $week) {
					if (array_key_exists(($day + 1), $week)) {
						$this->allRestrictedDates[$week[$day + 1]['dt']->format('Y-m-d')] = $date_status;
						$month[$w][$day + 1]['restricted'] = $date_status;
					}
				}
			}
		}
		$dt = null;
		foreach($month as $w => $week) {
			foreach($week as $d => $day) {
				if ($day['dt']->format('j') == 1 && ($dt === null || $dt->format('n') > $dat['dt']->format('n'))) {
					$dt = $day['dt']->copy();
					break 2;
				}
			}
		}
		
		if ($dt === null) {
			$days_of_firstweek = array_values($month)[1];
			$dt = array_values($days_of_firstweek)[2]['dt']->copy()->startOfMonth();
		}
		
		$_restricted_dates = RestrictedDate::getMonth($dt->format('Y'), $dt->format('n'));
		
		foreach($_restricted_dates as $rdate) {
			$this->allRestrictedDates[$rdate->date] = $rdate->status;
			foreach($month as $w => $week) {
				$break = false;
				foreach($week as $d => $day) {
					if ($day['dt']->format('Y-m-d') == $rdate->date) {
						$month[$w][$d]['restricted'] = $rdate->status;
						$break = true;
						break;
					}
				}
				if ($break) {
					break;
				}
			}
		}
		
		foreach($this->restrictedDates($dt->format('Y'), $dt->format('n'))->get() as $rdate) {
			$this->allRestrictedDates[$rdate->date] = $rdate->status;
			foreach($month as $w => $week) {
				$break = false;
				foreach($week as $d => $day) {
					if ($day['dt']->format('Y-m-d') == $rdate->date) {
						$month[$w][$d]['restricted'] = $rdate->status;
						$break = true;
						break;
					}
				}
				if ($break) {
					break;
				}
			}
		}
		
        return $this->allRestrictedDates;
    }
	
	public function closedChecks(int $year, int $month = null, $full = true) {
		$year_slice = ceil($month/3);
		$between_start = $full ? 1 : ((($year_slice-1)*3)+1);
		if (!array_key_exists($year.'-'.$month, $this->closedChecks)) {
			$worker = $this;
			$closedchecks = DB::table('closed_months')
			//->selectRaw('*, (CASE WHEN restricted_worker_dates.id THEN restricted_worker_date_statuses.alias WHEN restricted_dates.id THEN restricted_date_statuses.alias ELSE NULL END) AS status_alias')
			->selectRaw('*, DATE(checks.configured_check_in) AS groupdate')
			->join('closed_dates', function ($join) {
				$join->on('closed_dates.closed_month_id', '=', 'closed_months.id');
			})
			->leftJoin('checks', function($join) use($worker) {
				$join->on('checks.id', '=', 'closed_dates.check_id');
			})
			/*->leftJoin('restricted_dates', function($join) {
				$join->on('restricted_dates.date', '=', DB::raw('DATE(checks.configured_check_in)'));
			})
			->leftJoin('restricted_date_statuses', function($join) {
				$join->on('restricted_date_statuses.id', '=', 'restricted_dates.restricted_date_status_id');
			})
			->leftJoin('restricted_worker_dates', function($join) {
				$join->on('restricted_worker_dates.date', '=', DB::raw('DATE(checks.configured_check_in)'));
				$join->on('restricted_worker_dates.worker_id', '=', 'checks.worker_id');
			})
			->leftJoin('restricted_worker_date_statuses', function($join) {
				$join->on('restricted_worker_date_statuses.id', '=', 'restricted_worker_dates.restricted_worker_date_status_id');
			})*/
			->where('checks.worker_id', '=', $worker->id)
			->whereRaw('YEAR(closed_months.date) = '.$year);
			if ($month !== null) {
				$closedchecks = $closedchecks->whereRaw('MONTH(closed_months.date) BETWEEN '.$between_start.' AND '.$month.'');
			}
			
			$closedchecks = $closedchecks->whereRaw('((checks.configured_check_in IS NOT NULL AND checks.configured_check_out IS NOT NULL) OR checks.id IS NULL)');
			
			$closedchecks = $closedchecks->get();
			$this->closedChecks[$year.'-'.$month] = [];
			$standards = RestrictedDateStatus::all();
			foreach($closedchecks as $check) {
				$_check = $check;
				$this->checkFormat($_check);
				if (count($standards)) {
					$dt = Carbon::parse($_check->groupdate);
					foreach($standards as $rest) {
						if ($rest->hasDay($dt->format('N')-1)) {
							$_check->status_alias = $rest->alias;
							break;
						}
					}
				}
				$this->closedChecks[$year.'-'.$month][$_check->groupdate] = $_check;
			}
			
			$closedrests = DB::table('closed_months')
			->selectRaw('*, restricted_worker_date_statuses.alias status_alias, restricted_worker_dates.date AS groupdate')
			->leftJoin('restricted_worker_dates', function($join) {
				$join->on(DB::raw('DATE_FORMAT(`restricted_worker_dates`.`date`, "%Y-%m")'), '=', DB::raw('DATE_FORMAT(`closed_months`.`date`, "%Y-%m")'));
			})
			->leftJoin('restricted_worker_date_statuses', function($join) {
				$join->on('restricted_worker_date_statuses.id', '=', 'restricted_worker_dates.restricted_worker_date_status_id');
			})
			->where('restricted_worker_dates.worker_id', '=', $worker->id)
			->whereRaw('YEAR(closed_months.date) = '.$year);
			if ($month !== null) {
				$closedrests = $closedrests->whereRaw('MONTH(closed_months.date) BETWEEN '.$between_start.' AND '.$month.'');
			}
			$closedrests->groupBy('groupdate');
			
			$closedworkerrests = DB::table('closed_months')
			->selectRaw('*, restricted_date_statuses.alias AS status_alias, restricted_dates.date AS groupdate')
			->leftJoin('restricted_dates', function($join) {
				$join->on(DB::raw('DATE_FORMAT(`restricted_dates`.`date`, "%Y-%m")'), '=', DB::raw('DATE_FORMAT(`closed_months`.`date`, "%Y-%m")'));
			})
			->leftJoin('restricted_date_statuses', function($join) {
				$join->on('restricted_date_statuses.id', '=', 'restricted_dates.restricted_date_status_id');
			})
			->whereRaw('YEAR(closed_months.date) = '.$year)
			->whereNotNull('restricted_dates.id');
			if ($month !== null) {
				$closedworkerrests = $closedworkerrests->whereRaw('MONTH(closed_months.date) BETWEEN '.$between_start.' AND '.$month.'');
			}
			$closedworkerrests->groupBy('groupdate');
			
			$closedrests = $closedrests->get();
			
			foreach ($closedrests as $rest) {
				if (array_key_exists($rest->groupdate, $this->closedChecks[$year.'-'.$month])) {
					continue;
				}
				$this->checkFormat($rest);
				$this->closedChecks[$year.'-'.$month][$rest->groupdate] = $rest;
			}
			$closedworkerrests = $closedworkerrests->get();
			foreach ($closedworkerrests as $rest) {
				if (array_key_exists($rest->groupdate, $this->closedChecks[$year.'-'.$month])) {
					continue;
				}
				$this->checkFormat($rest);
				$this->closedChecks[$year.'-'.$month][$rest->groupdate] = $rest;
			}
			$start_dt = Carbon::parse($year.'-'.$month.'-1')->startOfMonth();
			$end_dt = $start_dt->copy()->endOfMonth();
			
			while ($start_dt < $end_dt) {
				if (!array_key_exists($start_dt->format('Y-m-d'), $this->closedChecks[$year.'-'.$month])) {
					$check = new \stdClass;
					$check->id = null;
					$this->checkFormat($check);
					foreach($standards as $rest) {
						if ($rest->hasDay($start_dt->format('N')-1)) {
							$check->status_alias = $rest->alias;
							break;
						}
					}
					$this->closedChecks[$year.'-'.$month][$start_dt->format('Y-m-d')] = $check;
				}
				$start_dt->addDay();
			}
		}
		
		if (Carbon::parse(ClosedMonth::first()->date)->year == $year) {
			$closedchecks = Check::where('worker_id', $this->id)->get();
			foreach($closedchecks as $check) {
				$_check = $check;
				$this->checkFormat($_check);
				if (count($standards)) {
					$dt = Carbon::parse($_check->groupdate);
					foreach($standards as $rest) {
						if ($rest->hasDay($dt->format('N')-1)) {
							$_check->status_alias = $rest->alias;
							break;
						}
					}
				}
				$this->closedChecks[$year.'-'.$month][$_check->groupdate] = $_check;
			}
			
			$closedworkerrests = RestrictedWorkerDate::where('worker_id', $this->id)->get();
			foreach ($closedworkerrests as $rest) {
				if (array_key_exists($rest->groupdate, $this->closedChecks[$year.'-'.$month])) {
					continue;
				}
				$this->checkFormat($rest);
				$this->closedChecks[$year.'-'.$month][$rest->groupdate] = $rest;
			}
			$start_dt = Carbon::parse($year.'-'.$month.'-1')->startOfMonth();
			$end_dt = $start_dt->copy()->endOfMonth();
			
			while ($start_dt < $end_dt) {
				if (!array_key_exists($start_dt->format('Y-m-d'), $this->closedChecks[$year.'-'.$month])) {
					$check = new \stdClass;
					$check->id = null;
					$this->checkFormat($check);
					foreach($standards as $rest) {
						if ($rest->hasDay($start_dt->format('N')-1)) {
							$check->status_alias = $rest->alias;
							break;
						}
					}
					$this->closedChecks[$year.'-'.$month][$start_dt->format('Y-m-d')] = $check;
				}
				$start_dt->addDay();
			}
			
		}
		
		ksort($this->closedChecks[$year.'-'.$month]);
		return $this->closedChecks[$year.'-'.$month];
	}
	
	protected function checkFormat(&$out_check, $in_check = null) {
		if ($in_check === null) {
			$in_check = $out_check;
		}
		
		$out_check->check_in = null;
		$out_check->rounded_check_in = null;
		$in_check_in_cb = null;
		$in_check_out_cb = null;;
		if (isset($in_check->configured_check_in) && $in_check->configured_check_in) {
			$in_check_in_cb = Carbon::createFromFormat('Y-m-d H:i:s', $in_check->configured_check_in);
			$in_check_in_rounded_cb = LsHelper::roundTime($in_check_in_cb->copy());
			$out_check->check_in = $in_check_in_cb->format('H:i');
			$out_check->rounded_check_in = $in_check_in_rounded_cb->format('H:i');
		}
		$out_check->check_out = null;
		$out_check->rounded_check_out = null;
		if (isset($in_check->configured_check_out) && $in_check->configured_check_out) {
			$in_check_out_cb = Carbon::createFromFormat('Y-m-d H:i:s', $in_check->configured_check_out);
			$in_check_out_rounded_cb = LsHelper::roundTime($in_check_out_cb->copy());
			$out_check->check_out = $in_check_out_cb->format('H:i');
			$out_check->rounded_check_out = $in_check_out_rounded_cb->format('H:i');
		}
		
		if ($in_check_in_cb !== null || $in_check_out_cb !== null) {
			$dt = $in_check_in_cb ? $in_check_in_cb : $in_check_out_cb;
			$dt = $dt->copy();
			$groupdate = $this->group->getGroupDate($dt);
			
			if ($in_check->configured_formal_start === null) {
				$in_check->configured_formal_start = $dt->format('Y-m-d') . ' ' . LsHelper::timeFormat($groupdate->start, false, 0, true) . ':00';
			}
			if ($in_check->configured_formal_end === null) {
				$end_dt = $dt->copy();
				if (LsHelper::timeExp($groupdate->start) > LsHelper::timeExp($groupdate->end)) {
					$end_dt->addDay();
				}
				$in_check->configured_formal_end = $end_dt->format('Y-m-d') . ' ' . LsHelper::timeFormat($groupdate->end, false, 0, true) . ':00';
			}
		}
		
		$out_check->formal_start = null;
		if (isset($in_check->configured_formal_start) && $in_check->configured_formal_start) {
			$formal_start_cb = Carbon::createFromFormat('Y-m-d H:i:s', $in_check->configured_formal_start);
			$out_check->formal_start = $formal_start_cb->format('H:i');
		}
		$out_check->formal_end = null;
		if (isset($in_check->configured_formal_end) && $in_check->configured_formal_end) {
			$formal_end = Carbon::createFromFormat('Y-m-d H:i:s', $in_check->configured_formal_end);
			$out_check->formal_end = $formal_end->format('H:i');
		}
		if (isset($in_check->configured_check_in) && isset($in_check->configured_check_out) && $in_check->configured_check_in && $in_check->configured_check_out) {
			$diff = $in_check_in_cb->diff($in_check_out_cb);
			$out_check->diff = $diff->format("%H:%I");
			$rounded_diff = $in_check_in_rounded_cb->diff($in_check_out_rounded_cb);
			$out_check->rounded_diff = $rounded_diff->format("%H:%I");
		}
		
		$out_check->normal_time = LsHelper::timeOperation($out_check->formal_start, $out_check->formal_end, $out_check->check_in, $out_check->check_out)->format('%H:%I');
		
		$out_check->rounded_normal_time = LsHelper::timeOperation($out_check->formal_start, $out_check->formal_end, $out_check->rounded_check_in, $out_check->rounded_check_out)->format('%H:%I');
		
		$out_check->over_time = LsHelper::timeOperation($out_check->formal_start, $out_check->formal_end, $out_check->check_in, $out_check->check_out, 'outer')->format('%H:%I');
		$out_check->rounded_over_time = LsHelper::timeOperation($out_check->formal_start, $out_check->formal_end, $out_check->rounded_check_in, $out_check->rounded_check_out, 'outer')->format('%H:%I');
		
		$out_check->id = $in_check->id;
	}
	
	public function getInfo(int $year, int $month = null, $closed = true) {
		if ($this->info === null) {
			$this->info = ['over_workhours' => 0, 'normal_time' => 0, 'over_time' => 0, 'global_hours' => 0, 'counts' => [], 'hours' => [],
						   'over_workhours_in_month' => 0, 'normal_time_in_month' => 0, 'over_time_in_month' => 0, 'global_hours_in_month' => 0,  'counts_in_month' => [],  'hours_in_month' => [],
						   'over_workhours_in_quarter' => 0, 'normal_time_in_quarter' => 0, 'over_time_in_quarter' => 0, 'global_hours_in_quarter' => 0,  'counts_in_quarter' => [],  'hours_in_quarter' => []];
			//dd($this->closedChecks($year, $month));
			foreach(RestrictedWorkerDateStatus::all() as $status) {
				$this->info['counts'][$status->alias] = 0;
				$this->info['counts_in_month'][$status->alias] = 0;
				$this->info['counts_in_quarter'][$status->alias] = 0;
				$this->info['hours'][$status->alias] = 0;
				$this->info['hours_in_month'][$status->alias] = 0;
				$this->info['hours_in_quarter'][$status->alias] = 0;
			}
			foreach(RestrictedDateStatus::all() as $status) {
				$this->info['counts'][$status->alias] = 0;
				$this->info['counts_in_month'][$status->alias] = 0;
				$this->info['counts_in_quarter'][$status->alias] = 0;
				$this->info['hours'][$status->alias] = 0;
				$this->info['hours_in_month'][$status->alias] = 0;
				$this->info['hours_in_quarter'][$status->alias] = 0;
			}
			$year_slice = ceil($month/3);
			$between_start = (int)(($year_slice-1)*3)+1;
			
			$this->info['quarter'] = $year_slice;
			
			$counted_days = [];
			$closed_months = [];
			
			$method_date = Carbon::createFromDate($year, $month, 1)->startOfMonth();
			
			foreach($this->closedChecks($year, $month, true) as $key => $check) {
				if (!$key) {
					continue;
				}
				$counted_days[] = $key;
				$date = Carbon::parse($key);
				if (!$closed && $date->format('Y-m') == $method_date->format('Y-m')) {
					continue;
				}
				if (!in_array($date->format('Y-m'), $closed_months)) {
					$closed_months[] = $date->format('Y-m');
				}
				$groupdate = $this->group->getGroupDate($date);
				$expected_hours = LSHelper::timeOperation($groupdate->start, $groupdate->end, $groupdate->start, $groupdate->end)->h;
				$this->info['normal_time'] += Carbon::parse($check->rounded_normal_time)->hour;;
				$this->info['over_time'] += Carbon::parse($check->rounded_over_time)->hour;
				if (!array_key_exists($check->status_alias, $this->info['counts'])) {
					$this->info['counts'][$check->status_alias] = 0;
					$this->info['hours'][$check->status_alias] = 0;
				}
				$this->info['counts'][$check->status_alias]++;
				$this->info['hours'][$check->status_alias] += $expected_hours;
				
				if ($date->year == $year && $date->month == $month) {
					$this->info['normal_time_in_month'] += Carbon::parse($check->rounded_normal_time)->hour;;
					$this->info['over_time_in_month'] += Carbon::parse($check->rounded_over_time)->hour;
					if (!array_key_exists($check->status_alias, $this->info['counts_in_month'])) {
						$this->info['counts_in_month'][$check->status_alias] = 0;
						$this->info['hours_in_month'][$check->status_alias] = 0;
					}
					$this->info['counts_in_month'][$check->status_alias]++;
					$this->info['hours_in_month'][$check->status_alias] += $expected_hours;
				}
				if ($date->year == $year && $date->month >= $between_start && $date->month <= $month) {
					$this->info['normal_time_in_quarter'] += Carbon::parse($check->rounded_normal_time)->hour;
					$this->info['over_time_in_quarter'] += Carbon::parse($check->rounded_over_time)->hour;
					if (!array_key_exists($check->status_alias, $this->info['counts_in_quarter'])) {
						$this->info['counts_in_quarter'][$check->status_alias] = 0;
						$this->info['hours_in_quarter'][$check->status_alias] = 0;
					}
					$this->info['counts_in_quarter'][$check->status_alias]++;
					$this->info['hours_in_quarter'][$check->status_alias] += $expected_hours;
				}
				
			}
			
			if (!$closed || !in_array($method_date->format('Y-m'), $closed_months)) {
				foreach($this->getMonthChecks($year, $month) as $key => $check) {
					if (!$key) {
						continue;
					}
					$counted_days[] = $key;
					$date = Carbon::parse($key);
					$groupdate = $this->group->getGroupDate($date);
					$restricted = $this->getWorkerDay($date);
					$check->status_alias = $restricted->alias;
					$expected_hours = LSHelper::timeOperation($groupdate->start, $groupdate->end, $groupdate->start, $groupdate->end)->h;
					$this->info['normal_time'] += Carbon::parse($check->rounded_normal_time)->hour;;
					$this->info['over_time'] += Carbon::parse($check->rounded_over_time)->hour;
					if (!array_key_exists($check->status_alias, $this->info['counts'])) {
						$this->info['counts'][$check->status_alias] = 0;
						$this->info['hours'][$check->status_alias] = 0;
					}
					$this->info['counts'][$check->status_alias]++;
					$this->info['hours'][$check->status_alias] += $expected_hours;
					if ($date->year == $year && $date->month == $month) {
						$this->info['normal_time_in_month'] += Carbon::parse($check->rounded_normal_time)->hour;;
						$this->info['over_time_in_month'] += Carbon::parse($check->rounded_over_time)->hour;
						if (!array_key_exists($check->status_alias, $this->info['counts_in_month'])) {
							$this->info['counts_in_month'][$check->status_alias] = 0;
							$this->info['hours_in_month'][$check->status_alias] = 0;
						}
						$this->info['counts_in_month'][$check->status_alias]++;
						$this->info['hours_in_month'][$check->status_alias] += $expected_hours;
					}
					if ($date->year == $year && $date->month >= $between_start && $date->month <= $month) {
						$this->info['normal_time_in_quarter'] += Carbon::parse($check->rounded_normal_time)->hour;
						$this->info['over_time_in_quarter'] += Carbon::parse($check->rounded_over_time)->hour;
						if (!array_key_exists($check->status_alias, $this->info['counts_in_quarter'])) {
							$this->info['counts_in_quarter'][$check->status_alias] = 0;
							$this->info['hours_in_quarter'][$check->status_alias] = 0;
						}
						$this->info['counts_in_quarter'][$check->status_alias]++;
						$this->info['hours_in_quarter'][$check->status_alias] += $expected_hours;
					}
					
				}
			}
			
			$date = $method_date->copy();
			$end_date = $date->copy()->endOfMonth();
			while ($date <= $end_date) {
				if (!in_array($date->toDateString(), $counted_days)) {
					$restricted = $this->getWorkerDay($date->toDateString(), true);
					$groupdate = $this->group->getGroupDate($date);
					$expected_hours = LSHelper::timeOperation($groupdate->start, $groupdate->end, $groupdate->start, $groupdate->end)->h;
					if (!array_key_exists($restricted->alias, $this->info['counts'])) {
						$this->info['counts'][$restricted->alias] = 0;
						$this->info['hours'][$restricted->alias] = 0;
					}
					$this->info['counts'][$restricted->alias]++;
					$this->info['hours'][$restricted->alias] += $expected_hours;
					if ($date->year == $year && $date->month == $month) {
						if (!array_key_exists($restricted->alias, $this->info['counts_in_month'])) {
							$this->info['counts_in_month'][$restricted->alias] = 0;
							$this->info['hours_in_month'][$restricted->alias] = 0;
						}
						$this->info['counts_in_month'][$restricted->alias]++;
						$this->info['hours_in_month'][$restricted->alias] += $expected_hours;
					}
					if ($date->year == $year && $date->month >= $between_start && $date->month <= $month) {
						if (!array_key_exists($restricted->alias, $this->info['counts_in_quarter'])) {
							$this->info['counts_in_quarter'][$restricted->alias] = 0;
							$this->info['hours_in_quarter'][$restricted->alias] = 0;
						}
						$this->info['counts_in_quarter'][$restricted->alias]++;
						$this->info['hours_in_quarter'][$restricted->alias] += $expected_hours;
					}
					$counted_days[] = $date->toDateString();
				}
				$date->addDay();
			}
			
			$this->info['expected_time'] = $this->info['counts']['workday'] * 8;
			$this->info['expected_time_in_month'] = $this->info['counts_in_month']['workday'] * 8;
			$this->info['expected_time_in_quarter'] = $this->info['counts_in_quarter']['workday'] * 8;
			
			$start_month = 1;
			while ($start_month <= 12) {
				$this->info['global_hours'] += $this->getGlobalHours(Carbon::parse($year.'-'.$start_month.'-1'));
				if ($month == $start_month) {
					$this->info['global_hours_in_month'] = $this->getGlobalHours(Carbon::parse($year.'-'.$start_month.'-1'));
				}
				if ($between_start <= $start_month && $month >= $start_month) {
					$this->info['global_hours_in_quarter'] += $this->getGlobalHours(Carbon::parse($year.'-'.$start_month.'-1'));
				}
				$start_month++;
			}
			
			$this->info['over_workhours'] = ($this->info['over_time'] + $this->info['normal_time'] + $this->info['hours']['free_day'] + $this->info['hours']['proven_day'] + $this->info['hours']['sick_leave']) - $this->info['global_hours'];
			//dd($this->info['over_time'], $this->info['normal_time'], $this->info['hours']['free_day'], $this->info['hours']['sick_leave']);
			
			$this->info['over_workhours_in_month'] = ($this->info['over_time_in_month'] + $this->info['normal_time_in_month'] + $this->info['hours_in_month']['free_day'] + $this->info['hours_in_month']['proven_day'] + $this->info['hours_in_month']['sick_leave']) - $this->info['global_hours_in_month'];
			//dd($this->info['over_time_in_month'], $this->info['normal_time_in_month'], $this->info['hours_in_month']['free_day'], $this->info['hours_in_month']['sick_leave'], $this->info['global_hours_in_month']);
			
			$this->info['over_workhours_in_quarter'] = ($this->info['over_time_in_quarter'] + $this->info['normal_time_in_quarter'] + $this->info['hours_in_quarter']['free_day'] + $this->info['hours_in_quarter']['proven_day'] + $this->info['hours_in_quarter']['sick_leave']) - $this->info['global_hours_in_quarter'];
		}
		
		return $this->info;
	}
	
	public function exportMonth($date, $directory, $closed_month = null) {
		$worker = $this;
		$storage_path = storage_path($directory);
		$filename = $worker->nr . '_' . $worker->name . '_'. $date->year .'-'. $date->month;
		$format = 'xls';
		
		Excel::create($filename, function($excel) use ($closed_month, $worker, $date)
			{
				$excel->sheet($date->year .'-'. $date->month, function($sheet) use ($closed_month, $worker, $date)
				{
					$month = $worker->getMonthDays($date->year, $date->month, true, true);
					$worker_info = $worker->getInfo($date->year, $date->month, ($closed_month !== null));
					$sheet->setCellValue('A1', 'MUNKAIDŐ JEGYZÉK');
					$sheet->mergeCells('A1:N1');
					$sheet->cell('A1', function ($cells) {
						$cells->setFontSize(14);
						$cells->setFontWeight('bold');
						$cells->setAlignment('center');
						$cells->setValignment('top');
					});
					
					$sheet->setCellValue('A2', $date->year . '. ' . $date->month);
					$sheet->setCellValue('B2', 'Munkavállaló neve, azonosító:');
					$sheet->mergeCells('B2:C2');
					$sheet->cells('B2:C2', function ($cells) {
						$cells->setFontWeight('bold');
					});
					$sheet->setCellValue('D2', $worker->name . ', ' . $worker->nr);
					$sheet->mergeCells('D2:I2');
					$sheet->cells('D2:I2', function ($cells) {
						$cells->setFontSize(14);
						$cells->setFontWeight('bold');
					});
					
					$sheet->setCellValue('J2', 'Generálás ideje');
					$sheet->mergeCells('J2:K2');
					if ($closed_month === null) {
						$sheet->setCellValue('L2', Carbon::now()->format('Y-m-d H:i:s'));
					} else {
						$sheet->setCellValue('L2', $closed_month->created_at->format('Y-m-d H:i:s'));
					}
					
					$sheet->mergeCells('L2:N2');
					$sheet->cells('L2:N2', function ($cells) {
						$cells->setAlignment('right');
					});
					
					$sheet->cells('A2:N2', function ($cells) {
						 $cells->setValignment('center');
					});
					$sheet->setHeight(array(
											1	=>	30,
										   ));
					
					$sheet->setCellValue('A4', 'Foglalkoztató neve');
					$sheet->mergeCells('A4:B4');
					$sheet->setCellValue('A5', 'Szervezeti egység');
					$sheet->mergeCells('A5:B5');
					$sheet->cells('A5:B5', function ($cells) {
						$cells->setFontWeight('bold');
					});
					
					$sheet->setCellValue('C4', $worker->company->name);
					$sheet->mergeCells('C4:E4');
					$sheet->setCellValue('C5', $worker->job->name);
					$sheet->mergeCells('C5:E5');
					
					$sheet->setCellValue('A7', 'Nap');
					$sheet->mergeCells('A7:A8');
					$sheet->setCellValue('B7', 'Nap típusa');
					$sheet->mergeCells('B7:B8');
					
					$sheet->setCellValue('C7', 'Beosztás');
					$sheet->mergeCells('C7:D7');
					$sheet->setCellValue('C8', 'Kezdés');
					$sheet->setCellValue('D8', 'Zárás');
					
					$sheet->setCellValue('E7', 'Munkaidő');
					$sheet->mergeCells('E7:F7');
					$sheet->setCellValue('E8', 'Kezdés');
					$sheet->setCellValue('F8', 'Zárás');
					
					$sheet->setCellValue('G7', 'Normál');
					$sheet->setCellValue('G8', '(óra)');
					$sheet->setCellValue('H7', 'SZ');
					$sheet->setCellValue('H8', '(óra)');
					$sheet->setCellValue('I7', 'BSZ');
					$sheet->setCellValue('I8', '(óra)');
					$sheet->setCellValue('J7', 'RM 50%');
					$sheet->setCellValue('J8', '(óra)');
					$sheet->setCellValue('K7', 'VP 50%');
					$sheet->setCellValue('K8', '(óra)');
					$sheet->setCellValue('L7', 'Ü 100%');
					$sheet->setCellValue('L8', '(óra)');
					$sheet->setCellValue('M7', 'MP 30%');
					$sheet->setCellValue('M8', '(óra)');
					$sheet->setCellValue('N7', 'ÉP 15%');
					$sheet->setCellValue('N8', '(óra)');
					
					$sheet->cells('A2:N2', function ($cells) {
						 $cells->setValignment('center');
					});
					
					$sheet->cells('A7:N8', function ($cells) {
						 $cells->setFontWeight('bold');
						 $cells->setValignment('center');
					});
					
					foreach (range ('G', 'N') as $letter) {
						$sheet->cell($letter.'7', function ($cell) {
							$cell->setBorder('thin', 'thin', 'none', 'thin');
						});
					}
					foreach (range ('G', 'N') as $letter) {
						$sheet->cell($letter.'8', function ($cell) {
							$cell->setBorder('none', 'thin', 'thin', 'thin');
						});
					}
					
					$start_date = $date->copy()->startOfMonth();
					$end_date = $date->copy()->endOfMonth();
					
					$_days = [];
					
					foreach($month as $week) {
						foreach($week as $day) {
							$_days[$day['dt']->format('Y-m-d')] = $day;
						}
					}
					$workdays = 0;
					$free_days = 0;
					$sick_leaves = 0;
					$free_day_hours = 0;
					$sick_leave_hours = 0;
					$sum_normal = '00:00';
					$over_time = '00:00';
					$cell_i = '00:00';
					$cell_j = '00:00';
					$cell_k = 0;
					$cell_l = 0;
					
					for($i = 9; $start_date < $end_date; $i++)
					{
						$sheet->setCellValue('A' . $i, $start_date->format('Y-m-d'));
						if (array_key_exists($start_date->format('Y-m-d'), $_days) && $_day = $_days[$start_date->format('Y-m-d')]) {
							$sheet->setCellValue('B' . $i, $_day['restricted']->name);
							if ($_day['restricted']->alias == 'workday') {
								$workdays++;
							}
							
							if ($_day['restricted']->alias == 'free_day' || $_day['restricted']->alias == 'proven_day' || $_day['restricted']->alias == 'sick_leave') {
								$expected_hours = LSHelper::timeOperation($_day['groupdate']->start, $_day['groupdate']->end, $_day['groupdate']->start, $_day['groupdate']->end)->h;
							}
							
							if ($_day['restricted']->alias == 'free_day') {
								$sheet->setCellValue('H' . $i, LsHelper::timeFormat($expected_hours));
								$free_days++;
								$free_day_hours += $expected_hours;
							}
							if ($_day['restricted']->alias == 'sick_leave') {
								$sheet->setCellValue('I' . $i, LsHelper::timeFormat($expected_hours));
								$sick_leaves++;
								$sick_leave_hours += $expected_hours;
							}
							
							
							if ($_day['restricted']->alias == 'day_off') {
								$sheet->cells('A'.$i.':N'.$i, function ($cells) {
									$cells->setBackground('#DDDDDD');
								});
							}
							
							if ($_day['restricted']->alias == 'day_off_sunday') {
								$sheet->cells('A'.$i.':N'.$i, function ($cells) {
									$cells->setBackground('#CCCCCC');
								});
							}
							
							$sheet->setCellValue('C' . $i, $_day['check']->formal_start);
							$sheet->setCellValue('D' . $i, $_day['check']->formal_end);
							if ($_day['check'] && $_day['check']->id !== null) {
								
								//dd($_day['check']);
								
								$sheet->setCellValue('E' . $i, $_day['check']->rounded_check_in);
								$sheet->setCellValue('F' . $i, $_day['check']->rounded_check_out);
								$sheet->setCellValue('G' . $i, LsHelper::timeFormat($_day['check']->rounded_normal_time, true));
								$sum_normal = LsHelper::timeMod($sum_normal, $_day['check']->rounded_normal_time);
								
								$sheet->setCellValue('J' . $i, LsHelper::timeFormat($_day['check']->rounded_over_time, true));
								$over_time = LsHelper::timeMod($over_time, $_day['check']->rounded_over_time);
								
								/*if ($start_date->format('N') == 7 || $_day['restricted']->alias == 'day_off_sunday') {
									$sheet->setCellValue('K' . $i, LsHelper::timeFormat($_day['check']->rounded_diff, true));
									$cell_i = LsHelper::timeMod($cell_i, $_day['check']->rounded_diff);
								}*/
								$check_in_h = LsHelper::timeExp($_day['check']->rounded_check_in);
								$check_out_h = LsHelper::timeExp($_day['check']->rounded_check_out);
								if (($start_date->format('N') == 7 || $_day['restricted']->alias == 'day_off_sunday')) {
									if ($check_in_h > $check_out_h) {
										//$cell_i = LsHelper::timeOperation($_day['check']->check_in, '00:00', $_day['check']->check_in, '00:00')->format('%H:%I');
										$i_operation = LsHelper::timeOperation($_day['check']->rounded_check_in, '00:00', $_day['check']->rounded_check_in, '00:00')->format('%H:%I');
									} else {
										$i_operation =  LsHelper::timeOperation('00:00', $_day['check']->rounded_check_out, '00:00', $_day['check']->rounded_check_out)->format('%H:%I');
									}
									$sheet->setCellValue('K' . $i, $i_operation);
									$cell_i = LsHelper::timeMod($cell_i, $i_operation);
								} elseif (($start_date->format('N') == 6 || (isset($_days[$start_date->copy()->addDay()->format('Y-m-d')]) && $_days[$start_date->copy()->addDay()->format('Y-m-d')]['restricted']->alias == 'day_off_sunday')) && $check_in_h > $check_out_h) {
									$i_operation = LsHelper::timeOperation('00:00', $_day['check']->rounded_check_out, '00:00', $_day['check']->rounded_check_out)->format('%H:%I');
									$sheet->setCellValue('K' . $i, LSHelper::timeFormat($i_operation));
									$cell_i = LsHelper::timeMod($cell_i, $i_operation);
								}
								
								if ($_day['restricted']->alias == 'holiday') {
									$sheet->setCellValue('L' . $i, LsHelper::timeFormat($_day['check'], true));
									$cell_j = LsHelper::timeMod($cell_j, $_day['check']->rounded_diff);
								} elseif ((isset($_days[$start_date->copy()->addDay()->format('Y-m-d')]) && $_days[$start_date->copy()->addDay()->format('Y-m-d')]['restricted']->alias == 'holiday') && $check_in_h > $check_out_h) {
									$j_operation = LsHelper::timeOperation('00:00', $_day['check']->rounded_check_out, '00:00', $_day['check']->rounded_check_out)->format('%H:%I');
									$sheet->setCellValue('L' . $i, LSHelper::timeFormat($j_operation));
									$cell_j = LsHelper::timeMod($cell_j, $j_operation);
								}
								
								//if ($worker->group->dates->count() > 1) {
								if (!$worker->nightBonusMonth($start_date->format('Y'), $start_date->format('n'))) {
									$_k = LsHelper::timeOperation('18:00', '06:00', $_day['check']->rounded_check_in, $_day['check']->rounded_check_out, 'inner')->format('%h');
									$sheet->setCellValue('M' . $i, LsHelper::timeFormat($_k, true));
									$cell_k += $_k;
								} else {
									$_l = LsHelper::timeOperation('22:00', '06:00', $_day['check']->rounded_check_in, $_day['check']->rounded_check_out, 'inner')->format('%h');
									$sheet->setCellValue('N' . $i, LsHelper::timeFormat($_l, true));
									$cell_l += $_l;
								}
								
								//dd($_day['check']);
							} /*elseif ($_day['restricted']->alias == 'workday') {
								$sheet->setCellValue('C' . $i, Carbon::parse($_day['groupdate']->start)->format('H:i'));
								$sheet->setCellValue('D' . $i, Carbon::parse($_day['groupdate']->end)->format('H:i'));
							}*/
						}
						$start_date->addDay();
					}
					//dd($sum_normal);
					//$i++;
					
					$sheet->setCellValue('A'.$i, 'Nap');
					$sheet->mergeCells('A'.$i.':A'.($i+1));
					$sheet->setCellValue('B'.$i, 'Nap típusa');
					$sheet->mergeCells('B'.$i.':B'.($i+1));
					
					$sheet->setCellValue('C'.$i, 'Beosztás');
					$sheet->mergeCells('C'.$i.':D'.$i);
					$sheet->setCellValue('C'.($i+1), 'Kezdés');
					$sheet->setCellValue('D'.($i+1), 'Zárás');
					
					$sheet->setCellValue('E'.$i, 'Munkaidő');
					$sheet->mergeCells('E'.$i.':F'.$i);
					$sheet->setCellValue('E'.($i+1), 'Kezdés');
					$sheet->setCellValue('F'.($i+1), 'Zárás');
					
					$sheet->setCellValue('G'.$i, 'Normál');
					$sheet->setCellValue('G'.($i+1), '(óra)');
					$sheet->setCellValue('H'.$i, 'SZ');
					$sheet->setCellValue('H'.($i+1), '(óra)');
					$sheet->setCellValue('I'.$i, 'BSZ');
					$sheet->setCellValue('I'.($i+1), '(óra)');
					$sheet->setCellValue('J'.$i, 'RM 50%');
					$sheet->setCellValue('J'.($i+1), '(óra)');
					$sheet->setCellValue('K'.$i, 'VP 50%');
					$sheet->setCellValue('K'.($i+1), '(óra)');
					$sheet->setCellValue('L'.$i, 'Ü 100%');
					$sheet->setCellValue('L'.($i+1), '(óra)');
					$sheet->setCellValue('M'.$i, 'MP 30%');
					$sheet->setCellValue('M'.($i+1), '(óra)');
					$sheet->setCellValue('N'.$i, 'ÉP 15%');
					$sheet->setCellValue('N'.($i+1), '(óra)');
					
					$sheet->cells('A'.$i.':N'.($i+1), function ($cells) {
						 $cells->setFontWeight('bold');
						 $cells->setValignment('center');
					});
					
					foreach (range ('G', 'N') as $letter) {
						$sheet->cell($letter.$i, function ($cells) {
							$cells->setBorder('thin', 'thin', 'none', 'thin');
							$cells->setValignment('bottom');
						});
					}
					
					$i++;
					
					foreach (range ('G', 'N') as $letter) {
						$sheet->cell($letter.$i, function ($cells) {
							$cells->setBorder('none', 'thin', 'thin', 'thin');
							$cells->setValignment('bottom');
						});
					}
					
					$i++;
					
					$sheet->setCellValue('A' . $i, 'Összesen');
					$sheet->setCellValue('B' . $i, $workdays);
					$sheet->setCellValue('G' . $i, LSHelper::timeFormat($sum_normal));
					$sheet->setCellValue('H' . $i, LSHelper::timeFormat($free_day_hours));
					$sheet->setCellValue('I' . $i, LSHelper::timeFormat($sick_leave_hours));
					$sheet->setCellValue('J' . $i, LSHelper::timeFormat($over_time));
					$sheet->setCellValue('K' . $i, LSHelper::timeFormat($cell_i));
					$sheet->setCellValue('L' . $i, LSHelper::timeFormat($cell_j));
					$sheet->setCellValue('M' . $i, LSHelper::timeFormat($cell_k));
					$sheet->setCellValue('N' . $i, LSHelper::timeFormat($cell_l));
					
					$sheet->cells('C7:N'.$i, function ($cells) {
						 $cells->setAlignment('center');
						 $cells->setValignment('center');
					});
					
					$sheet->setBorder('A7:F'.$i, 'thin');
					$sheet->setBorder('G9:N'.($i-3), 'thin');
					$sheet->setBorder('G'.$i.':N'.$i, 'thin');
					
					$i++;
					$i++;
					
					//dd($worker_info);
					$sheet->mergeCells('A' . $i . ':B' . $i);
					
					$sheet->setCellValue('A' . $i, 'Munkaidő keret a hónapban');
					//$sheet->setCellValue('C' . $i, LSHelper::timeFormat($worker_info['global_hours_in_month']));
					$sheet->setCellValue('C' . $i, LSHelper::timeFormat($worker_info['global_hours_in_month'] - $worker_info['hours_in_month']['sick_pay'] - $worker_info['hours_in_month']['sick_leave'])); // - $worker_info['hours_in_month']['unpaid_leave']
					
					//$sheet->setCellValue('D' . $i, $worker_info['over_time']);
					$sheet->mergeCells('F' . $i . ':H' . $i);
					$sheet->setCellValue('F' . $i, 'Fizetett szabadságkeret');
					$sheet->setCellValue('I' . $i, $worker->annual_free_day);
					$sheet->mergeCells('K' . $i . ':M' . $i);
					$sheet->setCellValue('K' . $i, 'Betegszabadságok keret');
					$sheet->setCellValue('N' . $i, $worker->annual_sick_leave);
					
					$i++;
					
					$sheet->mergeCells('A' . $i . ':B' . $i);
					
					$sheet->setCellValue('A' . $i, 'Munkaidő kereten felüli órák a hónapban');
					$sheet->setCellValue('C' . $i, LSHelper::timeFormat($worker_info['over_workhours_in_month']));
					
					$free_day_count = isset($worker_info['counts']['free_day']) ? $worker_info['counts']['free_day'] : 0;
					$sick_leave_count = isset($worker_info['counts']['sick_leave']) ? $worker_info['counts']['sick_leave'] : 0;
					
					$sheet->mergeCells('F' . $i . ':H' . $i);
					$sheet->setCellValue('F' . $i, 'Fizetett szabadságok');
					$sheet->setCellValue('I' . $i, $free_day_count);
					$sheet->mergeCells('K' . $i . ':M' . $i);
					$sheet->setCellValue('K' . $i, 'Betegszabadságok száma');
					$sheet->setCellValue('N' . $i, $sick_leave_count);
					
					$i++;
					
					$sheet->mergeCells('A' . $i . ':B' . $i);
					$sheet->setCellValue('A' . $i, 'Munkaidő kereten felüli órák a negyedévben');
					//$sheet->setCellValue('C' . $i, LSHelper::timeFormat($date->month%3 == 0? $worker_info['over_workhours_in_quarter'] : 0));
					$sheet->setCellValue('C' . $i, LSHelper::timeFormat($worker_info['over_workhours_in_quarter']));
					
					$sheet->mergeCells('F' . $i . ':H' . $i);
					$sheet->setCellValue('F' . $i, 'Fennmaradó fizetett szab.');
					$sheet->setCellValue('I' . $i, $worker->annual_free_day - $free_day_count);
					$sheet->mergeCells('K' . $i . ':M' . $i);
					$sheet->setCellValue('K' . $i, 'Fennmaradó betegszab.');
					$sheet->setCellValue('N' . $i, $worker->annual_sick_leave - $sick_leave_count);
					
					$i++;
					
					$i = $i + 6;
					
					$sheet->mergeCells('G' . $i . ':M' . $i);
					$sheet->setCellValue('G' . $i, 'Dolgozó aláírása');
					$sheet->cells('G'.$i.':M'.$i, function ($cells) {
						$cells->setBorder('thin', 'none', 'none', 'none');
						$cells->setAlignment('center');
					});
					
					$sheet->setWidth(array(
											'A'	=>	12,
											'B'	=>	28,
											'C'	=>	8,
											'D'	=>	8,
											'E'	=>	8,
											'F'	=>	8,
											'G'	=>	8,
											'H'	=>	8,
											'I'	=>	8,
											'J'	=>	8,
											'K'	=>	8,
											'L'	=>	8,
											'M'	=>	8,
											'N'	=>	8,
											));
					
					//$sheet->setCellValue('A' . ($i+2), var_export($worker_info, true));
					
					$sheet->setOrientation('portrait');
					
				});
			})->store($format, $storage_path);
		return $storage_path . $filename . '.' . $format;
	}
	
	public function nightBonusMonth($year, $month) {
		if (!count($this->nightBonusMonths) && !isset($this->nightBonusMonths[$year.'-'.$month])) {
			$start_hours = [];
			$sum_days = 0;
			foreach($this->getMonthDays($year, $month, true) as $weeks) {
				foreach($weeks as $day) {
					if($day['restricted']->alias != 'workday') {
						continue;
					}
					$start_hour = LsHelper::timeExp($day['check']->formal_start);
					if (!isset($start_hours[$start_hour])) {
						$start_hours[$start_hour] = 0;
					}
					$start_hours[$start_hour]++;
					$sum_days++;
				}
			}
			arsort($start_hours, SORT_NUMERIC);
			$this->nightBonusMonths[$year.'-'.$month] = (current($start_hours) / $sum_days) > 0.67;
		}
		return $this->nightBonusMonths[$year.'-'.$month];
	}
}
