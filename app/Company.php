<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = ['name', 'active', 'overtime'];
	public $timestamps = false;
	
	public function workers()
	{
		return $this->hasMany(Worker::class, 'company_id', 'id');
	}
}
