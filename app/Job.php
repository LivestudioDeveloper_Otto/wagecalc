<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';
    protected $fillable = ['name', 'white_collar'];
	public $timestamps = false;
	
	public function workers()
	{
		return $this->hasMany(Worker::class, 'job_id', 'id');
	}
}
