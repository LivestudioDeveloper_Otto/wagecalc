<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WorkerGroupDate extends Model
{
	public $timestamps = false;
	protected $fillable = ['start', 'end', 'active', 'ordering', 'group_id', 'bonus'];
	
	/* public function getStartAttribute($value)
	{
		return Carbon::parse($value)->format('H:i:s');
	}
	
	public function formStartAttribute($value)
	{
		return Carbon::parse($value)->format('H:i:s');
	} */
}
