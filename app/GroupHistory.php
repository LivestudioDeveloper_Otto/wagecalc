<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupHistory extends Model
{
	protected $table = 'group_history';
	
	protected $fillable = ['date', 'user_id'];
	
    public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}
}
