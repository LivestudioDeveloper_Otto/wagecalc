<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WorkerGroup extends Model
{
	public static $group_dates = [];
	public static $_dates = [];
	
	public $timestamps = false;
	
	protected $fillable = ['name', 'active'];

	/*
	 *	Get relation model with the highest possible date from floor
	 */
	protected function _groupReset(Carbon $date)
	{
		return GroupHistory::where('date', '<=', $date)->orderBy('date', 'desc')->first();
	}
	
	/*
	 *	Get group dates relation model
	 */
    public function dates()
	{
		return $this->hasMany(WorkerGroupDate::class, 'group_id', 'id')->orderBy('ordering', 'asc');
	}
	
	/*
	 *	Calculate group time from date
	 *	@IN:
	 *		- date	Carbon
	 *	@OUT:
	 *		- GroupDate model
	 *	@Error:
	 *		- Exception
	 */
	public function getGroupDate(Carbon $date)
	{
		if(!array_key_exists($this->id, static::$group_dates) || static::$group_dates[$this->id] == null)
		{
			/*
			 *	If we already selected group dates we dont need to select as many times as we call this function
			 */
			static::$group_dates[$this->id] = $this->dates();
		}
		
		if(!array_key_exists($this->id, static::$_dates))
		{
			static::$_dates[$this->id] = [];
		}
		
		if(!array_key_exists($date->format('YmdHis'), static::$_dates[$this->id]))
		{
			$clone_group_dates = clone static::$group_dates[$this->id];
			
			$group_reset = $this->_groupReset($date);
			
			if(!$group_reset)
			{
				throw new \Exception('Nem található kezdési dátum!');
			}
			
			/*
			 *	To given date compare the group history date and calculate which ordering will be at current date
			 *	diff_days / 7 means we only need /per week data, because group date interval is 7 day
			 */
			$diff_days = Carbon::parse($group_reset->date)->diffInDays($date);
			static::$_dates[$this->id][$date->format('YmdHis')] = $clone_group_dates->where('ordering', '=', (($diff_days / 7) % static::$group_dates[$this->id]->count()) + 1)->first();
		}
		
		return static::$_dates[$this->id][$date->format('YmdHis')];
	}
	
	/*
	 *	Get next group date from group
	 *	@IN:
	 *		- id	Current group date id
	 *	@OUT:
	 *		- date	Next group date id
	 */
	public function getNextGroupDate($id)
	{
		if(!array_key_exists($this->id, static::$group_dates) || static::$group_dates[$this->id] == null)
		{
			static::$group_dates[$this->id] = $this->dates();
		}
		
		$found = false;
		$date = null;
		
		foreach(static::$group_dates[$this->id] as $group_date)
		{	
			/*
			 *	store the first element of array
			 *	If we didnt find any then this will be the default
			 *	If the selected was the last element, then the first one will be the next, so we already stored it
			 */
			if($date == null)
			{
				$date = $group_date;
			}
			
			/*
			 *	If we found the previous selected element, so the next will come
			 */
			if($found)
			{
				$date = $group_date;
				break;
			}
			
			if($group_date->id == $id)
			{
				$found = true;
			}
		}
		
		return $date;
	}
}
