<?php

namespace App\Http\Controllers;

use Excel;
use Storage;
use App\ClosedMonth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Worker;
use App\Libraries\LsHelper;
use App\RestrictedDate;
use App\RestrictedDateStatus;
use App\ClosedDate;
use App\WorkerMonthHistory;
use Chumper\Zipper\Zipper;

class ClosedMonthController extends Controller
{
	protected $export_dir = 'export-xls';
	public function __construct()
	{
		$this->middleware('auth');
        parent::__construct();
	}
	
	public function index()
	{
		$closed_months = ClosedMonth::orderBy('date', 'desc')->where('id', '!=', '1')->get();
		return view('admin.closedmonth.default', compact('closed_months'));
	}
	
	public function view(Request $request)
	{		
		$messages = [
			'exists' => 'Nem létezik ilyen lezárt dátum!',
		];
		
		$request->validate([
            'id' => 'exists:closed_months,id'
        ], $messages);
		
		$closed_month = ClosedMonth::find($request->id);
		
		$workers = $closed_month->getWorkers();
		
		return view('admin.closedmonth.view', compact('closed_month', 'workers'));
	}
	
    public function close()
	{
		$active_date = ClosedMonth::getActiveDate();
		$today = Carbon::today();
		
		$invalid_checks = \App\Check::getInvalidChecks($active_date);
		if(count($invalid_checks))
		{
			return redirect()->back()->withErrors(['Vannak nem lezárt dátumok a dolgozóknál!']);
		}
		
		if($active_date->endOfMonth() >= $today)
		{
			return redirect()->back()->withErrors(['A lezárandó hónapot csak a rákövetkező hónaptól (' . $today->startOfMonth()->addMonth()->startOfMonth()->format('Y-m-d') . ') lehet lezárni! ']);
		}
		
		
		$closed_month = ClosedMonth::create(['date' => $active_date->startOfMonth(), 'user_id' => Auth::user()->id]);
		
		/* $checks = \App\Check::whereBetween('configured_check_in', [$active_date->startOfMonth()->format('Y-m-d H:i:s'), $active_date->endOfMonth()->format('Y-m-d H:i:s')])->get();
		
		$bulk = [];
		foreach($checks as $check)
		{
			$group = $check->worker->group->getGroupDate(Carbon::parse($check->configured_check_in));
			
			$bulk[] = [
				'closed_month_id' => $closed_month->id,
				'check_id' => $check->id,
				'overtime' => $check->worker->company->overtime,
				'bonus' => 1,
				'group' => $check->worker->group->name,
				'start' => $group->start,
				'end' => $group->end
			];
			
		}
		
		\App\ClosedDate::insert($bulk); */
				
		try {
			$workers = \App\Worker::all();
			foreach($workers as $worker)
			{
				$bulk = [];
				$worker_checks = $worker->getMonthDays($active_date->year, $active_date->month, false);
				
				\App\WorkerMonthHistory::insert([
					'worker_id' => $worker->id,
					'closed_month_id' => $closed_month->id,
					'annual_free_day' => $worker->annual_free_day,
					'annual_sick_leave' => $worker->annual_sick_leave,
					'annual_sick_pay' => $worker->annual_sick_pay,
					'overtime' => $worker->company->overtime
				]);
							
				foreach($worker_checks as $week => $days)
				{
					foreach($days as $day)
					{
						if($day['check'] !== null && $day['check']->id !== null)
						{
							$bulk[] = [
								'closed_month_id' => $closed_month->id,
								'check_id' => $day['check']->id,
								'bonus' => 1,
								'group' => $worker->group->name,
								'start' => $day['groupdate']->start,
								'end' => $day['groupdate']->end
							];
						}
					}
				}
				
				\App\ClosedDate::insert($bulk);
				$this->_setSessionDate();
			}
		} catch (\Exception $e) {
			ClosedMonth::destroy($closed_month->id);
			return redirect()->back()->withErrors(['Nem várt hiba: '.$e]);
		}
		
		return $this->export($closed_month);
	}
	
	protected function export(ClosedMonth $closed_month)
	{
		$date = Carbon::parse($closed_month->date);
		$directory = $this->export_dir . '/' . $date->year .'-'. $date->month;
		//dd(storage_path($directory));
		
		$workers = Worker::where('active', '=', 1)->get();
		
		//$worker = Worker::find(1);
		foreach($workers as $worker) {
			$worker->exportMonth($date, $directory, $closed_month);
		}
		
		Excel::create($date->year .'-'. $date->month, function($excel) use ($closed_month, $workers, $date)
		{
			$excel->sheet($date->year .'-'. $date->month, function($sheet) use ($closed_month, $workers, $date)
			{
					$sheet->setCellValue('A1', $date->year . '. ' . $date->month);
					
					$sheet->setCellValue('G1', 'Vállalati összesítő');
					//$sheet->mergeCells('G1:H1');
					$sheet->setCellValue('J1', 'Generálás ideje');
					$sheet->setCellValue('K1', $closed_month->created_at->format('Y-m-d H:i:s'));
					$sheet->mergeCells('K1:L1');
					
					$sheet->cells('K1:L1', function ($cells) {
						$cells->setAlignment('right');
					});
					
					$sheet->setCellValue('A3', 'Munkavállaló');
					$sheet->setCellValue('B3', 'Azonosító');
					$sheet->setCellValue('C3', 'Normál munkaidő');
					$sheet->setCellValue('D3', 'Szabadság');
					$sheet->setCellValue('E3', 'Beteg-szabadság');
					$sheet->setCellValue('F3', 'Rendkívüli munka - 50%');
					$sheet->setCellValue('G3', 'Munkaidő kereten felüli - 50%');
					$sheet->setCellValue('H3', 'Munkaidő kereten felül összesen - 50%');
					$sheet->setCellValue('I3', 'Vasárnapi pótlék - 50%');
					$sheet->setCellValue('J3', 'Ünnepnap - 100%');
					$sheet->setCellValue('K3', 'Műszak pótlék - 30%');
					$sheet->setCellValue('L3', 'Éjszakai pótlék - 15%');
					
					$sheet->cells('A3:L3', function ($cells) {
						$cells->setBorder('thin', 'thin', 'thin', 'thin');
					});
					
					foreach (range ('A', 'L') as $letter) {
						$sheet->cell($letter.'3', function ($cells) {
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}
					
					$i = 4;
					
					$start_date = $date->copy()->startOfMonth();
					$end_date = $date->copy()->endOfMonth();
					
					$_restricteds = RestrictedDate::select("*")->whereBetween('date', [$start_date, $end_date])->get();
					$restrited_workdays = [];
					foreach($_restricteds as $restricted) {
						if ($restricted->status->alias == 'workday') {
							$restrited_workdays[] = $restricted->date;
						}
					}
					
					$standard_workday = RestrictedDateStatus::where('alias', '=', 'workday')->first();
					while ($start_date < $end_date)
					{
						if (!in_array($start_date->format('Y-m-d'), $restrited_workdays)) {
							if ($standard_workday->hasDay($start_date->format('N')-1)) {
								$restrited_workdays[] = $start_date->format('Y-m-d');
							}
						}
						$start_date->addDay();
					}
					
					$all_workdays = 0;
					$all_free_days = 0;
					$all_sick_leaves = 0;
					$all_holidays = 0;
					$all_over_time = 0;
					$all_over_workhours_in_month = 0;
					$all_over_workhours_in_quarter = 0;
					$all_over_workhours = 0;
					$all_sundays = 0;
					$all_shifts = 0;
					$all_nights = 0;
					$all_sum_normal = 0;
					
					foreach($workers as $worker) {
						$month = $worker->getMonthDays($date->year, $date->month, true);
						$worker_info = $worker->getInfo($date->year, $date->month);
						
						$sheet->setCellValue('A'.$i, $worker->name);
						$sheet->setCellValue('B'.$i, $worker->nr);
						
						$workdays = 0;
						$free_days = 0;
						$sick_leaves = 0;
						$free_day_hours = 0;
						$sick_leave_hours = 0;
						$holidays = '00:00';
						$over_time = '00:00';
						$sundays = '00:00';
						$shifts = 0;
						$nights = 0;
						$sum_normal = '00:00';
						$last_day = null;
						
						foreach($month as $week) {
							foreach($week as $day) {
								if ($day['restricted']->alias == 'free_day' || $day['restricted']->alias == 'sick_leave') {
									$expected_hours = LSHelper::timeOperation($day['groupdate']->start, $day['groupdate']->end, $day['groupdate']->start, $day['groupdate']->end)->h;
								}
								
								switch($day['restricted']->alias) {
									case 'workday':
										$workdays++;
										break;
									case 'free_day':
										$free_days++;
										$free_day_hours += $expected_hours;
										break;
									case 'sick_leave':
										$sick_leaves++;
										$sick_leave_hours += $expected_hours;
										break;
									case 'holiday':
										if ($day['check'] && $day['check']->rounded_diff) {
											$holidays = LsHelper::timeMod($holidays, $day['check']->rounded_diff);
										}
										break;
									default:
										$check_in_h = LsHelper::timeExp($day['check']->check_in);
										$check_out_h = LsHelper::timeExp($day['check']->check_out);
										if (($day['dt']->format('N') == 7 || $day['restricted']->alias == 'day_off_sunday')) {
											if ($check_in_h > $check_out_h) {
												$sundays = LsHelper::timeMod($sundays, LsHelper::timeOperation($day['check']->check_in, '00:00', $day['check']->check_in, '00:00')->format('%H:%I'));
											} else {
												$sundays = LsHelper::timeMod($sundays, LsHelper::timeOperation('00:00', $day['check']->check_out, '00:00', $day['check']->check_out)->format('%H:%I'));
											}
										} elseif (($day['dt']->format('N') == 6 || ($last_day && $last_day['restricted']->alias == 'day_off_sunday')) && $check_in_h > $check_out_h) {
											$sundays = LsHelper::timeMod($sundays, LsHelper::timeOperation('00:00', $day['check']->check_out, $day['check']->check_in, $day['check']->check_out)->format('%H:%I'));
										}
										
										break;
								};
								if ($day['check'] && $day['check']->id !== null) {
									$over_time = LsHelper::timeMod($over_time, $day['check']->rounded_over_time);
									
									if (!$worker->nightBonusMonth($date->year, $date->month)) {
										$shifts = LsHelper::timeOperation('18:00', '06:00', $day['check']->rounded_check_in, $day['check']->rounded_check_out, 'inner')->format('%h');
										$nights = 0;
									} else {
										$shifts = 0;
										$nights = LsHelper::timeOperation('22:00', '06:00', $day['check']->rounded_check_in, $day['check']->rounded_check_out, 'inner')->format('%h');
									}
									$sum_normal = LsHelper::timeMod($sum_normal, $day['check']->rounded_normal_time);
								}
								$last_day = $day;
							}
						}
						
						$sheet->setCellValue('C'.$i, LsHelper::timeExp($sum_normal));
						$sheet->setCellValue('D'.$i, $free_day_hours);
						$sheet->setCellValue('E'.$i, $sick_leave_hours);
						$sheet->setCellValue('F'.$i, LsHelper::timeExp($over_time));
						//$sheet->setCellValue('G'.$i, (($date->month)%3 == 0? $worker_info['over_time'] : 0));
						$sheet->setCellValue('G'.$i, $worker_info['over_workhours_in_month']);
						$sheet->setCellValue('H'.$i, $worker_info['over_workhours_in_quarter']);
						$sheet->setCellValue('I'.$i, LsHelper::timeExp($sundays));
						$sheet->setCellValue('J'.$i, $holidays);
						$sheet->setCellValue('K'.$i, $shifts);
						$sheet->setCellValue('L'.$i, $nights);
						
						$all_workdays += $workdays;
						$all_free_days += $free_days;
						$all_sick_leaves += $sick_leaves;
						$all_holidays += LsHelper::timeExp($holidays);
						$all_over_time += LsHelper::timeExp($over_time);
						$all_over_workhours_in_month += $worker_info['over_workhours_in_month'];
						$all_over_workhours_in_quarter += $worker_info['over_workhours_in_quarter'];
						$all_over_workhours += $worker_info['over_workhours'];
						$all_sundays += LsHelper::timeExp($sundays);
						$all_shifts += $shifts;
						$all_nights += $nights;
						$all_sum_normal += LsHelper::timeExp($sum_normal);
						
						foreach (range ('A', 'L') as $letter) {
							$sheet->cell($letter.$i, function ($cells) {
								$cells->setBorder('thin', 'thin', 'thin', 'thin');
							});
						}
						
						$i++;
					}
					
					$i++;
					$sheet->setCellValue('A'.$i, "Összesen");
					$sheet->setCellValue('B'.$i, count($restrited_workdays));
					//$sheet->setCellValue('C'.$i, $all_sum_normal*8);
					$sheet->setCellValue('D'.$i, $all_free_days);
					$sheet->setCellValue('E'.$i, $all_sick_leaves);
					$sheet->setCellValue('F'.$i, $all_over_time);
					//$sheet->setCellValue('G'.$i, ($date->month%3 == 0? $all_worker_over_time : 0));
					$sheet->setCellValue('G'.$i, $all_over_workhours_in_month);
					//$sheet->setCellValue('H'.$i, $all_over_workhours);
					$sheet->setCellValue('H'.$i, $all_over_workhours_in_quarter);
					$sheet->setCellValue('I'.$i, $all_sundays);
					$sheet->setCellValue('J'.$i, $all_holidays);
					$sheet->setCellValue('K'.$i, $all_shifts);
					$sheet->setCellValue('L'.$i, $all_nights);
					
					foreach (range ('A', 'L') as $letter) {
						$sheet->cell($letter.$i, function ($cells) {
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}
					
					$sheet->setWidth(array(
											'A'	=>	24,
											'B'	=>	10,
											'C'	=>	16.5,
											'D'	=>	10,
											'E'	=>	16.5,
											'F'	=>	23,
											'G'	=>	29,
											'H'	=>	36,
											'I'	=>	22,
											'J'	=>	16.5,
											'K'	=>	19,
											'L'	=>	20,
											));
					
					$sheet->setOrientation('landscape');
			});
		})->store('xls', storage_path($directory));
		
		
		
		$zipper = new Zipper;
		$zip_path = storage_path($this->export_dir).'/'. $date->year .'-'. $date->month .'.zip';
		$zipper->make($zip_path)->add(glob(storage_path($directory)));
		$zipper->close();
		
		return redirect()->route('closed_month.index');
		//return response()->download($zip_path);
	}
	
	public function download($month_id, $worker_id = null) {
		$closed_month = ClosedMonth::find($month_id);
		if ($closed_month) {
			$date = Carbon::parse($closed_month->date);
			if ($worker_id) {
				$worker = Worker::find($worker_id);
				if ($worker) {
					$files = glob(storage_path($this->export_dir.'/'.$date->year.'-'.$date->month.'/*_'.$date->year.'-'.$date->month.'.xls'));
					foreach($files as $file) {
						$filename = pathinfo($file, PATHINFO_BASENAME);
						if (explode('_', $filename)[0] == $worker->nr) {
							return response()->download($file);
						}
					}
				}
			} else {
				return response()->download(storage_path($this->export_dir.'/'.$date->year.'-'.$date->month.'/'.$date->year.'-'.$date->month.'.xls'));
			}
		}
		return redirect()->route('closed_month.view', $month_id)->withErrors(['Nem található a fájl.']);
	}
	
	public function downloadZip($month_id) {
		$closed_month = ClosedMonth::find($month_id);
		if ($closed_month) {
			$date = Carbon::parse($closed_month->date);
			return response()->download(storage_path($this->export_dir.'/'.$date->year.'-'.$date->month.'.zip'));
		}
		return redirect()->route('closed_month.view', $month_id)->withErrors(['Nem található a fájl.']);
	}
	
	public function open() {
		$closed_months = ClosedMonth::orderBy('date', 'desc');
		if ($closed_months->count() < 1) {
			return redirect()->back()->withErrors(['Nincs feloldható hónap.']);
		}
		$closed_month = $closed_months->first();
		$dt = Carbon::parse($closed_month->date);
		$date = $dt->format('Y-n');
		ClosedDate::where(['closed_month_id' => $closed_month->id])->delete();
		WorkerMonthHistory::where(['closed_month_id' => $closed_month->id])->delete();
		Storage::deleteDirectory(storage_path($this->export_dir.'/'.$date));
		Storage::delete(storage_path($this->export_dir.'/'.$date.'.zip'));
		ClosedMonth::destroy($closed_month->id);
		$this->_setSessionDate(['year' => $dt->format('Y'), 'month' => $dt->format('n')]);
		return redirect()->back()->with(['msg' => ['Sikeresen feloldotta a legutóbbi zárást.']]);
	}
}
