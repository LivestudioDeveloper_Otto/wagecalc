<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RestrictedDate;
use App\ClosedMonth;
use App\RestrictedDateStatus;
use Carbon\Carbon;

class RestrictedDateController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
        parent::__construct();
	}
	
	public function view()
	{
		
		$date = Carbon::parse(ClosedMonth::getLast())->addMonth();
		
		$restricted_dates = RestrictedDate::where('date', '>=', $date)->get();
		$restricted_date_statuses = RestrictedDateStatus::get();
		
		$restricted_date_status_options = [];
		
		foreach($restricted_date_statuses as $restricted_date_status)
		{
			$restricted_date_status_options[$restricted_date_status->id] = $restricted_date_status->name; 
		}
		
		return view('admin.restricteddate.default', compact('restricted_dates', 'restricted_date_status_options', 'date'));
	}
	
	public function create(Request $request)
	{
		$messages = [
			'date.required' => 'A Dátum mezőt kötelező megadni!',
			'restricted_date_status_id.required' => 'A Típus mezőt kötelező megadni!',
			'exists' => 'A Típus nem létezik!'
		];
		
		$request->validate([
            'date' => 'required|date',
            'restricted_date_status_id' => 'required|int|exists:restricted_date_statuses,id'
        ], $messages);
		
		$new_date_status = RestrictedDateStatus::find($request->restricted_date_status_id);
		$date = Carbon::parse($request->date);
		
		if(ClosedMonth::isClosedMonth($date))
		{
			return redirect()->back()->withErrors(['A hónapot már lezárták!']);
		}
		
		$restricted_date = RestrictedDate::where('date', $request->date)->first();
		if($restricted_date !== null)
		{
			if($new_date_status->hasDay($date->format('N')-1))
			{
				$restricted_date->delete();
				return redirect()->back();
			}
			else if($restricted_date->restricted_date_status_id === $new_date_status->id)
			{
				return redirect()->back()->withErrors(['A dátum már a megadott típussal rendelkezik!']);
			}
		}
		else
		{
			if($new_date_status->hasDay($date->format('N')-1))
			{
				return redirect()->back()->withErrors(['A dátum új típusa megegyezik a globális beállításokkal (' . $new_date_status->name . ')!']);
			}
		}
		
		RestrictedDate::updateOrCreate(['date' => $request->date], ['restricted_date_status_id' => $request->restricted_date_status_id]);

		return redirect()->back();
	}
	
	public function delete(RestrictedDate $restricted_date)
	{
		$date = Carbon::parse($restricted_date->date);
		if(ClosedMonth::isClosedMonth($date))
		{
			return redirect()->back()->withErrors(['A hónapot már lezárták!']);
		}
		
		$restricted_date->delete();

		return redirect()->back();
	}
}
