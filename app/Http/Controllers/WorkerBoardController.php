<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Worker;
use App\Company;

class WorkerBoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }
	
    public function view()
	{
        $workers = Worker::orderBy('name')->get();
		
		$sessionDate = Session::get('sessionDate');
        $date = \Carbon\Carbon::createFromDate($sessionDate['year'], $sessionDate['month'], 1);
		
        return view('admin.workerboard.board', compact('workers', 'date'));
    }
	
    public function board($company_id)
	{
        $workers = Worker::where('company_id', '=', $company_id)->where('active', '=', '1')->orderBy('name')->get();
		
		$sessionDate = Session::get('sessionDate');
        $date = \Carbon\Carbon::createFromDate($sessionDate['year'], $sessionDate['month'], 1);
		
        return view('admin.workerboard.board', compact('workers', 'company_id', 'date'));
    }
}
