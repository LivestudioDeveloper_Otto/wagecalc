<?php

namespace App\Http\Controllers;

use App\GroupHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupHistoryController extends Controller
{
	protected $user;
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function view()
	{
		$group_histories = GroupHistory::all();
		return view('admin.grouphistory.default', compact('group_histories'));
	}
	
	public function create(Request $request)
	{
		$messages = [
			'date.unique' => 'Ezt a dátumot már definiálták!',
		];
		
		$request->validate([
            'date' => 'date|unique:group_history,date'
        ], $messages);
		
		$date = Carbon::parse($request->date);
		if(ClosedMonth::isClosedMonth($date))
		{
			return redirect()->back()->withErrors(['A hónapot már lezárták!']);
		}
		
		GroupHistory::updateOrCreate(['date' => $request->date], ['user_id' => Auth::user()->id]);

		return redirect()->back();
	}
	
	public function delete(GroupHistory $group_history)
	{
		$date = \Carbon\Carbon::parse($group_history->date);
		if(\App\ClosedMonth::isClosedMonth($date))
		{
			return redirect()->back()->withErrors(['A hónapot már lezárták!']);
		}
		
		if(!$group_history->user_id)
		{
			return redirect()->back()->withErrors(['Rendszer által hozzáadott műszak újrakezdést nem lehet törölni!']);
		}
		
		$group_history->delete();

		return redirect()->back();
	}
}
