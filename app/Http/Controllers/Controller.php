<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\ClosedMonth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct() {
        $sessionDate = Session::get('sessionDate');
        if (!$sessionDate) {
            $this->_setSessionDate();
        }
    }
    
    public function setSessionDate(Request $request) {
        $this->middleware("auth");
        $this->_setSessionDate($request->only('year', 'month'));
        return back();
    }
    
    protected function _setSessionDate($sessionDate = null) {
        $dt = Carbon::createFromFormat('Y-m-d', ClosedMonth::getLast());
        $dt->addMonth();
        if ($sessionDate) {
            $dt1 = Carbon::createFromDate($sessionDate['year'], $sessionDate['month'], 1);
            if ($dt1 >= $dt) {
                $dt = $dt1;
            }
        }
        Session::put(['sessionDate' => ['year' => $dt->year, 'month' => $dt->month]]);
    }
}
