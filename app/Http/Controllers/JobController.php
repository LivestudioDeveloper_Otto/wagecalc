<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;

class JobController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
        parent::__construct();
	}
	
	public function view()
	{
		$jobs = Job::all();
		return view('admin.job.default', compact('jobs'));
	}
	
	public function edit(Request $request)
	{
		$job = Job::findOrNew($request->id);
		
		return view('admin.job.edit', compact('job'));
	}
	
	public function create(Request $request)
	{
		$messages = [
			'name.required' => 'A nevet kötelező megadni!',
			'white_collar.required' => 'Mezőt kötelező kitölteni: Szellemi'
		];
		
		$request->validate([
            'name' => 'required|string',
            'white_collar' => 'required|boolean'
        ], $messages);
		
		$job = Job::firstOrNew($request->only('id'));
		$job->fill($request->only('name', 'white_collar'));
		$job->save();

		return redirect()->back();
	}
	
	public function delete(Request $request)
	{
		$messages = [
			'exists' => 'A törölni kívánt elem nem létezik!',
		];
		
		$request->validate([
            'id' => 'exists:jobs,id'
        ], $messages);
		
		Job::destroy($request->id);

		return redirect()->back();
	}
}
