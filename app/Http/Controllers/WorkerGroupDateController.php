<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkerGroup;
use App\WorkerGroupDate;

class WorkerGroupDateController extends Controller
{
    public function __construct(Request $request)
	{
		$this->middleware('auth');
        parent::__construct();
		
		$messages = [
			'group_id.exists' => 'A megadott csoport nem létezik!',
		];
		
		$request->validate([
            'group_id' => 'exists:worker_groups,id',
        ], $messages);
	}
	
	public function view(Request $request)
	{		
		$worker_group = WorkerGroup::find($request->group_id);
		$worker_group_dates = WorkerGroupDate::where(['group_id' => $request->group_id])->get();
		return view('admin.workergroupdate.default', compact('worker_group_dates', 'worker_group'));
	}
	
	public function edit(Request $request)
	{
		$group_date = WorkerGroupDate::findOrNew($request->id);
		$group_id = $request->group_id;
		
		if($group_date !== null && $group_date->protected)
		{
			return redirect()->back()->withErrors(['Ez egy alapértelmezett műszak. Nem lehet módosítani!']);
		}
		
		return view('admin.workergroupdate.edit', compact('group_date', 'group_id', 'ordering'));
	}
	
	public function create(Request $request)
	{
		$messages = [
			'active.required' => 'A státuszt kötelező megadni!',
			'ordering.required' => 'A rendezést kötelező megadni!',
			'start.required' => 'A kezdő időpontot kötelező megadni!',
			'end.required' => 'A zárás időpontot kötelező megadni!',
			'bonus.required' => 'A bónuszt kötelező megadni!'
		];
		
		$request->validate([
            'start' => 'required|string',
            'end' => 'required|string',
            'active' => 'required|string',
            'ordering' => 'required|int',
            'bonus' => 'required|int'
        ], $messages);
		
		
		$worker_group_date = WorkerGroupDate::find($request->id);
		if($worker_group_date !== null && $worker_group_date->protected)
		{
			return redirect()->back()->withErrors(['Ez egy alapértelmezett csoport. Nem lehet módosítani!']);
		}
		
		WorkerGroupDate::updateOrCreate(['id' => $request->id], ['start' => $request->start, 'end' => $request->end, 'active' => $request->active, 'ordering' => $request->ordering, 'group_id' => $request->group_id, 'bonus' => $request->bonus]);

		return redirect()->back();
	}
	
	public function delete(WorkerGroupDate $worker_group_date)
	{
		if($worker_group_date->protected)
		{
			return redirect()->back()->withErrors(['Ez egy alapértelmezett műszak. Nem lehet törölni!']);
		}
		
		$worker_group_date->delete();

		return redirect()->back();
	}
}
