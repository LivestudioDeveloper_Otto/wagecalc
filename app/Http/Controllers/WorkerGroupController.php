<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkerGroup;

class WorkerGroupController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
        parent::__construct();
	}
	
	public function view()
	{
		$worker_groups = WorkerGroup::all();
		return view('admin.workergroup.default', compact('worker_groups'));
	}
	
	public function edit(Request $request)
	{
		$worker_group = WorkerGroup::findOrNew($request->id);
		
		if($worker_group !== null && $worker_group->protected)
		{
			return redirect()->back()->withErrors(['Ez egy alapértelmezett csoport. Nem lehet módosítani!']);
		}
		
		return view('admin.workergroup.edit', compact('worker_group'));
	}
	
	public function create(Request $request)
	{
		$messages = [
			'name.required' => 'A nevet kötelező megadni!',
			'active.required' => 'A státuszt kötelező megadni!'
		];
		
		$request->validate([
            'name' => 'required|string',
            'active' => 'required|boolean'
        ], $messages);
		
		$worker_group = WorkerGroup::find($request->id);
		if($worker_group !== null && $worker_group->protected)
		{
			return redirect()->back()->withErrors(['Ez egy alapértelmezett csoport. Nem lehet módosítani!']);
		}
		
		WorkerGroup::updateOrCreate(['id' => $request->id], ['name' => $request->name, 'active' => $request->active]);

		return redirect()->back();
	}
	
	public function delete(WorkerGroup $worker_group)
	{
		if($worker_group->protected)
		{
			return redirect()->back()->withErrors(['Ez egy alapértelmezett csoport. Nem lehet törölni!']);
		}
		
		$worker_group->delete();

		return redirect()->back();
	}
}
