<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Worker;
use App\RestrictedWorkerDate;
use App\RestrictedWorkerDateStatus;
use App\RestrictedDate;
use App\RestrictedDateStatus;
use App\Check;
use App\WorkerGroup;
use App\Company;
use App\Job;
use App\ClosedMonth;

use App\Libraries\LsHelper;

class WorkerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }
    
    public function board($worker_id) {
        $dt = $this->_getDate();
        $worker = Worker::findOrFail($worker_id);
        $month = $worker->getMonthDays($dt->format('Y'), $dt->format('n'), true);
        
        $core_times = Worker::getCoreTimes();
        $formal_times = Worker::getFormalTimes();
        
		/*
        $worker->core_start = Carbon::createFromFormat('H:i:s', $worker->core_start)->format('H:i');
        $worker->core_end = Carbon::createFromFormat('H:i:s', $worker->core_end)->format('H:i');
        $worker->formal_start = Carbon::createFromFormat('H:i:s', $worker->formal_start)->format('H:i');
        $worker->formal_end = Carbon::createFromFormat('H:i:s', $worker->formal_end)->format('H:i');
        */
        
        $_statuses = RestrictedWorkerDateStatus::all();
        $statuses = [];
		$statuses['default'] = ['name' => 'Alapértelmezett', 'alias' => 'default'];
        foreach ($_statuses as $status) {
            $statuses[$status->id] = ['name' => $status->name, 'alias' => $status->alias];
        }
        
        $free_days = $worker->countLastDays('free_day');
        $sick_leaves = $worker->countLastDays('sick_leave');
        $workdays = $worker->countLastDays('workday');
        
        $info = $worker->getInfo($dt->format('Y'), $dt->format('m'));
		
        return view('admin.worker.board', compact('worker', 'core_times', 'formal_times', 'month', 'statuses', 'free_days', 'sick_leaves', 'workdays', 'info'));
    }
    
    public function saveBoard($worker_id, Request $request) {
        $dt = $this->_getDate();
        $worker = Worker::findOrFail($worker_id);
        $month = $worker->getMonthDays($dt->format('Y'), $dt->format('n'));
        
        $check_dates = $worker->getMonthChecks($dt->format('Y'), $dt->format('n'));
        
        $configured_status = $request->get('configured_status');
        $restricted_dates = $worker->getAllRestrictedDates($month, $worker);
        foreach($configured_status as $date => $status_id) {
            $restricted_date = RestrictedWorkerDate::firstOrNew(['date' => $date, 'worker_id' => $worker_id]);
			if ($status_id == 'default' && $restricted_date) {
				RestrictedWorkerDate::destroy($restricted_date->id);
			}
            if ($status_id == $restricted_dates[$date]->id) {
                unset($configured_status[$date]);
                continue;
            }
			
            $restricted_date->restricted_worker_date_status_id = $status_id;
            $restricted_date->save();
        }
        
        $check = [];
        $check_in = $request->get('configured_check_in');
        $check_out = $request->get('configured_check_out');
        $formal_start = $request->get('configured_formal_start');
        $formal_end = $request->get('configured_formal_end');
        $formal = Worker::getFormalTimes();
		
        foreach($month as $week) {
            foreach($week as $_day) {
                $day = $_day['dt']->format('Y-m-d');
                $check_id = 0;
				
                if (array_key_exists($day, $check_dates)) {
                    $check_id = $check_dates[$day]->id;
                }
				
                $check = Check::findOrNew($check_id);
				//dd($day, $_day, $check_in[$day], $check_out[$day]);
				$act_group_date = $_day['groupdate'];
				
				if (!$check_in[$day] && !$check_out[$day] && ((!$formal_start[$day] && !$formal_end[$day]) || ($formal_start[$day] == LsHelper::timeFormat($act_group_date->start, false, 0, true) && $formal_end[$day] == LsHelper::timeFormat($act_group_date->end, false, 0, true)))) {
					$check->configured_check_in = null;
					$check->configured_check_out = null;
					$check->configured_formal_start = null;
					$check->configured_formal_end = null;
					$check->save();
					continue;
                }
				
				if (!$check_id) {
					$check->worker_id = $worker_id;
				}
				
				$check->configured_check_in = ($check_in[$day] !== null ? $day . ' ' . $check_in[$day] : null);
                $check_out_day = $day;
				
                if ($check_in[$day] && $check_out[$day] && Carbon::createFromFormat("H:i", $check_in[$day]) > Carbon::createFromFormat("H:i", $check_out[$day])) {
                    $date = Carbon::createFromFormat('Y-m-d', $check_out_day);
                    $date->addDay();
                    $check_out_day = $date->format('Y-m-d');
                }
				
				$check->configured_check_out = ($check_out[$day] !== null ? $check_out_day . ' ' . $check_out[$day] : null);
				$check->configured_formal_start = ($formal_start[$day] !== null ? $day . ' ' . $formal_start[$day] : null);
				
                $formal_end_day = $day;
				
                if ($formal_start[$day] && $formal_end[$day] && Carbon::createFromFormat("H:i", $formal_start[$day]) > Carbon::createFromFormat("H:i", $formal_end[$day])) {
                    $date = Carbon::createFromFormat('Y-m-d', $formal_end_day);
                    $date->addDay();
                    $formal_end_day = $date->format('Y-m-d');
                }
				
				$check->configured_formal_end = ($formal_end[$day] !== null ? $formal_end_day . ' ' . $formal_end[$day] : null);
				
                $check->save();
            }
        }
        
        //dd($request->all());
        $redirect = redirect();
		if ($request->input('returnBack')) {
			$redirect = $redirect->route('workerboard');
		} else {
			$redirect = $redirect->route('worker.board', $worker_id);
		}
		return $redirect->with('msg', 'Sikeres mentés.');
        
    }
    
    public function edit($worker_id = null) {
		if ($worker_id) {
			$worker = Worker::findOrFail($worker_id);
		} else {
			$worker = new Worker;
		}
        
        $companies = [];
        foreach(Company::all() as $company) {
            $companies[$company->id] = $company->name;
        }
        
        $groups = [];
        foreach(WorkerGroup::all() as $group) {
            $groups[$group->id] = $group->name;
        }
        
        $jobs = [];
        foreach(Job::all() as $job) {
            $jobs[$job->id] = $job->name;
        }
        
        return view('admin.worker.edit')->with(compact('worker', 'groups', 'companies', 'jobs'));
    }
    
    public function save(Request $request) {
		$messages = [
			'nr.required' => 'Azonosítót kötelező megadni!',
			'name.required' => 'A nevet kötelező megadni!',
			'company_id.required' => 'Részleget választani kötelező!',
			'group_id.required' => 'Csoportot választani kötelező!',
			'job_id.required' => 'Beosztást választani kötelező!'
		];
		
		$request->validate([
            'nr' => 'required|string',
            'name' => 'required|string',
            'company_id' => 'required|int',
            'group_id' => 'required|int',
            'job_id' => 'required|int'
        ], $messages);
		$worker = Worker::firstOrNew($request->only('id'));
		$worker->fill($request->only('nr', 'name', 'company_id', 'group_id', 'job_id', 'annual_free_day', 'annual_sick_leave', /*'annual_sick_pay',*/ 'active'));
		$worker->save();
		return redirect()->route('workerboard');
    }
	
	public function exportMonth($worker_id) {
		$worker = Worker::findOrFail($worker_id);
		$date = $this->_getDate();
		$path = $worker->exportMonth($date, 'worker-export/');
		
		return response()->download($path);
	}
    
    protected function _getDate() {
        $sessionDate = Session::get('sessionDate');
        return Carbon::createFromDate($sessionDate['year'], $sessionDate['month'], 1);
    }

}
