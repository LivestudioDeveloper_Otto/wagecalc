<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App;
use App\Check;

class CheckController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
        parent::__construct();
	}
	
    public function index() {
        //ID	Card ID	Worker No.	Users	Department	Date/Time	Address	Allowable	Leírás

        $checks = Check::getChecksArray();
		//dd($checks);
        return view('admin.check.index');
    }
    
    public function import(Request $request) {
		$file = $request->file('input_file');
        $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $validator = Validator::make([
            'input_file' => $request->input_file,
            'ext' => $ext,
        ], [
            'input_file' => 'required',
            'ext' => 'in:csv,xls,xlsx,txt',
        ], [
            'input_file.required' => 'Fájl feltöltése kötelező!',
            'ext' => 'A fájl formátuma nem engedélyezett!',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $basename = rtrim(pathinfo($file->hashname(), PATHINFO_BASENAME), '.');
		$path = $file->storeAs('input', $basename.'.'.$ext);
        
        $inputFileName = storage_path('app/'.$path);
        $excel = App::make('excel');
        
        $checks = Check::getChecksArray();
		try {
			$excel->load($inputFileName, function($reader) use(&$checks) {
				$objects = [];
				foreach($reader->toObject() as $object) {
					if (!$object->{'worker_no.'}) {
						continue;
					}
					$objects[$object->id] = $object;
				}
				ksort($objects);
				foreach($objects as $key => $row) {
					$worker_no = intval($row->{'worker_no.'});
					$dt = Carbon::createFromFormat('Y.m.d.G:i:s', str_replace(' ', '', $row->datetime));
					$today = $dt->toDateString();
					if (!array_key_exists($worker_no, $checks)) {
						$checks[$worker_no] = [];
					}
					if (strpos($row->address, '[Be]')) {
						if (!array_key_exists($today, $checks[$worker_no])) {
							$checks[$worker_no][$today] = ['id' => null, 'configured_check_in' => null, 'configured_check_out' => null];
						}
						$checks[$worker_no][$today]['check_in'] = $dt->toDateTimeString();
						if (!$checks[$worker_no][$today]['configured_check_in']) {
							$checks[$worker_no][$today]['configured_check_in'] = $dt->toDateTimeString();
						}
					}
					if (strpos($row->address, '[Kilép]')) {
						$yesterday = $dt->copy()->subDay()->toDateString();
						
						if (!array_key_exists($today, $checks[$worker_no])) {
							$today = $yesterday;
						}
							
						if (!array_key_exists($today, $checks[$worker_no])) {
							$checks[$worker_no][$today] = ['id' => null, 'configured_check_in' => null, 'configured_check_out' => null];
						} else {
							$checks[$worker_no][$today]['check_out'] = $dt->toDateTimeString();
							if (!$checks[$worker_no][$today]['configured_check_out']) {
								$checks[$worker_no][$today]['configured_check_out'] = $dt->toDateTimeString();
							}
						}
					}
				}
			});
			
			$result = Check::setChecksArray($checks, true);
		} catch(\Exception $e) {
			return redirect()->back()->withErrors(['msg' => 'Hiba történt a fájl betöltése során! ' . $e]);
		}
        
        return redirect()->back()->with('success', 'Fájl feldolgozása sikeres volt. Feldolgozott dokkolások: '. $result['success'] .', kihagyva: '. $result['miss'] .'.');
    }
}
