<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
        parent::__construct();
	}
	
	public function view()
	{
		$companies = Company::all();
		return view('admin.company.default', compact('companies'));
	}
	
	public function edit(Request $request)
	{
		$company = Company::findOrNew($request->id);
		
		return view('admin.company.edit', compact('company'));
	}
	
	public function create(Request $request)
	{
		$messages = [
			'name.required' => 'A nevet kötelező megadni!',
			//'active.required' => 'A státuszt kötelező megadni!',
			'overtime.required' => 'A túlórakeretet kötelező megadni!'
		];
		
		$request->validate([
            'name' => 'required|string',
            //'active' => 'required|boolean',
            'overtime' => 'required|integer',
        ], $messages);
		
		Company::updateOrCreate(['id' => $request->id], ['name' => $request->name, 'active' => $request->active, 'overtime' => $request->overtime]);

		return redirect()->back();
	}
	
	public function delete(Request $request)
	{
		$messages = [
			'exists' => 'A törölni kívánt elem nem létezik!',
		];
		
		$request->validate([
            'id' => 'exists:companies,id'
        ], $messages);
		
		Company::destroy($request->id);

		return redirect()->back();
	}
}
