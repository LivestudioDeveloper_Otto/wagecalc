<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ClosedMonth extends Model
{
    protected $fillable = ['date', 'user_id'];
    
	public static function isClosedMonth(Carbon $date)
	{
		return self::where('date', '>=', $date->copy()->startOfMonth())->exists();
	}
	
	public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}
	
	public function dates()
	{
		return $this->hasMany(ClosedDate::class, 'closed_month_id', 'id');
	}
	
	public function getWorkers() {
		$workers = $this->dates()->select('workers.*')->join('checks', 'checks.id', '=', 'closed_dates.check_id')->join('workers', 'workers.id', '=', 'checks.worker_id')->groupBy('workers.id')->orderBy('workers.name')->get();
		
		return $workers;
	}
	/*
	 *	Get last Closed month
	 */
    public static function getLast()
	{
        return self::orderBy('date', 'desc')->pluck('date')->first();
    }
	
	/*
	 *	Get the active date (+1 months from last closed month)
	 *	@return
	 *	Carbon DateTime
	 */
    public static function getActiveDate()
	{
        $date = self::getLast();
		return Carbon::parse($date)->addMonth();
    }
	
}
